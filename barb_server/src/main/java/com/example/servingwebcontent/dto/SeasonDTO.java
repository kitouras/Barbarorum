package com.example.servingwebcontent.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SeasonDTO {
    private String name;
    private double temperatureMod;
    private double windMod;
    private double humidityMod;
    private double atmosphericPressureMod;
    long incomeModsId;
}
