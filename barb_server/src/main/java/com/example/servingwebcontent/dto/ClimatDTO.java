package com.example.servingwebcontent.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClimatDTO {
    private String name;
    private int temperatureDegree;
    private int windDegree;
    private int humidityDegree;
    private int atmosphericPressureDegree;
    long incomeModsId;
}
