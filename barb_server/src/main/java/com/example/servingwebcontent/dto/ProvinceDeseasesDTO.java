package com.example.servingwebcontent.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProvinceDeseasesDTO {
    private int prevalence;
    private double collectiveImmunity;
    private long provinceId;
    private long deseaseId;
}
