package com.example.servingwebcontent.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LegalSystemDTO {
    private String groupName;
    private boolean isTaxFixed;
    private double taxAboveMultiplier;
    private double autonomyFactor;
    long incomeModsId;
}
