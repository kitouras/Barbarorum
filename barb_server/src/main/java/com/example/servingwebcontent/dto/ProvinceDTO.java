package com.example.servingwebcontent.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProvinceDTO {
    private String name;
    private int autonomy;
    private int rebellionPotential;
    private int villagePopularity;
    private int popularityGrowth;
    private double treasury;
    private double taxInto;
    private double taxAbove;
    long cityId;
    long stateId;
    long legalSystemId;
    long climatId;
    long seasonId;
    long landscapeId;
}
