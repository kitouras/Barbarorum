package com.example.servingwebcontent.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProvinceResourcesLifecycleDTO {
    private int resourceStock;
    private int resourceProduction;
    private int resourceLimit;
    private int resourceMarketing;
    private long provinceId;
    private long resourceId;
}
