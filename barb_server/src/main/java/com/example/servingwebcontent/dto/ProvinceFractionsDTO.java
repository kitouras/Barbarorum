package com.example.servingwebcontent.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProvinceFractionsDTO {
    private int authority;
    private int integrity;
    private long provinceId;
    private long fractionId;

}
