package com.example.servingwebcontent.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NeighborhoodOfProvincesDTO {
    private long firstProvinceId;
    private long secondProvinceId;
    private long relationId;
    private int degree;
}
