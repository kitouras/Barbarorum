package com.example.servingwebcontent.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IncomeAffectableModeDTO {
    private String name;
    private double villageTaxMultiplier;
    private double cityTaxMultiplier;
    private double foodMultiplier;
    private double tradeMultiplier;
    private double handicraftMultiplier;
    private double foodMarketingMultiplier;
    private double tradeMarketingMultiplier;
    private double handicraftMarketingMultiplier;
}
