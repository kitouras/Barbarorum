package com.example.servingwebcontent.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeseaseDTO {
    private String name;
    private double incidenceRate;
    private double mortality;
}
