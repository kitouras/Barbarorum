package com.example.servingwebcontent.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LandscapeDTO {
    private String name;
    private int mountainousness;
    long incomeModsId;
}
