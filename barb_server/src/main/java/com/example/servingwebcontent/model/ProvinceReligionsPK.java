package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class ProvinceReligionsPK implements Serializable {
    @Column(name = "province_id")
    private long provinceId;
    @Column(name = "religion_id")
    private long religionId;

    public ProvinceReligionsPK() {
    }

    public ProvinceReligionsPK(Long provinceId, Long religionId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvinceReligionsPK neighborhoodId = (ProvinceReligionsPK) o;
        return provinceId == neighborhoodId.religionId &&
                religionId == neighborhoodId.religionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(provinceId, religionId);
    }
}