package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class ProvinceDeseasesPK implements Serializable {
    @Column(name = "province_id")
    private long provinceId;
    @Column(name = "desease_id")
    private long deseaseId;

    public ProvinceDeseasesPK() {
    }

    public ProvinceDeseasesPK(Long provinceId, Long deseaseId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvinceDeseasesPK neighborhoodId = (ProvinceDeseasesPK) o;
        return provinceId == neighborhoodId.deseaseId &&
                deseaseId == neighborhoodId.deseaseId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(provinceId, deseaseId);
    }
}