package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
@Getter
@Setter
@Embeddable
public class NeighborhoodOfProvincesPK implements Serializable {
    @Column(name = "first_province_id")
    private long firstProvinceId;
    @Column(name = "second_province_id")
    private long secondProvinceId;

    public NeighborhoodOfProvincesPK() {
    }

    public NeighborhoodOfProvincesPK(Long firstProvinceId, Long secondProvinceId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NeighborhoodOfProvincesPK neighborhoodId = (NeighborhoodOfProvincesPK) o;
        return firstProvinceId == neighborhoodId.firstProvinceId &&
                secondProvinceId == neighborhoodId.secondProvinceId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstProvinceId, secondProvinceId);
    }
}