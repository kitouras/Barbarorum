package com.example.servingwebcontent.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "climat")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Climat implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private int temperatureDegree;
    private int windDegree;
    private int humidityDegree;
    private int atmosphericPressureDegree;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "income_mods_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    IncomeAffectableMode incomeModsId;
}
