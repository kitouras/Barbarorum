package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class ProvinceCulturesPK implements Serializable {
    @Column(name = "province_id")
    private long provinceId;
    @Column(name = "culture_id")
    private long cultureId;

    public ProvinceCulturesPK() {
    }

    public ProvinceCulturesPK(Long provinceId, Long cultureId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvinceCulturesPK neighborhoodId = (ProvinceCulturesPK) o;
        return provinceId == neighborhoodId.cultureId &&
                cultureId == neighborhoodId.cultureId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(provinceId, cultureId);
    }
}