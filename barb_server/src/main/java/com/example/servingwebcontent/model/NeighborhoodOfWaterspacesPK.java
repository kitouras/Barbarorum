package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class NeighborhoodOfWaterspacesPK implements Serializable {
    @Column(name = "first_waterspace_id")
    private long firstWaterspaceId;
    @Column(name = "second_waterspace_id")
    private long secondWaterspaceId;

    public NeighborhoodOfWaterspacesPK() {
    }

    public NeighborhoodOfWaterspacesPK(Long firstWaterspaceId, Long secondWaterspaceId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NeighborhoodOfWaterspacesPK neighborhoodId = (NeighborhoodOfWaterspacesPK) o;
        return firstWaterspaceId == neighborhoodId.secondWaterspaceId &&
                secondWaterspaceId == neighborhoodId.secondWaterspaceId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstWaterspaceId, secondWaterspaceId);
    }
}