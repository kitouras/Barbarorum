package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class ProvinceResourcesLifecyclePK implements Serializable {
    @Column(name = "province_id")
    private long provinceId;
    @Column(name = "resource_id")
    private long resourceId;

    public ProvinceResourcesLifecyclePK() {
    }

    public ProvinceResourcesLifecyclePK(Long provinceId, Long resourceId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvinceResourcesLifecyclePK neighborhoodId = (ProvinceResourcesLifecyclePK) o;
        return provinceId == neighborhoodId.resourceId &&
                resourceId == neighborhoodId.resourceId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(provinceId, resourceId);
    }
}