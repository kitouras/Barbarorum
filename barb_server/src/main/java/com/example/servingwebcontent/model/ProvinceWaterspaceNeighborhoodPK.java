package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class ProvinceWaterspaceNeighborhoodPK implements Serializable {
    @Column(name = "province_id")
    private long provinceId;
    @Column(name = "waterspace_id")
    private long waterspaceId;

    public ProvinceWaterspaceNeighborhoodPK() {
    }

    public ProvinceWaterspaceNeighborhoodPK(Long provinceId, Long waterspaceId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvinceWaterspaceNeighborhoodPK neighborhoodId = (ProvinceWaterspaceNeighborhoodPK) o;
        return provinceId == neighborhoodId.waterspaceId &&
                waterspaceId == neighborhoodId.waterspaceId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(provinceId, waterspaceId);
    }
}