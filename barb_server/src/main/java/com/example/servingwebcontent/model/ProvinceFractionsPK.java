package com.example.servingwebcontent.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class ProvinceFractionsPK implements Serializable {
    @Column(name = "province_id")
    private long provinceId;
    @Column(name = "fraction_id")
    private long fractionId;

    public ProvinceFractionsPK() {
    }

    public ProvinceFractionsPK(Long provinceId, Long fractionId) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvinceFractionsPK neighborhoodId = (ProvinceFractionsPK) o;
        return provinceId == neighborhoodId.fractionId &&
                fractionId == neighborhoodId.fractionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(provinceId, fractionId);
    }
}