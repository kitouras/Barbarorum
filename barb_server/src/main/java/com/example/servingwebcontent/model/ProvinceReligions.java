package com.example.servingwebcontent.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "province_religions")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class ProvinceReligions implements Serializable {
    @EmbeddedId
    private ProvinceReligionsPK id;

    private double percentage;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "province_id", nullable = false, insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    Province provinceId;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "religion_id", nullable = false, insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    Religion religionId;
}