package com.example.servingwebcontent.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Table(name = "income_affectable_mode")
public class IncomeAffectableMode implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private double villageTaxMultiplier;
    private double cityTaxMultiplier;
    private double foodMultiplier;
    private double tradeMultiplier;
    private double handicraftMultiplier;
    private double foodMarketingMultiplier;
    private double tradeMarketingMultiplier;
    private double handicraftMarketingMultiplier;

}
