package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Season;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeasonRepository extends JpaRepository<Season, Long>, CustomSeasonRepository {
    List<Season> findByIncomeModsId(Long incomeModsId);
}
