package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceResourcesLifecycle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomProvinceResourcesLifecycleRepository {
    Page<ProvinceResourcesLifecycle> findAllWithFilter(Pageable pageable, Long provinceId,
                                                       Long resourceId, Integer resourceStock, Integer resourceProduction,
                                                       Integer resourceLimit, Integer resourceMarketing, String sortBy, String sortDir);
}
