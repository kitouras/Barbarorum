package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceFractions;
import com.example.servingwebcontent.model.ProvinceFractionsPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProvinceFractionsRepository extends JpaRepository<ProvinceFractions, ProvinceFractionsPK> {
    Optional<ProvinceFractions> findByIdProvinceIdAndIdFractionId
            (Long provinceId, Long fractionId);
}