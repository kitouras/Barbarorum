package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Fraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FractionRepository extends JpaRepository<Fraction, Long> {
}