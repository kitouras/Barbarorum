package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Climat;
import javax.persistence.criteria.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomClimatRepositoryImpl implements  CustomClimatRepository{
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public Page<Climat> findAllWithFilter(Pageable pageable, Long id, String name,
                                                              Integer temperatureDegree, Integer windDegree,
                                                              Integer humidityDegree, Integer atmosphericPressureDegree,
                                                              Long incomeModsId, String sortBy, String sortDir) {
        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Climat> q = cb.createQuery(Climat.class);
        Root<Climat> recipe = q.from(Climat.class);
        List<Predicate> predicates = new ArrayList<>();
        if(id != null)
            predicates.add(cb.equal(recipe.get("id"), id));
        if(name != null && !name.isEmpty())
            predicates.add(cb.equal(recipe.get("name"), name));
        if(temperatureDegree != null)
            predicates.add(cb.equal(recipe.get("temperatureDegree"), temperatureDegree));
        if(windDegree != null)
            predicates.add(cb.equal(recipe.get("windDegree"), windDegree));
        if(humidityDegree != null)
            predicates.add(cb.equal(recipe.get("humidityDegree"), humidityDegree));
        if(atmosphericPressureDegree != null)
            predicates.add(cb.equal(recipe.get("atmosphericPressureDegree"), atmosphericPressureDegree));
        if(incomeModsId != null)
            predicates.add(cb.equal(recipe.get("incomeModsId"), incomeModsId));
        q.select(recipe).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        ).orderBy(sortDir.equals("ASC") ? cb.asc(recipe.get(sortBy)): cb.desc(recipe.get(sortBy)));


        List<Climat> hits = entityManager.createQuery(q).getResultList();
        int start = (int)pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), hits.size());
        return new PageImpl<>(hits.subList(start, end), pageable, hits.size());
    }
}
