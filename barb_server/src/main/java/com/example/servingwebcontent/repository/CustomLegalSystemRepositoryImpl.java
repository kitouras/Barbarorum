package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.LegalSystem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomLegalSystemRepositoryImpl implements  CustomLegalSystemRepository{
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public Page<LegalSystem> findAllWithFilter(Pageable pageable, Long id, String groupName, Boolean isTaxFixed,
                                               Double taxAboveMultiplier, Double autonomyFactor, Long incomeModsId, String sortBy, String sortDir) {
        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<LegalSystem> q = cb.createQuery(LegalSystem.class);
        Root<LegalSystem> recipe = q.from(LegalSystem.class);
        List<Predicate> predicates = new ArrayList<>();
        if(id != null)
            predicates.add(cb.equal(recipe.get("id"), id));
        if(groupName != null && !groupName.isEmpty())
            predicates.add(cb.equal(recipe.get("groupName"), groupName));
        if(isTaxFixed != null)
            predicates.add(cb.equal(recipe.get("isTaxFixed"), isTaxFixed));
        if(taxAboveMultiplier != null)
            predicates.add(cb.equal(recipe.get("taxAboveMultiplier"), taxAboveMultiplier));
        if(autonomyFactor != null)
            predicates.add(cb.equal(recipe.get("autonomyFactor"), autonomyFactor));
        if(incomeModsId != null)
            predicates.add(cb.equal(recipe.get("incomeModsId"), incomeModsId));
        q.select(recipe).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        ).orderBy(sortDir.equals("ASC") ? cb.asc(recipe.get(sortBy)): cb.desc(recipe.get(sortBy)));

        List<LegalSystem> hits = entityManager.createQuery(q).getResultList();
        int start = (int)pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), hits.size());
        return new PageImpl<>(hits.subList(start, end), pageable, hits.size());
    }
}

