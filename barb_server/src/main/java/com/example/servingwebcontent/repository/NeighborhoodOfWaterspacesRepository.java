package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.NeighborhoodOfWaterspaces;
import com.example.servingwebcontent.model.NeighborhoodOfWaterspacesPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NeighborhoodOfWaterspacesRepository extends JpaRepository<NeighborhoodOfWaterspaces, NeighborhoodOfWaterspacesPK> {

    Optional<NeighborhoodOfWaterspaces> findByIdFirstWaterspaceIdAndIdSecondWaterspaceId
            (Long firstWaterspaceId, Long secondWaterspaceId);
}