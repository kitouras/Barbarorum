package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceResourcesLifecycle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomProvinceResourcesLifecycleRepositoryImpl implements CustomProvinceResourcesLifecycleRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<ProvinceResourcesLifecycle> findAllWithFilter(Pageable pageable, Long provinceId,
                                                              Long resourceId, Integer resourceStock, Integer resourceProduction,
                                                              Integer resourceLimit, Integer resourceMarketing, String sortBy, String sortDir) {
        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProvinceResourcesLifecycle> q = cb.createQuery(ProvinceResourcesLifecycle.class);
        Root<ProvinceResourcesLifecycle> recipe = q.from(ProvinceResourcesLifecycle.class);
        List<Predicate> predicates = new ArrayList<>();
        if(provinceId != null)
            predicates.add(cb.equal(recipe.get("provinceId"), provinceId));
        if(resourceId != null)
            predicates.add(cb.equal(recipe.get("resourceId"), resourceId));
        if(resourceStock != null)
            predicates.add(cb.equal(recipe.get("resourceStock"), resourceStock));
        if(resourceProduction != null)
            predicates.add(cb.equal(recipe.get("resourceProduction"), resourceProduction));
        if(resourceLimit != null)
            predicates.add(cb.equal(recipe.get("resourceLimit"), resourceLimit));
        if(resourceMarketing != null)
            predicates.add(cb.equal(recipe.get("resourceMarketing"), resourceMarketing));
        q.select(recipe).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        ).orderBy(sortDir.equals("ASC") ? cb.asc(recipe.get(sortBy)): cb.desc(recipe.get(sortBy)));

        List<ProvinceResourcesLifecycle> hits = entityManager.createQuery(q).getResultList();
        int start = (int)pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), hits.size());
        return new PageImpl<>(hits.subList(start, end), pageable, hits.size());
    }
}
