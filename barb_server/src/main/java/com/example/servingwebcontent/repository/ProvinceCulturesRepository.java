package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceCultures;
import com.example.servingwebcontent.model.ProvinceCulturesPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProvinceCulturesRepository extends JpaRepository<ProvinceCultures, ProvinceCulturesPK> {
    Optional<ProvinceCultures> findByIdProvinceIdAndIdCultureId
            (Long provinceId, Long cultureId);
}