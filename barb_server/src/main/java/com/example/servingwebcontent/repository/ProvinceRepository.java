package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Province;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;


@Repository
public interface ProvinceRepository extends JpaRepository<Province, Long> {
    Optional<Province> findByName(String name);
    @Query("SELECT p FROM Province p INNER JOIN p.stateId st WHERE st.name = :stateName ORDER BY p.id")
    Page<Province> findAllProvinceByStateNameWithPagination(@Param("stateName") String stateName, Pageable page);
    @Query(value = "SELECT st.name, COUNT(p.id) FROM province p " +
            "INNER JOIN state st ON p.state_id = st.id " +
            "WHERE p.treasury >= (SELECT AVG(p.treasury) FROM Province p LEFT JOIN legal_system ls ON p.legal_system_id = ls.id" +
            " WHERE ls.is_tax_fixed = TRUE)" +
            " GROUP BY st.id HAVING SUM(p.tax_into) > 20 ORDER BY st.id DESC", nativeQuery = true)
    List<Object> countAllStatesProvincesWithConditions();
    @Query(value = "SELECT st.name, SUM(p.treasury) FROM province p " +
            "INNER JOIN state st ON p.state_id = st.id " +
            "GROUP BY st.id ORDER BY st.id ASC", nativeQuery = true)
    List<Object> countStatesTreasuryByProvince();
    @Query(value = "SELECT p.name, SUM(c.popularity) + SUM(p.village_popularity) FROM province p " +
            "INNER JOIN city c ON p.city_id = c.id " +
            "GROUP BY p.name ORDER BY p.name ASC", nativeQuery = true)
    List<Object> countFullPopulationOfProvinces();


}