package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.NeighborhoodOfProvinces;
import com.example.servingwebcontent.model.NeighborhoodOfProvincesPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NeighborhoodOfProvincesRepository extends JpaRepository<NeighborhoodOfProvinces, NeighborhoodOfProvincesPK> {

    Optional<NeighborhoodOfProvinces> findByIdFirstProvinceIdAndIdSecondProvinceId
            (Long firstProvinceId, Long secondProvinceId);
}