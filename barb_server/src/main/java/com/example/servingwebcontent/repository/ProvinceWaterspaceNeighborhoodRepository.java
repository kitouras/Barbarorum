package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceWaterspaceNeighborhood;
import com.example.servingwebcontent.model.ProvinceWaterspaceNeighborhoodPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProvinceWaterspaceNeighborhoodRepository extends JpaRepository<ProvinceWaterspaceNeighborhood, ProvinceWaterspaceNeighborhoodPK> {
    Optional<ProvinceWaterspaceNeighborhood> findByIdProvinceIdAndIdWaterspaceId
            (Long provinceId, Long waterspaceId);
}