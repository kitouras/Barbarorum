package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.LegalSystem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomLegalSystemRepository {
    Page<LegalSystem> findAllWithFilter(Pageable pageable, Long id, String groupName, Boolean isTaxFixed,
                                   Double taxAboveMultiplier, Double autonomyFactor, Long incomeModsId, String sortBy, String sortDir);
}
