package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Religion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReligionRepository extends JpaRepository<Religion, Long> {
    List<Religion> findByIncomeModsId(Long incomeModsId);
}
