package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.LegalSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LegalSystemRepository extends JpaRepository<LegalSystem, Long>, CustomLegalSystemRepository {
    List<LegalSystem> findByIncomeModsId(Long incomeModsId);
}
