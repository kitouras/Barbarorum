package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.IncomeAffectableMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IncomeAffectableModeRepository extends JpaRepository<IncomeAffectableMode, Long> {
}