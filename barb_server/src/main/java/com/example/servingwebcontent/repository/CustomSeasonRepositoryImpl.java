package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Season;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomSeasonRepositoryImpl implements  CustomSeasonRepository{
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public Page<Season> findAllWithFilter(Pageable pageable, Long id, String name,
                                          Double temperatureMod, Double windMod,
                                          Double humidityMod, Double atmosphericPressureMod,
                                          Long incomeModsId, String sortBy, String sortDir) {
        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Season> q = cb.createQuery(Season.class);
        Root<Season> recipe = q.from(Season.class);
        List<Predicate> predicates = new ArrayList<>();
        if(name != null && !name.isEmpty())
            predicates.add(cb.equal(recipe.get("name"), name));
        if(id != null)
            predicates.add(cb.equal(recipe.get("id"), id));
        if(temperatureMod != null)
            predicates.add(cb.equal(recipe.get("temperatureMod"), temperatureMod));
        if(windMod != null)
            predicates.add(cb.equal(recipe.get("windMod"), windMod));
        if(humidityMod != null)
            predicates.add(cb.equal(recipe.get("humidityMod"), humidityMod));
        if(atmosphericPressureMod != null)
            predicates.add(cb.equal(recipe.get("atmosphericPressureMod"), atmosphericPressureMod));
        if(incomeModsId != null)
            predicates.add(cb.equal(recipe.get("incomeModsId"), incomeModsId));
        q.select(recipe).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        ).orderBy(sortDir.equals("ASC") ? cb.asc(recipe.get(sortBy)): cb.desc(recipe.get(sortBy)));

        List<Season> hits = entityManager.createQuery(q).getResultList();
        int start = (int)pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), hits.size());
        return new PageImpl<>(hits.subList(start, end), pageable, hits.size());
    }
}
