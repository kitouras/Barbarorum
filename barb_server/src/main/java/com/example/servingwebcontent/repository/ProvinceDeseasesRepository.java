package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceDeseases;
import com.example.servingwebcontent.model.ProvinceDeseasesPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProvinceDeseasesRepository extends JpaRepository<ProvinceDeseases, ProvinceDeseasesPK> {
    Optional<ProvinceDeseases> findByIdProvinceIdAndIdDeseaseId
            (Long provinceId, Long deseaseId);
}