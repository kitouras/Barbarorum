package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Waterspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WaterspaceRepository extends JpaRepository<Waterspace, Long> {
    List<Waterspace> findByClimatIdAndSeasonId(Long climatId, Long seasonId);
}
