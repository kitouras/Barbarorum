package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Season;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomSeasonRepository {
    Page<Season> findAllWithFilter(Pageable pageable, Long id, String name, Double temperatureMod,
                                   Double windMod, Double humidityMod, Double atmosphericPressureMod,
                                   Long incomeModsId, String sortBy, String sortDir);
}
