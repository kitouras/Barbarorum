package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.City;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    @Query("SELECT c FROM City c INNER JOIN c.province p WHERE p.name = :provinceName")
    List<City> findCityByProvinceName(@Param("provinceName") String provinceName, Pageable page);
}
