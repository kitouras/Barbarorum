package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Culture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CultureRepository extends JpaRepository<Culture, Long> {
    List<Culture> findByIncomeModsId(Long incomeModsId);
}
