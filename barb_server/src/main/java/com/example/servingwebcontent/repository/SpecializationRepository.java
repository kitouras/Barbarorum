package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecializationRepository extends JpaRepository<Specialization, Long> {
    List<Specialization> findByIncomeModsId(Long incomeModsId);
}
