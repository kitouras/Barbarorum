package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Climat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomClimatRepository {
    Page<Climat> findAllWithFilter(Pageable pageable, Long id, String name, Integer temperatureDegree,
                                                       Integer windDegree, Integer humidityDegree, Integer atmosphericPressureDegree,
                                                       Long incomeModsId, String sortBy, String sortDir);
}
