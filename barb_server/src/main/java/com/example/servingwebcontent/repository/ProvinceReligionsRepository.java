package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceReligions;
import com.example.servingwebcontent.model.ProvinceReligionsPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProvinceReligionsRepository extends JpaRepository<ProvinceReligions, ProvinceReligionsPK> {
    Optional<ProvinceReligions> findByIdProvinceIdAndIdReligionId
            (Long provinceId, Long religionId);
}