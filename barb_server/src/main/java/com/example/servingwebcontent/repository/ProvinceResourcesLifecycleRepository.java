package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.ProvinceResourcesLifecycle;
import com.example.servingwebcontent.model.ProvinceResourcesLifecyclePK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProvinceResourcesLifecycleRepository extends JpaRepository<ProvinceResourcesLifecycle, ProvinceResourcesLifecyclePK>,
CustomProvinceResourcesLifecycleRepository{
    Optional<ProvinceResourcesLifecycle> findByIdProvinceIdAndIdResourceId
            (Long provinceId, Long resourceId);


}