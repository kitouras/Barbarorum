package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Desease;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeseaseRepository extends JpaRepository<Desease, Long> {
}