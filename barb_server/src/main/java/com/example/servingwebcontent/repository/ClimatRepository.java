package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Climat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClimatRepository extends JpaRepository<Climat, Long>, CustomClimatRepository {
    List<Climat> findByIncomeModsId(Long incomeModsId);
}
