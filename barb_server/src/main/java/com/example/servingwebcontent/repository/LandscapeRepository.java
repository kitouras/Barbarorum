package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.model.Landscape;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LandscapeRepository extends JpaRepository<Landscape, Long> {
    List<Landscape> findByIncomeModsId(Long incomeModsId);
}
