package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.IncomeAffectableModeDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.IncomeAffectableMode;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class IncomeAffectableModeController {
    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/incomeAffectableMode")
    public Page<IncomeAffectableMode> getIncomeAffectableMode(Pageable pageable) {
        return incomeAffectableModeRepository.findAll(pageable);
    }

    @GetMapping("/incomeAffectableMode/{incomeAffectableModeId}")
    public IncomeAffectableMode getIncomeAffectableModeById(@PathVariable Long incomeAffectableModeId) {
        var incomeAffectableMode = incomeAffectableModeRepository.findById(incomeAffectableModeId);
        if(incomeAffectableMode.isPresent())
            return incomeAffectableMode.get();
        else throw new ResourceNotFoundException("IncomeAffectableMode not found with id " + incomeAffectableModeId);

    }

    @PostMapping("/incomeAffectableMode")
    public IncomeAffectableMode createIncomeAffectableMode(@Valid @RequestBody IncomeAffectableModeDTO incomeAffectableModeRequest) {
        var incomeAffectableMode = new IncomeAffectableMode();
        incomeAffectableMode.setName(incomeAffectableModeRequest.getName());
        incomeAffectableMode.setFoodMarketingMultiplier(incomeAffectableModeRequest.getFoodMarketingMultiplier());
        incomeAffectableMode.setCityTaxMultiplier(incomeAffectableModeRequest.getCityTaxMultiplier());
        incomeAffectableMode.setFoodMultiplier(incomeAffectableModeRequest.getFoodMultiplier());
        incomeAffectableMode.setHandicraftMarketingMultiplier(incomeAffectableModeRequest.getHandicraftMarketingMultiplier());
        incomeAffectableMode.setVillageTaxMultiplier(incomeAffectableModeRequest.getVillageTaxMultiplier());
        incomeAffectableMode.setHandicraftMultiplier(incomeAffectableModeRequest.getHandicraftMultiplier());
        incomeAffectableMode.setTradeMultiplier(incomeAffectableModeRequest.getTradeMultiplier());
        incomeAffectableMode.setTradeMarketingMultiplier(incomeAffectableModeRequest.getTradeMarketingMultiplier());
        return incomeAffectableModeRepository.save(incomeAffectableMode);
    }

    @PutMapping("/incomeAffectableMode/{incomeAffectableModeId}")
    public IncomeAffectableMode updateIncomeAffectableMode(@PathVariable Long incomeAffectableModeId,
                                 @Valid @RequestBody IncomeAffectableModeDTO incomeAffectableModeRequest) {
        return incomeAffectableModeRepository.findById(incomeAffectableModeId)
                .map(incomeAffectableMode -> {
                    incomeAffectableMode.setName(incomeAffectableModeRequest.getName());
                    incomeAffectableMode.setFoodMarketingMultiplier(incomeAffectableModeRequest.getFoodMarketingMultiplier());
                    incomeAffectableMode.setCityTaxMultiplier(incomeAffectableModeRequest.getCityTaxMultiplier());
                    incomeAffectableMode.setFoodMultiplier(incomeAffectableModeRequest.getFoodMultiplier());
                    incomeAffectableMode.setHandicraftMarketingMultiplier(incomeAffectableModeRequest.getHandicraftMarketingMultiplier());
                    incomeAffectableMode.setVillageTaxMultiplier(incomeAffectableModeRequest.getVillageTaxMultiplier());
                    incomeAffectableMode.setHandicraftMultiplier(incomeAffectableModeRequest.getHandicraftMultiplier());
                    incomeAffectableMode.setTradeMultiplier(incomeAffectableModeRequest.getTradeMultiplier());
                    incomeAffectableMode.setTradeMarketingMultiplier(incomeAffectableModeRequest.getTradeMarketingMultiplier());
                    return incomeAffectableModeRepository.save(incomeAffectableMode);
                }).orElseThrow(() -> new ResourceNotFoundException("Desease not found with id " + incomeAffectableModeId));
    }


    @DeleteMapping("/incomeAffectableMode/{incomeAffectableModeId}")
    public ResponseEntity<Object> deleteDesease(@PathVariable Long incomeAffectableModeId) {
        return incomeAffectableModeRepository.findById(incomeAffectableModeId)
                .map(incomeAffectableMode -> {
                    incomeAffectableModeRepository.delete(incomeAffectableMode);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("income_affectable_mode not found with id " + incomeAffectableModeId));
    }
}
