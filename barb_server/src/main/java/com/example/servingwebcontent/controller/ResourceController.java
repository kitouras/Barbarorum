package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ResourceDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Resource;
import com.example.servingwebcontent.repository.ResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ResourceController {

    @Autowired
    private ResourceRepository resourceRepository;

    @GetMapping("/resource")
    public Page<Resource> getResource(Pageable pageable) {
        return resourceRepository.findAll(pageable);
    }

    @GetMapping("/resource/{resourceId}")
    public Resource getResourceById(@PathVariable Long resourceId) {
        var resource = resourceRepository.findById(resourceId);
        if(resource.isPresent())
            return resource.get();
        else throw new ResourceNotFoundException("Resource not found with id " + resourceId);

    }
    
    @PostMapping("/resource")
    public Resource createResource(@Valid @RequestBody ResourceDTO resourceRequest) {
        var resource = new Resource();
        resource.setName(resourceRequest.getName());
        resource.setPopulationGrowthMultiplier(resourceRequest.getPopulationGrowthMultiplier());
        return resourceRepository.save(resource);
    }

    @PutMapping("/resource/{resourceId}")
    public Resource updateResource(@PathVariable Long resourceId,
                           @Valid @RequestBody ResourceDTO resourceRequest) {
        return resourceRepository.findById(resourceId)
                .map(resource -> {
                    resource.setName(resourceRequest.getName());
                    resource.setPopulationGrowthMultiplier(resourceRequest.getPopulationGrowthMultiplier());
                    return resourceRepository.save(resource);
                }).orElseThrow(() -> new ResourceNotFoundException("Resource not found with id " + resourceId));
    }


    @DeleteMapping("/resource/{resourceId}")
    public ResponseEntity<Object> deleteResource(@PathVariable Long resourceId) {
        return resourceRepository.findById(resourceId)
                .map(resource -> {
                    resourceRepository.delete(resource);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Resource not found with id " + resourceId));
    }
}