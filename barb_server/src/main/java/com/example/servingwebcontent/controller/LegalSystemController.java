package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.LegalSystemDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.LegalSystem;
import com.example.servingwebcontent.repository.LegalSystemRepository;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class LegalSystemController {

    @Autowired
    private LegalSystemRepository legalSystemRepository;

    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/legalSystem")
    public Page<LegalSystem> getLegalSystem(Pageable pageable,
                                            @RequestParam(defaultValue = "id") String sortBy,
                                            @RequestParam(defaultValue = "ASC") String sortDir,
                                            @RequestParam(required = false) Long id,
                                            @RequestParam(required = false) String groupName,
                                            @RequestParam(required = false) Boolean isTaxFixed,
                                            @RequestParam(required = false) Double taxAboveMultiplier,
                                            @RequestParam(required = false) Double autonomyFactor,
                                            @RequestParam(required = false) Long incomeModsId) {

        return legalSystemRepository.findAllWithFilter(pageable,
                id, groupName, isTaxFixed, taxAboveMultiplier, autonomyFactor, incomeModsId, sortBy, sortDir);
    }

    @GetMapping("/legalSystem/{legalSystemId}")
    public LegalSystem getLegalSystemById(@PathVariable Long legalSystemId) {
        var legalSystem = legalSystemRepository.findById(legalSystemId);
        if(legalSystem.isPresent())
            return legalSystem.get();
        else throw new ResourceNotFoundException("LegalSystem not found with id " + legalSystemId);

    }

    @PostMapping("/legalSystem")
    public LegalSystem createLegalSystem(@Valid @RequestBody LegalSystemDTO legalSystemRequest) {
        var legalSystem = new LegalSystem();
        return incomeAffectableModeRepository.findById(legalSystemRequest.getIncomeModsId())
                .map(incomeMods -> {
                    legalSystem.setIncomeModsId(incomeMods);
                    legalSystem.setGroupName(legalSystemRequest.getGroupName());
                    legalSystem.setTaxFixed(legalSystemRequest.isTaxFixed());
                    legalSystem.setTaxAboveMultiplier(legalSystemRequest.getTaxAboveMultiplier());
                    legalSystem.setAutonomyFactor(legalSystemRequest.getAutonomyFactor());
                    return legalSystemRepository.save(legalSystem);
                }).orElseThrow(() -> new ResourceNotFoundException("IncomeMods not found with id "
                        + legalSystemRequest.getIncomeModsId()));
    }

    @PutMapping("/legalSystem/{legalSystemId}")
    public LegalSystem updateLegalSystem(@PathVariable Long legalSystemId,
                                     @Valid @RequestBody LegalSystemDTO legalSystemRequest) {
        if(!incomeAffectableModeRepository.existsById(legalSystemRequest.getIncomeModsId())) {
            throw new ResourceNotFoundException("IncomeMods not found with id " + legalSystemRequest.getIncomeModsId());
        }
        return legalSystemRepository.findById(legalSystemId)
                .map(legalSystem -> {
                    legalSystem.setGroupName(legalSystemRequest.getGroupName());
                    legalSystem.setTaxFixed(legalSystemRequest.isTaxFixed());
                    legalSystem.setTaxAboveMultiplier(legalSystemRequest.getTaxAboveMultiplier());
                    legalSystem.setAutonomyFactor(legalSystemRequest.getAutonomyFactor());
                    legalSystem.setIncomeModsId(incomeAffectableModeRepository.findById(legalSystemRequest.getIncomeModsId()).get());
                    return legalSystemRepository.save(legalSystem);
                }).orElseThrow(() -> new ResourceNotFoundException("LegalSystem not found with id " + legalSystemId));
    }


    @DeleteMapping("/legalSystem/{legalSystemId}")
    public ResponseEntity<Object> deleteLegalSystem(@PathVariable Long legalSystemId) {
        return legalSystemRepository.findById(legalSystemId)
                .map(legalSystem -> {
                    legalSystemRepository.delete(legalSystem);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("LegalSystem not found with id " + legalSystemId));
    }
}
