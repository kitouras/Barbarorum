package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.SpecializationDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Specialization;
import com.example.servingwebcontent.repository.SpecializationRepository;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class SpecializationController {

    @Autowired
    private SpecializationRepository specializationRepository;

    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/specialization")
    public Page<Specialization> getSpecialization(Pageable pageable) {
        return specializationRepository.findAll(pageable);
    }

    @GetMapping("/specialization/{specializationId}")
    public Specialization getSpecializationById(@PathVariable Long specializationId) {
        var specialization = specializationRepository.findById(specializationId);
        if(specialization.isPresent())
            return specialization.get();
        else throw new ResourceNotFoundException("Specialization not found with id " + specializationId);

    }

    @PostMapping("/specialization")
    public Specialization createSpecialization(@Valid @RequestBody SpecializationDTO specializationRequest) {
        var specialization = new Specialization();
        return incomeAffectableModeRepository.findById(specializationRequest.getIncomeModsId())
                .map(incomeMods -> {
                    specialization.setIncomeModsId(incomeMods);
                    specialization.setName(specializationRequest.getName());
                    return specializationRepository.save(specialization);
                }).orElseThrow(() -> new ResourceNotFoundException("IncomeMods not found with id "
                        + specializationRequest.getIncomeModsId()));
    }

    @PutMapping("/specialization/{specializationId}")
    public Specialization updateSpecialization(@PathVariable Long specializationId,
                                     @Valid @RequestBody SpecializationDTO specializationRequest) {
        if(!incomeAffectableModeRepository.existsById(specializationRequest.getIncomeModsId())) {
            throw new ResourceNotFoundException("IncomeMods not found with id " + specializationRequest.getIncomeModsId());
        }
        return specializationRepository.findById(specializationId)
                .map(specialization -> {
                    specialization.setName(specializationRequest.getName());
                    specialization.setIncomeModsId(incomeAffectableModeRepository.findById(
                            specializationRequest.getIncomeModsId()).get());
                    return specializationRepository.save(specialization);
                }).orElseThrow(() -> new ResourceNotFoundException("Specialization not found with id " + specializationId));
    }


    @DeleteMapping("/specialization/{specializationId}")
    public ResponseEntity<Object> deleteSpecialization(@PathVariable Long specializationId) {
        return specializationRepository.findById(specializationId)
                .map(specialization -> {
                    specializationRepository.delete(specialization);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Specialization not found with id " + specializationId));
    }
}
