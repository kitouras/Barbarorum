package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.LandscapeDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Landscape;
import com.example.servingwebcontent.repository.LandscapeRepository;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class LandscapeController {

    @Autowired
    private LandscapeRepository landscapeRepository;

    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/landscape")
    public Page<Landscape> getLandscape(Pageable pageable) {
        return landscapeRepository.findAll(pageable);
    }

    @GetMapping("/landscape/{landscapeId}")
    public Landscape getLandscapeById(@PathVariable Long landscapeId) {
        var landscape = landscapeRepository.findById(landscapeId);
        if(landscape.isPresent())
            return landscape.get();
        else throw new ResourceNotFoundException("Landscape not found with id " + landscapeId);

    }

    @PostMapping("/landscape")
    public Landscape createLandscape(@Valid @RequestBody LandscapeDTO landscapeRequest) {
        var landscape = new Landscape();
        return incomeAffectableModeRepository.findById(landscapeRequest.getIncomeModsId())
                .map(incomeMods -> {
                    landscape.setIncomeModsId(incomeMods);
                    landscape.setName(landscapeRequest.getName());
                    landscape.setMountainousness(landscapeRequest.getMountainousness());
                    return landscapeRepository.save(landscape);
                }).orElseThrow(() -> new ResourceNotFoundException("IncomeMods not found with id "
                        + landscapeRequest.getIncomeModsId()));
    }

    @PutMapping("/landscape/{landscapeId}")
    public Landscape updateLandscape(@PathVariable Long landscapeId,
                                 @Valid @RequestBody LandscapeDTO landscapeRequest) {
        if(!incomeAffectableModeRepository.existsById(landscapeRequest.getIncomeModsId())) {
            throw new ResourceNotFoundException("IncomeMods not found with id " + landscapeRequest.getIncomeModsId());
        }
        return landscapeRepository.findById(landscapeId)
                .map(landscape -> {
                    landscape.setName(landscapeRequest.getName());
                    landscape.setMountainousness(landscapeRequest.getMountainousness());
                    landscape.setIncomeModsId(incomeAffectableModeRepository.findById(landscapeRequest.getIncomeModsId()).get());
                    return landscapeRepository.save(landscape);
                }).orElseThrow(() -> new ResourceNotFoundException("Landscape not found with id " + landscapeId));
    }


    @DeleteMapping("/landscape/{landscapeId}")
    public ResponseEntity<Object> deleteLandscape(@PathVariable Long landscapeId) {
        return landscapeRepository.findById(landscapeId)
                .map(landscape -> {
                    landscapeRepository.delete(landscape);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Landscape not found with id " + landscapeId));
    }
}
