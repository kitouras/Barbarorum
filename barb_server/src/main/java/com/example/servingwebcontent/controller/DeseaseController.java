package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.DeseaseDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Desease;
import com.example.servingwebcontent.repository.DeseaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class DeseaseController {

    @Autowired
    private DeseaseRepository deseaseRepository;

    @GetMapping("/desease")
    public Page<Desease> getDeseases(Pageable pageable) {
        return deseaseRepository.findAll(pageable);
    }

    @GetMapping("/desease/{deseaseId}")
    public Desease getDeseaseById(@PathVariable Long deseaseId) {
        var desease = deseaseRepository.findById(deseaseId);
        if(desease.isPresent())
            return desease.get();
        else throw new ResourceNotFoundException("Desease not found with id " + deseaseId);

    }


    @PostMapping("/desease")
    public Desease createDesease(@Valid @RequestBody DeseaseDTO deseaseRequest) {
        var desease = new Desease();
        desease.setName(deseaseRequest.getName());
        desease.setIncidenceRate(deseaseRequest.getIncidenceRate());
        desease.setMortality(deseaseRequest.getMortality());
        return deseaseRepository.save(desease);
    }

    @PutMapping("/desease/{deseaseId}")
    public Desease updateDesease(@PathVariable Long deseaseId,
                                   @Valid @RequestBody DeseaseDTO deseaseRequest) {
        return deseaseRepository.findById(deseaseId)
                .map(desease -> {
                    desease.setName(deseaseRequest.getName());
                    desease.setIncidenceRate(deseaseRequest.getIncidenceRate());
                    desease.setMortality(deseaseRequest.getMortality());
                    return deseaseRepository.save(desease);
                }).orElseThrow(() -> new ResourceNotFoundException("Desease not found with id " + deseaseId));
    }


    @DeleteMapping("/desease/{deseaseId}")
    public ResponseEntity<Object> deleteDesease(@PathVariable Long deseaseId) {
        return deseaseRepository.findById(deseaseId)
                .map(desease -> {
                    deseaseRepository.delete(desease);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Desease not found with id " + deseaseId));
    }
}
