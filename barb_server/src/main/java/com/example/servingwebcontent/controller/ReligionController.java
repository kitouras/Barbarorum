package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ReligionDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Religion;
import com.example.servingwebcontent.repository.ReligionRepository;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ReligionController {

    @Autowired
    private ReligionRepository religionRepository;

    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/religion")
    public Page<Religion> getReligion(Pageable pageable) {
        return religionRepository.findAll(pageable);
    }

    @GetMapping("/religion/{religionId}")
    public Religion getReligionById(@PathVariable Long religionId) {
        var religion = religionRepository.findById(religionId);
        if(religion.isPresent())
            return religion.get();
        else throw new ResourceNotFoundException("Religion not found with id " + religionId);

    }

    @PostMapping("/religion")
    public Religion createReligion(@Valid @RequestBody ReligionDTO religionRequest) {
        var religion = new Religion();
        return incomeAffectableModeRepository.findById(religionRequest.getIncomeModsId())
                .map(incomeMods -> {
                    religion.setIncomeModsId(incomeMods);
                    religion.setName(religionRequest.getName());
                    religion.setTolerance(religionRequest.getTolerance());
                    return religionRepository.save(religion);
                }).orElseThrow(() -> new ResourceNotFoundException("IncomeMods not found with id "
                        + religionRequest.getIncomeModsId()));
    }

    @PutMapping("/religion/{religionId}")
    public Religion updateReligion(@PathVariable Long religionId,
                                 @Valid @RequestBody ReligionDTO religionRequest) {
        if(!incomeAffectableModeRepository.existsById(religionRequest.getIncomeModsId())) {
            throw new ResourceNotFoundException("IncomeMods not found with id " + religionRequest.getIncomeModsId());
        }
        return religionRepository.findById(religionId)
                .map(religion -> {
                    religion.setName(religionRequest.getName());
                    religion.setTolerance(religionRequest.getTolerance());
                    religion.setIncomeModsId(incomeAffectableModeRepository.findById(religionRequest.getIncomeModsId()).get());
                    return religionRepository.save(religion);
                }).orElseThrow(() -> new ResourceNotFoundException("Religion not found with id " + religionId));
    }


    @DeleteMapping("/religion/{religionId}")
    public ResponseEntity<Object> deleteReligion(@PathVariable Long religionId) {
        return religionRepository.findById(religionId)
                .map(religion -> {
                    religionRepository.delete(religion);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Religion not found with id " + religionId));
    }
}
