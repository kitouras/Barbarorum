package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.auth.UserAuthenticationService;

import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.User;
import com.example.servingwebcontent.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@RestController
@RequestMapping("/api/public/users")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
final class PublicUsersController {
    @NonNull
    UserAuthenticationService authentication;
    @NonNull
    UserRepository users;

    @PostMapping("/register")
    public String register(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password) {
        users
                .save(
                        User
                                .builder()
                                .username(username)
                                .password(password)
                                .build()
                );

        return login(username, password);
    }
    @PostMapping("/find_by_token")
    public User findByToken(@RequestParam("token") final String token)
    {
        var user = authentication.findByToken(token);
        if(user.isPresent())
            return user.get();
        else throw new ResourceNotFoundException("User not found with this token");

    }
    @PostMapping("/login")
    public String login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password) {
        return authentication
                .login(username, password)
                .orElseThrow(() -> new RuntimeException("invalid login and/or password"));
    }
}
