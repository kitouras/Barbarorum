package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.NeighborhoodOfProvincesDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.NeighborhoodOfProvinces;
import com.example.servingwebcontent.model.NeighborhoodOfProvincesPK;
import com.example.servingwebcontent.repository.NeighborhoodOfProvincesRepository;
import com.example.servingwebcontent.repository.ProvinceRepository;
import com.example.servingwebcontent.repository.RelationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class NeighborhoodOfProvincesController {
    private static final Logger log = LoggerFactory.getLogger(NeighborhoodOfProvincesController.class);
    private static final String PROVINCE_NOT_FOUND_WITH_ID = "Province not found with id ";

    @Autowired
    private NeighborhoodOfProvincesRepository neighborhoodOfProvincesRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private RelationRepository relationRepository;

    @GetMapping("/neighborhoodOfProvinces")
    public Page<NeighborhoodOfProvinces> getNeighborhoodOfProvinces(Pageable pageable) {
        return neighborhoodOfProvincesRepository.findAll(pageable);
    }

    @GetMapping("/neighborhoodOfProvinces/{firstProvinceId}_{secondProvinceId}")
    public NeighborhoodOfProvinces getNeighborhoodOfProvincesById(@PathVariable Long firstProvinceId,
                                                            @PathVariable Long secondProvinceId) {
        if(neighborhoodOfProvincesRepository.findByIdFirstProvinceIdAndIdSecondProvinceId(firstProvinceId, secondProvinceId).isPresent())
            return neighborhoodOfProvincesRepository.findByIdFirstProvinceIdAndIdSecondProvinceId(firstProvinceId, secondProvinceId).get();
        else throw new ResourceNotFoundException("NeighborhoodOfProvinces not found with ids "
                + firstProvinceId + " and " + secondProvinceId);
    }


    @PostMapping("/neighborhoodOfProvinces")
    public NeighborhoodOfProvinces createNeighborhoodOfProvinces(@Valid @RequestBody NeighborhoodOfProvincesDTO
                                                                             neighborhoodOfProvincesRequest) {
        if(!provinceRepository.existsById(neighborhoodOfProvincesRequest.getFirstProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + neighborhoodOfProvincesRequest.getFirstProvinceId());
        }
        if(!provinceRepository.existsById(neighborhoodOfProvincesRequest.getSecondProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + neighborhoodOfProvincesRequest.getSecondProvinceId());
        }
        if(!relationRepository.existsById(neighborhoodOfProvincesRequest.getRelationId())) {
            throw new ResourceNotFoundException("Relation not found with id " + neighborhoodOfProvincesRequest.getRelationId());
        }
        var neighborhoodOfProvinces = new NeighborhoodOfProvinces();
        neighborhoodOfProvinces.setRelationId(relationRepository.findById(neighborhoodOfProvincesRequest.getRelationId()).get());
        neighborhoodOfProvinces.setFirstProvinceId(provinceRepository.findById(neighborhoodOfProvincesRequest.getFirstProvinceId()).get());
        neighborhoodOfProvinces.setSecondProvinceId(provinceRepository.findById(neighborhoodOfProvincesRequest.getSecondProvinceId()).get());
        neighborhoodOfProvinces.setDegree(neighborhoodOfProvincesRequest.getDegree());
        var neighborhoodOfProvincePK = new NeighborhoodOfProvincesPK();
        neighborhoodOfProvincePK.setFirstProvinceId(neighborhoodOfProvincesRequest.getFirstProvinceId());
        neighborhoodOfProvincePK.setSecondProvinceId(neighborhoodOfProvincesRequest.getSecondProvinceId());
        neighborhoodOfProvinces.setId(neighborhoodOfProvincePK);
        return neighborhoodOfProvincesRepository.save(neighborhoodOfProvinces);
    }

    @PutMapping("/neighborhoodOfProvinces/{firstProvinceId}_{secondProvinceId}")
    public NeighborhoodOfProvinces updateNeighborhoodOfProvinces(@PathVariable Long firstProvinceId, @PathVariable Long secondProvinceId,
                               @Valid @RequestBody NeighborhoodOfProvincesDTO neighborhoodOfProvincesRequest) {
        if(!provinceRepository.existsById(firstProvinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + firstProvinceId);
        }
        if(!provinceRepository.existsById(secondProvinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + secondProvinceId);
        }
        if(!relationRepository.existsById(neighborhoodOfProvincesRequest.getRelationId())) {
            throw new ResourceNotFoundException("Relation not found with id " + neighborhoodOfProvincesRequest.getRelationId());
        }
        return neighborhoodOfProvincesRepository.findByIdFirstProvinceIdAndIdSecondProvinceId(
                firstProvinceId, secondProvinceId)
                .map(neighborhoodOfProvinces -> {
                    neighborhoodOfProvinces.setDegree(neighborhoodOfProvincesRequest.getDegree());
                    neighborhoodOfProvinces.setRelationId(relationRepository.findById(
                            neighborhoodOfProvincesRequest.getRelationId()).get());
                    return neighborhoodOfProvincesRepository.save(neighborhoodOfProvinces);
                }).orElseThrow(() -> new ResourceNotFoundException("NeighborhoodOfProvinces not found with ids "
                        + firstProvinceId + " and " + secondProvinceId));
    }

    @DeleteMapping("/neighborhoodOfProvinces/{firstProvinceId}_{secondProvinceId}")
    public ResponseEntity<Object> deleteNeighborhoodOfProvinces(@PathVariable Long firstProvinceId,
                                                             @PathVariable Long secondProvinceId) {
        log.info("Deleting neighborhoodOfProvinces with firstProvinceId={} and secondProvinceId={}",
                firstProvinceId, secondProvinceId);
        if(!provinceRepository.existsById(firstProvinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + firstProvinceId);
        }
        if(!provinceRepository.existsById(secondProvinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + secondProvinceId);
        }

        return neighborhoodOfProvincesRepository.findByIdFirstProvinceIdAndIdSecondProvinceId(firstProvinceId, secondProvinceId)
                .map(neighborhoodOfProvinces -> {
                    neighborhoodOfProvincesRepository.delete(neighborhoodOfProvinces);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("NeighborhoodOfProvinces not found with ids "
                        + firstProvinceId + " and " + secondProvinceId));
    }
}
