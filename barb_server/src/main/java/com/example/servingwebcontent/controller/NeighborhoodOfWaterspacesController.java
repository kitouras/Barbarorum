package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.NeighborhoodOfWaterspacesDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.NeighborhoodOfWaterspaces;
import com.example.servingwebcontent.model.NeighborhoodOfWaterspacesPK;
import com.example.servingwebcontent.repository.NeighborhoodOfWaterspacesRepository;
import com.example.servingwebcontent.repository.WaterspaceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class NeighborhoodOfWaterspacesController {
    private static final Logger log = LoggerFactory.getLogger(NeighborhoodOfWaterspacesController.class);
    private static final String WATERSPACE_NOT_FOUND_WITH_ID = "Waterspace not found with id ";

    @Autowired
    private NeighborhoodOfWaterspacesRepository neighborhoodOfWaterspacesRepository;

    @Autowired
    private WaterspaceRepository waterspaceRepository;

    @GetMapping("/neighborhoodOfWaterspaces")
    public Page<NeighborhoodOfWaterspaces> getNeighborhoodOfWaterspaces(Pageable pageable) {
        return neighborhoodOfWaterspacesRepository.findAll(pageable);
    }

    @GetMapping("/neighborhoodOfWaterspaces/{firstWaterspaceId}_{secondWaterspaceId}")
    public NeighborhoodOfWaterspaces getNeighborhoodOfWaterspacesById(@PathVariable Long firstWaterspaceId,
                                                                  @PathVariable Long secondWaterspaceId) {
        if(neighborhoodOfWaterspacesRepository.findByIdFirstWaterspaceIdAndIdSecondWaterspaceId(firstWaterspaceId, secondWaterspaceId).isPresent())
            return neighborhoodOfWaterspacesRepository.findByIdFirstWaterspaceIdAndIdSecondWaterspaceId(firstWaterspaceId, secondWaterspaceId).get();
        else throw new ResourceNotFoundException("NeighborhoodOfWaterspaces not found with ids "
                + firstWaterspaceId + " and " + secondWaterspaceId);
    }


    @PostMapping("/neighborhoodOfWaterspaces")
    public NeighborhoodOfWaterspaces createNeighborhoodOfWaterspaces(@Valid @RequestBody NeighborhoodOfWaterspacesDTO
                                                                                 neighborhoodOfWaterspacesRequest) {
        if(!waterspaceRepository.existsById(neighborhoodOfWaterspacesRequest.getFirstWaterspaceId())) {
            throw new ResourceNotFoundException(WATERSPACE_NOT_FOUND_WITH_ID +
                    neighborhoodOfWaterspacesRequest.getFirstWaterspaceId());
        }
        if(!waterspaceRepository.existsById(neighborhoodOfWaterspacesRequest.getSecondWaterspaceId())) {
            throw new ResourceNotFoundException(WATERSPACE_NOT_FOUND_WITH_ID +
                    neighborhoodOfWaterspacesRequest.getSecondWaterspaceId());
        }
        var neighborhoodOfWaterspaces = new NeighborhoodOfWaterspaces();
        neighborhoodOfWaterspaces.setFirstWaterspaceId(
                waterspaceRepository.findById(neighborhoodOfWaterspacesRequest.getFirstWaterspaceId()).get());
        neighborhoodOfWaterspaces.setSecondWaterspaceId(
                waterspaceRepository.findById(neighborhoodOfWaterspacesRequest.getSecondWaterspaceId()).get());
        var neighborhoodOfWaterspacesPK = new NeighborhoodOfWaterspacesPK();
        neighborhoodOfWaterspacesPK.setFirstWaterspaceId(neighborhoodOfWaterspacesRequest.getFirstWaterspaceId());
        neighborhoodOfWaterspacesPK.setSecondWaterspaceId(neighborhoodOfWaterspacesRequest.getSecondWaterspaceId());
        neighborhoodOfWaterspaces.setId(neighborhoodOfWaterspacesPK);
        return neighborhoodOfWaterspacesRepository.save(neighborhoodOfWaterspaces);
    }

    @DeleteMapping("/neighborhoodOfWaterspaces/{firstWaterspaceId}_{secondWaterspaceId}")
    public ResponseEntity<Object> deleteNeighborhoodOfWaterspaces(@PathVariable Long firstWaterspaceId,
                                                             @PathVariable Long secondWaterspaceId) {
        log.info("Deleting neighborhoodOfWaterspaces with firstWaterspaceId={} and secondWaterspaceId={}",
                firstWaterspaceId, secondWaterspaceId);
        if(!waterspaceRepository.existsById(firstWaterspaceId)) {
            throw new ResourceNotFoundException(WATERSPACE_NOT_FOUND_WITH_ID + firstWaterspaceId);
        }
        if(!waterspaceRepository.existsById(secondWaterspaceId)) {
            throw new ResourceNotFoundException(WATERSPACE_NOT_FOUND_WITH_ID + secondWaterspaceId);
        }

        return neighborhoodOfWaterspacesRepository.findByIdFirstWaterspaceIdAndIdSecondWaterspaceId(firstWaterspaceId, secondWaterspaceId)
                .map(neighborhoodOfWaterspaces -> {
                    neighborhoodOfWaterspacesRepository.delete(neighborhoodOfWaterspaces);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("NeighborhoodOfWaterspaces not found with ids "
                        + firstWaterspaceId + " and " + secondWaterspaceId));
    }
}
