package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ProvinceFractionsDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.ProvinceFractionsPK;
import com.example.servingwebcontent.model.ProvinceFractions;
import com.example.servingwebcontent.repository.FractionRepository;
import com.example.servingwebcontent.repository.ProvinceFractionsRepository;
import com.example.servingwebcontent.repository.ProvinceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ProvinceFractionsController {
    private static final Logger log = LoggerFactory.getLogger(ProvinceFractionsController.class);
    private static final String PROVINCE_NOT_FOUND_WITH_ID = "Province not found with id ";
    private static final String FRACTION_NOT_FOUND_WITH_ID = "Fraction not found with id ";

    @Autowired
    private ProvinceFractionsRepository provinceFractionsRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private FractionRepository fractionRepository;

    @GetMapping("/provinceFractions")
    public Page<ProvinceFractions> getProvinceFractions(Pageable pageable) {
        return provinceFractionsRepository.findAll(pageable);
    }

    @GetMapping("/provinceFractions/{provinceId}_{fractionId}")
    public ProvinceFractions getProvinceFractionsById(@PathVariable Long provinceId,
                                                      @PathVariable Long fractionId) {
        if(provinceFractionsRepository.findByIdProvinceIdAndIdFractionId(provinceId, fractionId).isPresent())
            return provinceFractionsRepository.findByIdProvinceIdAndIdFractionId(provinceId, fractionId).get();
        else throw new ResourceNotFoundException("ProvinceFractions not found with ids "
                + provinceId + " and " + fractionId);
    }


    @PostMapping("/provinceFractions")
    public ProvinceFractions createProvinceFractions(@Valid @RequestBody ProvinceFractionsDTO provinceFractionsRequest) {
        if(!provinceRepository.existsById(provinceFractionsRequest.getProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceFractionsRequest.getProvinceId());
        }
        if(!fractionRepository.existsById(provinceFractionsRequest.getFractionId())) {
            throw new ResourceNotFoundException(FRACTION_NOT_FOUND_WITH_ID + provinceFractionsRequest.getFractionId());
        }
        var provinceFractions = new ProvinceFractions();
        provinceFractions.setProvinceId(provinceRepository.findById(provinceFractionsRequest.getProvinceId()).get());
        provinceFractions.setFractionId(fractionRepository.findById(provinceFractionsRequest.getFractionId()).get());
        provinceFractions.setIntegrity(provinceFractionsRequest.getIntegrity());
        provinceFractions.setAuthority(provinceFractionsRequest.getAuthority());

        var provinceFractionsPK = new ProvinceFractionsPK();
        provinceFractionsPK.setProvinceId(provinceFractionsRequest.getProvinceId());
        provinceFractionsPK.setFractionId(provinceFractionsRequest.getFractionId());
        provinceFractions.setId(provinceFractionsPK);
        
        return provinceFractionsRepository.save(provinceFractions);
    }

    @PutMapping("/provinceFractions/{provinceId}_{fractionId}")
    public ProvinceFractions updateProvinceFractions(@PathVariable Long provinceId, @PathVariable Long fractionId,
                                                   @Valid @RequestBody ProvinceFractionsDTO provinceFractionsRequest) {
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!fractionRepository.existsById(fractionId)) {
            throw new ResourceNotFoundException(FRACTION_NOT_FOUND_WITH_ID + fractionId);
        }
        return provinceFractionsRepository.findByIdProvinceIdAndIdFractionId(
                provinceId, fractionId)
                .map(provinceFractions -> {
                    provinceFractions.setIntegrity(provinceFractionsRequest.getIntegrity());
                    provinceFractions.setAuthority(provinceFractionsRequest.getAuthority());
                    return provinceFractionsRepository.save(provinceFractions);
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceFractions not found with ids "
                        + provinceId + " and " + fractionId));
    }

    @DeleteMapping("/provinceFractions/{provinceId}_{fractionId}")
    public ResponseEntity<Object> deleteProvinceFractions(@PathVariable Long provinceId,
                                                    @PathVariable Long fractionId) {
        log.info("Deleting provinceFractions with provinceId={}and fractionId={}", provinceId, fractionId);
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!fractionRepository.existsById(fractionId)) {
            throw new ResourceNotFoundException(FRACTION_NOT_FOUND_WITH_ID + fractionId);
        }

        return provinceFractionsRepository.findByIdProvinceIdAndIdFractionId(provinceId, fractionId)
                .map(provinceFractions -> {
                    provinceFractionsRepository.delete(provinceFractions);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceFractions not found with ids "
                        + provinceId + " and " + fractionId));
    }
}
