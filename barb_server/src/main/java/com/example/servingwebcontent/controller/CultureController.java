package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.CultureDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Culture;
import com.example.servingwebcontent.repository.CultureRepository;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class CultureController {

    @Autowired
    private CultureRepository cultureRepository;

    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/culture")
    public Page<Culture> getCulture(Pageable pageable) {
        return cultureRepository.findAll(pageable);
    }

    @GetMapping("/culture/{cultureId}")
    public Culture getCultureById(@PathVariable Long cultureId) {
        var culture = cultureRepository.findById(cultureId);
        if(culture.isPresent())
            return culture.get();
        else throw new ResourceNotFoundException("Culture not found with id " + cultureId);

    }
    @PostMapping("/culture")
    public Culture createCulture(@Valid @RequestBody CultureDTO cultureRequest) {
        var culture = new Culture();
        return incomeAffectableModeRepository.findById(cultureRequest.getIncomeModsId())
                .map(incomeMods -> {
                    culture.setIncomeModsId(incomeMods);
                    culture.setName(cultureRequest.getName());
                    culture.setTolerance(cultureRequest.getTolerance());
                    return cultureRepository.save(culture);
                }).orElseThrow(() -> new ResourceNotFoundException("IncomeMods not found with id "
                        + cultureRequest.getIncomeModsId()));
    }

    @PutMapping("/culture/{cultureId}")
    public Culture updateCulture(@PathVariable Long cultureId,
                               @Valid @RequestBody CultureDTO cultureRequest) {
        if(!incomeAffectableModeRepository.existsById(cultureRequest.getIncomeModsId())) {
            throw new ResourceNotFoundException("IncomeMods not found with id " + cultureRequest.getIncomeModsId());
        }
        return cultureRepository.findById(cultureId)
                .map(culture -> {
                    culture.setName(cultureRequest.getName());
                    culture.setTolerance(cultureRequest.getTolerance());
                    culture.setIncomeModsId(incomeAffectableModeRepository.findById(cultureRequest.getIncomeModsId()).get());
                    return cultureRepository.save(culture);
                }).orElseThrow(() -> new ResourceNotFoundException("Culture not found with id " + cultureId));
    }


    @DeleteMapping("/culture/{cultureId}")
    public ResponseEntity<Object> deleteCulture(@PathVariable Long cultureId) {
        return cultureRepository.findById(cultureId)
                .map(culture -> {
                    cultureRepository.delete(culture);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Culture not found with id " + cultureId));
    }
}
