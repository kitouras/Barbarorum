package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.WaterspaceDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Waterspace;
import com.example.servingwebcontent.repository.SeasonRepository;
import com.example.servingwebcontent.repository.WaterspaceRepository;
import com.example.servingwebcontent.repository.ClimatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class WaterspaceController {

    @Autowired
    private WaterspaceRepository waterspaceRepository;

    @Autowired
    private ClimatRepository climateRepository;

    @Autowired
    private SeasonRepository seasonRepository;

    @GetMapping("/waterspace")
    public Page<Waterspace> getWaterspace(Pageable pageable) {
        return waterspaceRepository.findAll(pageable);
    }

    @GetMapping("/waterspace/{waterspaceId}")
    public Waterspace getWaterspaceById(@PathVariable Long waterspaceId) {
        var waterspace = waterspaceRepository.findById(waterspaceId);
        if(waterspace.isPresent())
            return waterspace.get();
        else throw new ResourceNotFoundException("Waterspace not found with id " + waterspaceId);

    }

    @PostMapping("/waterspace")
    public Waterspace createWaterspace(@Valid @RequestBody WaterspaceDTO waterspaceRequest) {
        if(!climateRepository.existsById(waterspaceRequest.getClimatId())) {
            throw new ResourceNotFoundException("Climat not found with id " + waterspaceRequest.getClimatId());
        }
        if(!seasonRepository.existsById(waterspaceRequest.getSeasonId())) {
            throw new ResourceNotFoundException("Season not found with id " + waterspaceRequest.getSeasonId());
        }
        var waterspace = new Waterspace();
        waterspace.setClimatId(climateRepository.findById(waterspaceRequest.getClimatId()).get());
        waterspace.setSeasonId(seasonRepository.findById(waterspaceRequest.getSeasonId()).get());
        waterspace.setDepth(waterspaceRequest.getDepth());
        waterspace.setWidth(waterspaceRequest.getWidth());
        return waterspaceRepository.save(waterspace);
    }

    @PutMapping("/waterspace/{waterspaceId}")
    public Waterspace updateWaterspace(@PathVariable Long waterspaceId,
                               @Valid @RequestBody WaterspaceDTO waterspaceRequest) {
        if(!climateRepository.existsById(waterspaceRequest.getClimatId())) {
            throw new ResourceNotFoundException("Climat not found with id " + waterspaceRequest.getClimatId());
        }
        if(!seasonRepository.existsById(waterspaceRequest.getSeasonId())) {
            throw new ResourceNotFoundException("Season not found with id " + waterspaceRequest.getSeasonId());
        }
        return waterspaceRepository.findById(waterspaceId)
                .map(waterspace -> {
                    waterspace.setDepth(waterspaceRequest.getDepth());
                    waterspace.setWidth(waterspaceRequest.getWidth());
                    waterspace.setClimatId(climateRepository.findById(waterspaceRequest.getClimatId()).get());
                    waterspace.setSeasonId(seasonRepository.findById(waterspaceRequest.getSeasonId()).get());
                    return waterspaceRepository.save(waterspace);
                }).orElseThrow(() -> new ResourceNotFoundException("Waterspace not found with id " + waterspaceId));
    }


    @DeleteMapping("/waterspace/{waterspaceId}")
    public ResponseEntity<Object> deleteWaterspace(@PathVariable Long waterspaceId) {
        return waterspaceRepository.findById(waterspaceId)
                .map(waterspace -> {
                    waterspaceRepository.delete(waterspace);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Waterspace not found with id " + waterspaceId));
    }
}
