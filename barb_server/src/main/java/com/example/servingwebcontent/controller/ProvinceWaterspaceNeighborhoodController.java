package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ProvinceWaterspaceNeighborhoodDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.ProvinceWaterspaceNeighborhood;
import com.example.servingwebcontent.model.ProvinceWaterspaceNeighborhoodPK;
import com.example.servingwebcontent.repository.WaterspaceRepository;
import com.example.servingwebcontent.repository.ProvinceWaterspaceNeighborhoodRepository;
import com.example.servingwebcontent.repository.ProvinceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ProvinceWaterspaceNeighborhoodController {
    private static final Logger log = LoggerFactory.getLogger(ProvinceWaterspaceNeighborhoodController.class);
    private static final String PROVINCE_NOT_FOUND_WITH_ID = "Province not found with id ";
    private static final String WATERSPACE_NOT_FOUND_WITH_ID = "Waterspace not found with id ";

    @Autowired
    private ProvinceWaterspaceNeighborhoodRepository provinceWaterspaceNeighborhoodRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private WaterspaceRepository waterspaceRepository;

    @GetMapping("/provinceWaterspaceNeighborhood")
    public Page<ProvinceWaterspaceNeighborhood> getProvinceWaterspaceNeighborhood(Pageable pageable) {
        return provinceWaterspaceNeighborhoodRepository.findAll(pageable);
    }

    @GetMapping("/provinceWaterspaceNeighborhood/{provinceId}_{waterspaceId}")
    public ProvinceWaterspaceNeighborhood getProvinceWaterspaceNeighborhoodById(@PathVariable Long provinceId,
                                                                        @PathVariable Long waterspaceId) {
        if(provinceWaterspaceNeighborhoodRepository.findByIdProvinceIdAndIdWaterspaceId(provinceId, waterspaceId).isPresent())
            return provinceWaterspaceNeighborhoodRepository.findByIdProvinceIdAndIdWaterspaceId(provinceId, waterspaceId).get();
        else throw new ResourceNotFoundException("ProvinceWaterspaceNeighborhood not found with ids "
                + provinceId + " and " + waterspaceId);
    }


    @PostMapping("/provinceWaterspaceNeighborhood")
    public ProvinceWaterspaceNeighborhood createProvinceWaterspaceNeighborhood(@Valid @RequestBody ProvinceWaterspaceNeighborhoodDTO
                                                                                           provinceWaterspaceNeighborhoodRequest) {
        if(!provinceRepository.existsById(provinceWaterspaceNeighborhoodRequest.getProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceWaterspaceNeighborhoodRequest.getProvinceId());
        }
        if(!waterspaceRepository.existsById(provinceWaterspaceNeighborhoodRequest.getWaterspaceId())) {
            throw new ResourceNotFoundException(WATERSPACE_NOT_FOUND_WITH_ID + provinceWaterspaceNeighborhoodRequest.getWaterspaceId());
        }
        var provinceWaterspaceNeighborhood = new ProvinceWaterspaceNeighborhood();
        provinceWaterspaceNeighborhood.setProvinceId(provinceRepository.findById(
                provinceWaterspaceNeighborhoodRequest.getProvinceId()).get());
        provinceWaterspaceNeighborhood.setWaterspaceId(waterspaceRepository.findById(
                provinceWaterspaceNeighborhoodRequest.getWaterspaceId()).get());

        var provinceWaterspaceNeighborhoodPK = new ProvinceWaterspaceNeighborhoodPK();
        provinceWaterspaceNeighborhoodPK.setProvinceId(provinceWaterspaceNeighborhoodRequest.getProvinceId());
        provinceWaterspaceNeighborhoodPK.setWaterspaceId(provinceWaterspaceNeighborhoodRequest.getWaterspaceId());
        provinceWaterspaceNeighborhood.setId(provinceWaterspaceNeighborhoodPK);

        return provinceWaterspaceNeighborhoodRepository.save(provinceWaterspaceNeighborhood);
    }


    @DeleteMapping("/provinceWaterspaceNeighborhood/{provinceId}_{waterspaceId}")
    public ResponseEntity<Object> deleteProvinceWaterspaceNeighborhood(@PathVariable Long provinceId,
                                                    @PathVariable Long waterspaceId) {
        log.info("Deleting provinceWaterspaceNeighborhood with provinceId={} and waterspaceId={}", provinceId, waterspaceId);
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!waterspaceRepository.existsById(waterspaceId)) {
            throw new ResourceNotFoundException(WATERSPACE_NOT_FOUND_WITH_ID + waterspaceId);
        }

        return provinceWaterspaceNeighborhoodRepository.findByIdProvinceIdAndIdWaterspaceId(provinceId, waterspaceId)
                .map(provinceWaterspaceNeighborhood -> {
                    provinceWaterspaceNeighborhoodRepository.delete(provinceWaterspaceNeighborhood);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceWaterspaceNeighborhood not found with ids "
                        + provinceId + " and " + waterspaceId));
    }
}
