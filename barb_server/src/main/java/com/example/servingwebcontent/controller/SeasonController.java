package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.SeasonDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Season;
import com.example.servingwebcontent.model.Season;
import com.example.servingwebcontent.repository.SeasonRepository;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class SeasonController {

    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/season")
    public Page<Season> getSeason(Pageable pageable,
                                  @RequestParam(defaultValue = "id") String sortBy,
                                  @RequestParam(defaultValue = "ASC") String sortDir,
                                  @RequestParam(required = false) Long id,
                                  @RequestParam(required = false) String name,
                                  @RequestParam(required = false) Double temperatureMod,
                                  @RequestParam(required = false) Double windMod,
                                  @RequestParam(required = false) Double humidityMod,
                                  @RequestParam(required = false) Double atmosphericPressureMod,
                                  @RequestParam(required = false) Long incomeModsId) {

        return seasonRepository.findAllWithFilter(pageable,
                id, name,temperatureMod,windMod,humidityMod, atmosphericPressureMod,incomeModsId, sortBy, sortDir);
    }

    @GetMapping("/season/{seasonId}")
    public Season getSeasonById(@PathVariable Long seasonId) {
        var season = seasonRepository.findById(seasonId);
        if(season.isPresent())
            return season.get();
        else throw new ResourceNotFoundException("Season not found with id " + seasonId);

    }
    @PostMapping("/season")
    public Season createSeason(@Valid @RequestBody SeasonDTO seasonRequest) {
        var season = new Season();
        return incomeAffectableModeRepository.findById(seasonRequest.getIncomeModsId())
                .map(incomeMods -> {
                    season.setIncomeModsId(incomeMods);
                    season.setName(seasonRequest.getName());
                    season.setTemperatureMod(seasonRequest.getTemperatureMod());
                    season.setWindMod(seasonRequest.getWindMod());
                    season.setHumidityMod(seasonRequest.getHumidityMod());
                    season.setAtmosphericPressureMod(seasonRequest.getAtmosphericPressureMod());
                    return seasonRepository.save(season);
                }).orElseThrow(() -> new ResourceNotFoundException("IncomeMods not found with id " + seasonRequest.getIncomeModsId()));
    }

    @PutMapping("/season/{seasonId}")
    public Season updateSeason(@PathVariable Long seasonId,
                               @Valid @RequestBody SeasonDTO seasonRequest) {
        if(!incomeAffectableModeRepository.existsById(seasonRequest.getIncomeModsId())) {
            throw new ResourceNotFoundException("IncomeMods not found with id " + seasonRequest.getIncomeModsId());
        }
        return seasonRepository.findById(seasonId)
                .map(season -> {
                    season.setName(seasonRequest.getName());
                    season.setTemperatureMod(seasonRequest.getTemperatureMod());
                    season.setWindMod(seasonRequest.getWindMod());
                    season.setHumidityMod(seasonRequest.getHumidityMod());
                    season.setAtmosphericPressureMod(seasonRequest.getAtmosphericPressureMod());
                    season.setIncomeModsId(incomeAffectableModeRepository.findById(seasonRequest.getIncomeModsId()).get());
                    return seasonRepository.save(season);
                }).orElseThrow(() -> new ResourceNotFoundException("Season not found with id " + seasonId));
    }


    @DeleteMapping("/season/{seasonId}")
    public ResponseEntity<Object> deleteSeason(@PathVariable Long seasonId) {
        return seasonRepository.findById(seasonId)
                .map(season -> {
                    seasonRepository.delete(season);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Season not found with id " + seasonId));
    }
}
