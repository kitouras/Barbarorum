package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ProvinceReligionsDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.ProvinceReligionsPK;
import com.example.servingwebcontent.model.ProvinceReligions;
import com.example.servingwebcontent.repository.ReligionRepository;
import com.example.servingwebcontent.repository.ProvinceReligionsRepository;
import com.example.servingwebcontent.repository.ProvinceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ProvinceReligionsController {
    private static final Logger log = LoggerFactory.getLogger(ProvinceReligionsController.class);
    private static final String PROVINCE_NOT_FOUND_WITH_ID = "Province not found with id ";
    private static final String RELIGION_NOT_FOUND_WITH_ID = "Religion not found with id ";

    @Autowired
    private ProvinceReligionsRepository provinceReligionsRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private ReligionRepository religionRepository;

    @GetMapping("/provinceReligions")
    public Page<ProvinceReligions> getProvinceReligions(Pageable pageable) {
        return provinceReligionsRepository.findAll(pageable);
    }

    @GetMapping("/provinceReligions/{provinceId}_{religionId}")
    public ProvinceReligions getProvinceReligionsById(@PathVariable Long provinceId,
                                                                                @PathVariable Long religionId) {
        if(provinceReligionsRepository.findByIdProvinceIdAndIdReligionId(provinceId, religionId).isPresent())
            return provinceReligionsRepository.findByIdProvinceIdAndIdReligionId(provinceId, religionId).get();
        else throw new ResourceNotFoundException("ProvinceReligions not found with ids "
                + provinceId + " and " + religionId);
    }

    @PostMapping("/provinceReligions")
    public ProvinceReligions createProvinceReligions(@Valid @RequestBody ProvinceReligionsDTO provinceReligionsRequest) {
        if(!provinceRepository.existsById(provinceReligionsRequest.getProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceReligionsRequest.getProvinceId());
        }
        if(!religionRepository.existsById(provinceReligionsRequest.getReligionId())) {
            throw new ResourceNotFoundException(RELIGION_NOT_FOUND_WITH_ID + provinceReligionsRequest.getReligionId());
        }
        var provinceReligions = new ProvinceReligions();
        provinceReligions.setProvinceId(provinceRepository.findById(provinceReligionsRequest.getProvinceId()).get());
        provinceReligions.setReligionId(religionRepository.findById(provinceReligionsRequest.getReligionId()).get());
        provinceReligions.setPercentage(provinceReligionsRequest.getPercentage());

        var provinceReligionsPK = new ProvinceReligionsPK();
        provinceReligionsPK.setProvinceId(provinceReligionsRequest.getProvinceId());
        provinceReligionsPK.setReligionId(provinceReligionsRequest.getReligionId());
        provinceReligions.setId(provinceReligionsPK);
        return provinceReligionsRepository.save(provinceReligions);
    }

    @PutMapping("/provinceReligions/{provinceId}_{religionId}")
    public ProvinceReligions updateProvinceReligions(@PathVariable Long provinceId, @PathVariable Long religionId,
                                                   @Valid @RequestBody ProvinceReligionsDTO provinceReligionsRequest) {
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!religionRepository.existsById(religionId)) {
            throw new ResourceNotFoundException(RELIGION_NOT_FOUND_WITH_ID + religionId);
        }
        return provinceReligionsRepository.findByIdProvinceIdAndIdReligionId(
                provinceId, religionId)
                .map(provinceReligions -> {
                    provinceReligions.setPercentage(provinceReligionsRequest.getPercentage());
                    return provinceReligionsRepository.save(provinceReligions);
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceReligions not found with ids "
                        + provinceId + " and " + religionId));
    }

    @DeleteMapping("/provinceReligions/{provinceId}_{religionId}")
    public ResponseEntity<Object> deleteProvinceReligions(@PathVariable Long provinceId,
                                                    @PathVariable Long religionId) {
        log.info("Deleting provinceReligions with provinceId={} and religionId={}", provinceId, religionId);
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!religionRepository.existsById(religionId)) {
            throw new ResourceNotFoundException(RELIGION_NOT_FOUND_WITH_ID + religionId);
        }

        return provinceReligionsRepository.findByIdProvinceIdAndIdReligionId(provinceId, religionId)
                .map(provinceReligions -> {
                    provinceReligionsRepository.delete(provinceReligions);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceReligions not found with ids "
                        + provinceId + " and " + religionId));
    }
}
