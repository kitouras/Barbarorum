package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ProvinceDeseasesDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.ProvinceDeseasesPK;
import com.example.servingwebcontent.model.ProvinceDeseases;
import com.example.servingwebcontent.repository.DeseaseRepository;
import com.example.servingwebcontent.repository.ProvinceDeseasesRepository;
import com.example.servingwebcontent.repository.ProvinceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ProvinceDeseasesController {
    private static final Logger log = LoggerFactory.getLogger(ProvinceDeseasesController.class);
    private static final String PROVINCE_NOT_FOUND_WITH_ID = "Province not found with id ";
    private static final String DESEASE_NOT_FOUND_WITH_ID = "Desease not found with id ";

    @Autowired
    private ProvinceDeseasesRepository provinceDeseasesRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private DeseaseRepository deseaseRepository;

    @GetMapping("/provinceDeseases")
    public Page<ProvinceDeseases> getProvinceDeseases(Pageable pageable) {
        return provinceDeseasesRepository.findAll(pageable);
    }

    @GetMapping("/provinceDeseases/{provinceId}_{deseaseId}")
    public ProvinceDeseases getProvinceDeseasesById(@PathVariable Long provinceId,
                                                      @PathVariable Long deseaseId) {
        if(provinceDeseasesRepository.findByIdProvinceIdAndIdDeseaseId(provinceId, deseaseId).isPresent())
            return provinceDeseasesRepository.findByIdProvinceIdAndIdDeseaseId(provinceId, deseaseId).get();
        else throw new ResourceNotFoundException("ProvinceDeseases not found with ids "
                + provinceId + " and " + deseaseId);
    }

    @PostMapping("/provinceDeseases")
    public ProvinceDeseases createProvinceDeseases(@Valid @RequestBody ProvinceDeseasesDTO provinceDeseasesRequest) {
        if(!provinceRepository.existsById(provinceDeseasesRequest.getProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceDeseasesRequest.getProvinceId());
        }
        if(!deseaseRepository.existsById(provinceDeseasesRequest.getDeseaseId())) {
            throw new ResourceNotFoundException(DESEASE_NOT_FOUND_WITH_ID + provinceDeseasesRequest.getDeseaseId());
        }
        var provinceDeseases = new ProvinceDeseases();
        provinceDeseases.setProvinceId(provinceRepository.findById(provinceDeseasesRequest.getProvinceId()).get());
        provinceDeseases.setDeseaseId(deseaseRepository.findById(provinceDeseasesRequest.getDeseaseId()).get());
        provinceDeseases.setPrevalence(provinceDeseasesRequest.getPrevalence());
        provinceDeseases.setCollectiveImmunity(provinceDeseasesRequest.getCollectiveImmunity());

        var provinceDeseasesPK = new ProvinceDeseasesPK();
        provinceDeseasesPK.setProvinceId(provinceDeseasesRequest.getProvinceId());
        provinceDeseasesPK.setDeseaseId(provinceDeseasesRequest.getDeseaseId());
        provinceDeseases.setId(provinceDeseasesPK);
        
        return provinceDeseasesRepository.save(provinceDeseases);
    }

    @PutMapping("/provinceDeseases/{provinceId}_{deseaseId}")
    public ProvinceDeseases updateProvinceDeseases(@PathVariable Long provinceId, @PathVariable Long deseaseId,
                                                                       @Valid @RequestBody ProvinceDeseasesDTO provinceDeseasesRequest) {
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!deseaseRepository.existsById(deseaseId)) {
            throw new ResourceNotFoundException(DESEASE_NOT_FOUND_WITH_ID + deseaseId);
        }
        return provinceDeseasesRepository.findByIdProvinceIdAndIdDeseaseId(
                provinceId, deseaseId)
                .map(provinceDeseases -> {
                    provinceDeseases.setPrevalence(provinceDeseasesRequest.getPrevalence());
                    provinceDeseases.setCollectiveImmunity(provinceDeseasesRequest.getCollectiveImmunity());
                    return provinceDeseasesRepository.save(provinceDeseases);
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceDeseases not found with ids "
                        + provinceId + " and " + deseaseId));
    }

    @DeleteMapping("/provinceDeseases/{provinceId}_{deseaseId}")
    public ResponseEntity<Object> deleteProvinceDeseases(@PathVariable Long provinceId,
                                                              @PathVariable Long deseaseId) {
        log.info("Deleting provinceDeseases with provinceId={} and deseaseId={}", provinceId, deseaseId);
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!deseaseRepository.existsById(deseaseId)) {
            throw new ResourceNotFoundException(DESEASE_NOT_FOUND_WITH_ID + deseaseId);
        }

        return provinceDeseasesRepository.findByIdProvinceIdAndIdDeseaseId(provinceId, deseaseId)
                .map(provinceDeseases -> {
                    provinceDeseasesRepository.delete(provinceDeseases);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceDeseases not found with ids "
                        + provinceId + " and " + deseaseId));
    }
}
