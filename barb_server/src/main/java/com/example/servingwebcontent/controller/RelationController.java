package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.RelationDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Relation;
import com.example.servingwebcontent.repository.RelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class RelationController {

    @Autowired
    private RelationRepository relationRepository;

    @GetMapping("/relation")
    public Page<Relation> getRelation(Pageable pageable) {
        return relationRepository.findAll(pageable);
    }

    @GetMapping("/relation/{relationId}")
    public Relation getRelationById(@PathVariable Long relationId) {
        var relation = relationRepository.findById(relationId);
        if(relation.isPresent())
            return relation.get();
        else throw new ResourceNotFoundException("Relation not found with id " + relationId);

    }
    
    @PostMapping("/relation")
    public Relation createRelation(@Valid @RequestBody RelationDTO relationRequest) {
        var relation = new Relation();
        relation.setName(relationRequest.getName());
        relation.setFractionsConnectivity(relationRequest.getFractionsConnectivity());
        return relationRepository.save(relation);
    }

    @PutMapping("/relation/{relationId}")
    public Relation updateRelation(@PathVariable Long relationId,
                                   @Valid @RequestBody RelationDTO relationRequest) {
        return relationRepository.findById(relationId)
                .map(relation -> {
                    relation.setName(relationRequest.getName());
                    relation.setFractionsConnectivity(relationRequest.getFractionsConnectivity());
                    return relationRepository.save(relation);
                }).orElseThrow(() -> new ResourceNotFoundException("Relation not found with id " + relationId));
    }


    @DeleteMapping("/relation/{relationId}")
    public ResponseEntity<Object> deleteRelation(@PathVariable Long relationId) {
        return relationRepository.findById(relationId)
                .map(relation -> {
                    relationRepository.delete(relation);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Relation not found with id " + relationId));
    }
}
