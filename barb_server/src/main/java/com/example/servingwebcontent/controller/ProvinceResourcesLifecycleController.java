package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ProvinceResourcesLifecycleDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.ProvinceResourcesLifecyclePK;
import com.example.servingwebcontent.model.ProvinceResourcesLifecycle;
import com.example.servingwebcontent.repository.ProvinceResourcesLifecycleRepository;
import com.example.servingwebcontent.repository.ProvinceRepository;
import com.example.servingwebcontent.repository.ResourceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ProvinceResourcesLifecycleController {
    private static final Logger log = LoggerFactory.getLogger(ProvinceResourcesLifecycleController.class);
    private static final String PROVINCE_NOT_FOUND_WITH_ID = "Province not found with id ";
    private static final String RESOURCE_NOT_FOUND_WITH_ID = "Resource not found with id ";

    @Autowired
    private ProvinceResourcesLifecycleRepository provinceResourcesLifecycleRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @GetMapping("/provinceResourcesLifecycle")
    public Page<ProvinceResourcesLifecycle> getProvinceResourcesLifecycle(Pageable pageable,
                                                                          @RequestParam(defaultValue = "provinceId") String sortBy,
                                                                          @RequestParam(defaultValue = "ASC") String sortDir,
                                                                          @RequestParam(required = false) Long provinceId,
                                                                          @RequestParam(required = false) Long resourceId,
                                                                          @RequestParam(required = false) Integer resourceStock,
                                                                          @RequestParam(required = false) Integer resourceProduction,
                                                                          @RequestParam(required = false) Integer resourceLimit,
                                                                          @RequestParam(required = false) Integer resourceMarketing) {
        return provinceResourcesLifecycleRepository.findAllWithFilter(pageable,
                provinceId, resourceId, resourceStock, resourceProduction, resourceLimit, resourceMarketing, sortBy, sortDir);
    }
    @GetMapping("/provinceResourcesLifecycle/{provinceId}_{resourceId}")
    public ProvinceResourcesLifecycle getProvinceResourcesLifecycleById(@PathVariable Long provinceId,
                                                                              @PathVariable Long resourceId) {
        if(provinceResourcesLifecycleRepository.findByIdProvinceIdAndIdResourceId(provinceId, resourceId).isPresent())
            return provinceResourcesLifecycleRepository.findByIdProvinceIdAndIdResourceId(provinceId, resourceId).get();
        else throw new ResourceNotFoundException("ProvinceResourcesLifecycle not found with ids "
                + provinceId + " and " + resourceId);
    }


    @PostMapping("/provinceResourcesLifecycle")
    public ProvinceResourcesLifecycle createProvinceResourcesLifecycle(@Valid @RequestBody ProvinceResourcesLifecycleDTO
                                                                                   provinceResourcesLifecycleRequest) {
        if(!provinceRepository.existsById(provinceResourcesLifecycleRequest.getProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceResourcesLifecycleRequest.getProvinceId());
        }
        if(!resourceRepository.existsById(provinceResourcesLifecycleRequest.getResourceId())) {
            throw new ResourceNotFoundException(RESOURCE_NOT_FOUND_WITH_ID + provinceResourcesLifecycleRequest.getResourceId());
        }
        var provinceResourcesLifecycle = new ProvinceResourcesLifecycle();
        provinceResourcesLifecycle.setProvinceId(provinceRepository.findById(provinceResourcesLifecycleRequest.getProvinceId()).get());
        provinceResourcesLifecycle.setResourceId(resourceRepository.findById(provinceResourcesLifecycleRequest.getResourceId()).get());
        provinceResourcesLifecycle.setResourceLimit(provinceResourcesLifecycleRequest.getResourceLimit());
        provinceResourcesLifecycle.setResourceStock(provinceResourcesLifecycleRequest.getResourceStock());
        provinceResourcesLifecycle.setResourceProduction(provinceResourcesLifecycleRequest.getResourceProduction());
        provinceResourcesLifecycle.setResourceMarketing(provinceResourcesLifecycleRequest.getResourceMarketing());

        var provinceResourcesLifecyclePK = new ProvinceResourcesLifecyclePK();
        provinceResourcesLifecyclePK.setProvinceId(provinceResourcesLifecycleRequest.getProvinceId());
        provinceResourcesLifecyclePK.setResourceId(provinceResourcesLifecycleRequest.getResourceId());
        provinceResourcesLifecycle.setId(provinceResourcesLifecyclePK);
        
        return provinceResourcesLifecycleRepository.save(provinceResourcesLifecycle);
    }

    @PutMapping("/provinceResourcesLifecycle/{provinceId}_{resourceId}")
    public ProvinceResourcesLifecycle updateProvinceResourcesLifecycle(@PathVariable Long provinceId, @PathVariable Long resourceId,
                                                                 @Valid @RequestBody ProvinceResourcesLifecycleDTO provinceResourcesLifecycleRequest) {
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!resourceRepository.existsById(resourceId)) {
            throw new ResourceNotFoundException(RESOURCE_NOT_FOUND_WITH_ID + resourceId);
        }
        return provinceResourcesLifecycleRepository.findByIdProvinceIdAndIdResourceId(
                provinceId, resourceId)
                .map(provinceResourcesLifecycle -> {
                    provinceResourcesLifecycle.setResourceLimit(provinceResourcesLifecycleRequest.getResourceLimit());
                    provinceResourcesLifecycle.setResourceStock(provinceResourcesLifecycleRequest.getResourceStock());
                    provinceResourcesLifecycle.setResourceProduction(provinceResourcesLifecycleRequest.getResourceProduction());
                    provinceResourcesLifecycle.setResourceMarketing(provinceResourcesLifecycleRequest.getResourceMarketing());
                    return provinceResourcesLifecycleRepository.save(provinceResourcesLifecycle);
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceResourcesLifecycle not found with ids "
                        + provinceId + " and " + resourceId));
    }

    @DeleteMapping("/provinceResourcesLifecycle/{provinceId}_{resourceId}")
    public ResponseEntity<Object> deleteProvinceResourcesLifecycle(@PathVariable Long provinceId,
                                                           @PathVariable Long resourceId) {
        log.info("Deleting provinceResourcesLifecycle with provinceId={} and resourceId={}", provinceId, resourceId);
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!resourceRepository.existsById(resourceId)) {
            throw new ResourceNotFoundException(RESOURCE_NOT_FOUND_WITH_ID + resourceId);
        }

        return provinceResourcesLifecycleRepository.findByIdProvinceIdAndIdResourceId(provinceId, resourceId)
                .map(provinceResourcesLifecycle -> {
                    provinceResourcesLifecycleRepository.delete(provinceResourcesLifecycle);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceResourcesLifecycle not found with ids "
                        + provinceId + " and " + resourceId));
    }
}
