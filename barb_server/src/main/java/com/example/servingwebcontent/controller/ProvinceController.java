package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ProvinceDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Province;
import com.example.servingwebcontent.repository.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("/api")
@RestController
public class ProvinceController {

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private ClimatRepository climateRepository;

    @Autowired
    private SeasonRepository seasonRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private LegalSystemRepository legalSystemRepository;

    @Autowired
    private LandscapeRepository landscapeRepository;

    @GetMapping("/province")
    public Page<Province> getProvince(Pageable pageable) {
        return provinceRepository.findAll(pageable);
    }

    @GetMapping("/province/states")
    public String countAllStatesProvincesWithConditions() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(provinceRepository.countAllStatesProvincesWithConditions());
    }
    @GetMapping("/province/statesTreasury")
    public String countStatesTreasuryByProvince() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(provinceRepository.countStatesTreasuryByProvince());
    }
    @GetMapping("/province/fullPopulation")
    public String countFullPopulationOfProvinces() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(provinceRepository.countFullPopulationOfProvinces());
    }

    @GetMapping("/province/state/{stateName}")
    public Page<Province> getProvinceByState(@PathVariable String stateName, Pageable pageable) {
        return provinceRepository.findAllProvinceByStateNameWithPagination(stateName, pageable);
    }

    @GetMapping("/province/{provinceId}")
    public Province getProvinceById(@PathVariable Long provinceId) {
        var province = provinceRepository.findById(provinceId);
        if(province.isPresent())
            return province.get();
        else throw new ResourceNotFoundException("Province not found with id " + provinceId);

    }

    @GetMapping("/province/name/{provinceId}")
    public Province getProvinceByName(@PathVariable String provinceName) {
        var province = provinceRepository.findByName(provinceName);
        if(province.isPresent())
            return province.get();
        else throw new ResourceNotFoundException("Province not found with name " + provinceName);

    }

    @PostMapping("/province")
    public Province createProvince(@Valid @RequestBody ProvinceDTO provinceRequest) {
        if(!climateRepository.existsById(provinceRequest.getClimatId())) {
            throw new ResourceNotFoundException("Climat not found with id " + provinceRequest.getClimatId());
        }
        if(!seasonRepository.existsById(provinceRequest.getSeasonId())) {
            throw new ResourceNotFoundException("Season not found with id " + provinceRequest.getSeasonId());
        }
        if(!stateRepository.existsById(provinceRequest.getStateId())) {
            throw new ResourceNotFoundException("State not found with id " + provinceRequest.getStateId());
        }
        if(!cityRepository.existsById(provinceRequest.getCityId())) {
            throw new ResourceNotFoundException("City not found with id " + provinceRequest.getCityId());
        }
        if(!legalSystemRepository.existsById(provinceRequest.getLegalSystemId())) {
            throw new ResourceNotFoundException("LegalSystem not found with id " + provinceRequest.getLegalSystemId());
        }
        if(!landscapeRepository.existsById(provinceRequest.getLandscapeId())) {
            throw new ResourceNotFoundException("Landscape not found with id " + provinceRequest.getLandscapeId());
        }

        var province = new Province();
        province.setClimatId(climateRepository.findById(provinceRequest.getClimatId()).get());
        province.setSeasonId(seasonRepository.findById(provinceRequest.getSeasonId()).get());
        province.setStateId(stateRepository.findById(provinceRequest.getStateId()).get());
        province.setCityId(cityRepository.findById(provinceRequest.getCityId()).get());
        province.setLegalSystemId(legalSystemRepository.findById(provinceRequest.getLegalSystemId()).get());
        province.setLandscapeId(landscapeRepository.findById(provinceRequest.getLandscapeId()).get());

        province.setName(provinceRequest.getName());
        province.setTreasury(provinceRequest.getTreasury());
        province.setTaxAbove(provinceRequest.getTaxAbove());
        province.setTaxInto(provinceRequest.getTaxInto());
        province.setAutonomy(provinceRequest.getAutonomy());
        province.setRebellionPotential(provinceRequest.getRebellionPotential());
        province.setVillagePopularity(provinceRequest.getVillagePopularity());
        province.setPopularityGrowth(provinceRequest.getPopularityGrowth());
        return provinceRepository.save(province);
    }

    @PutMapping("/province/{provinceId}")
    public Province updateProvince(@PathVariable Long provinceId,
                                       @Valid @RequestBody ProvinceDTO provinceRequest) {
        if(!climateRepository.existsById(provinceRequest.getClimatId())) {
            throw new ResourceNotFoundException("Climat not found with id " + provinceRequest.getClimatId());
        }
        if(!seasonRepository.existsById(provinceRequest.getSeasonId())) {
            throw new ResourceNotFoundException("Season not found with id " + provinceRequest.getSeasonId());
        }
        if(!stateRepository.existsById(provinceRequest.getStateId())) {
            throw new ResourceNotFoundException("State not found with id " + provinceRequest.getStateId());
        }
        if(!cityRepository.existsById(provinceRequest.getCityId())) {
            throw new ResourceNotFoundException("City not found with id " + provinceRequest.getCityId());
        }
        if(!legalSystemRepository.existsById(provinceRequest.getLegalSystemId())) {
            throw new ResourceNotFoundException("LegalSystem not found with id " + provinceRequest.getLegalSystemId());
        }
        if(!landscapeRepository.existsById(provinceRequest.getLandscapeId())) {
            throw new ResourceNotFoundException("Landscape not found with id " + provinceRequest.getLandscapeId());
        }

        return provinceRepository.findById(provinceId)
                .map(province -> {
                    province.setTreasury(provinceRequest.getTreasury());
                    province.setTaxAbove(provinceRequest.getTaxAbove());
                    province.setTaxInto(provinceRequest.getTaxInto());
                    province.setAutonomy(provinceRequest.getAutonomy());
                    province.setRebellionPotential(provinceRequest.getRebellionPotential());
                    province.setVillagePopularity(provinceRequest.getVillagePopularity());
                    province.setPopularityGrowth(provinceRequest.getPopularityGrowth());
                    province.setName(provinceRequest.getName());

                    province.setClimatId(climateRepository.findById(provinceRequest.getClimatId()).get());
                    province.setSeasonId(seasonRepository.findById(provinceRequest.getSeasonId()).get());
                    province.setStateId(stateRepository.findById(provinceRequest.getStateId()).get());
                    province.setCityId(cityRepository.findById(provinceRequest.getCityId()).get());
                    province.setLegalSystemId(legalSystemRepository.findById(provinceRequest.getLegalSystemId()).get());
                    province.setLandscapeId(landscapeRepository.findById(provinceRequest.getLandscapeId()).get());
                    return provinceRepository.save(province);
                }).orElseThrow(() -> new ResourceNotFoundException("Province not found with id " + provinceId));
    }


    @DeleteMapping("/province/{provinceId}")
    public ResponseEntity<Object> deleteProvince(@PathVariable Long provinceId) {
        return provinceRepository.findById(provinceId)
                .map(province -> {
                    provinceRepository.delete(province);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Province not found with id " + provinceId));
    }
}
