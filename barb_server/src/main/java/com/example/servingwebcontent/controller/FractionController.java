package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.FractionDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Fraction;
import com.example.servingwebcontent.repository.FractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class FractionController {

    @Autowired
    private FractionRepository fractionRepository;

    @GetMapping("/fraction")
    public Page<Fraction> getFraction(Pageable pageable) {
        return fractionRepository.findAll(pageable);
    }

    @GetMapping("/fraction/{fractionId}")
    public Fraction getFractionById(@PathVariable Long fractionId) {
        var fraction = fractionRepository.findById(fractionId);
        if(fraction.isPresent())
            return fraction.get();
        else throw new ResourceNotFoundException("Fraction not found with id " + fractionId);

    }

    @PostMapping("/fraction")
    public Fraction createFraction(@Valid @RequestBody FractionDTO fractionRequest) {
        var fraction = new Fraction();
        fraction.setName(fractionRequest.getName());
        fraction.setRebellionTendency(fractionRequest.getRebellionTendency());
        return fractionRepository.save(fraction);
    }

    @PutMapping("/fraction/{fractionId}")
    public Fraction updateFraction(@PathVariable Long fractionId,
                           @Valid @RequestBody FractionDTO fractionRequest) {
        return fractionRepository.findById(fractionId)
                .map(fraction -> {
                    fraction.setName(fractionRequest.getName());
                    fraction.setRebellionTendency(fractionRequest.getRebellionTendency());
                    return fractionRepository.save(fraction);
                }).orElseThrow(() -> new ResourceNotFoundException("Fraction not found with id " + fractionId));
    }


    @DeleteMapping("/fraction/{fractionId}")
    public ResponseEntity<Object> deleteFraction(@PathVariable Long fractionId) {
        return fractionRepository.findById(fractionId)
                .map(fraction -> {
                    fractionRepository.delete(fraction);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Fraction not found with id " + fractionId));
    }
}
