package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.CityDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.City;
import com.example.servingwebcontent.repository.CityRepository;
import com.example.servingwebcontent.repository.SpecializationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private SpecializationRepository specializationRepository;

    @GetMapping("/city")
    public Page<City> getCity(Pageable pageable) {
        return cityRepository.findAll(pageable);
    }

    @GetMapping("/city/{cityId}")
    public City getCityById(@PathVariable Long cityId) {
        var city = cityRepository.findById(cityId);
        if(city.isPresent())
            return city.get();
        else throw new ResourceNotFoundException("City not found with id " + cityId);

    }
    @PostMapping("/city")
    public City createCity(@Valid @RequestBody CityDTO cityRequest) {
        var city = new City();
        return specializationRepository.findById(cityRequest.getSpecializationId())
                .map(specialization -> {
                    city.setSpecializationId(specialization);
                    city.setName(cityRequest.getName());
                    city.setPopularity(cityRequest.getPopularity());
                    return cityRepository.save(city);
                }).orElseThrow(() -> new ResourceNotFoundException("Specialization not found with id "
                        + cityRequest.getSpecializationId()));
    }

    @PutMapping("/city/{cityId}")
    public City updateCity(@PathVariable Long cityId,
                                     @Valid @RequestBody CityDTO cityRequest) {
        if(!specializationRepository.existsById(cityRequest.getSpecializationId())) {
            throw new ResourceNotFoundException("Specialization not found with id " + cityRequest.getSpecializationId());
        }
        return cityRepository.findById(cityId)
                .map(city -> {
                    city.setName(cityRequest.getName());
                    city.setPopularity(cityRequest.getPopularity());
                    city.setSpecializationId(specializationRepository.findById(cityRequest.getSpecializationId()).get());
                    return cityRepository.save(city);
                }).orElseThrow(() -> new ResourceNotFoundException("City not found with id " + cityId));
    }


    @DeleteMapping("/city/{cityId}")
    public ResponseEntity<Object> deleteCity(@PathVariable Long cityId) {
        return cityRepository.findById(cityId)
                .map(city -> {
                    cityRepository.delete(city);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("City not found with id " + cityId));
    }
}
