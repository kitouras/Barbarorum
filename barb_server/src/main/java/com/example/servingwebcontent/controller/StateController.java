package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.StateDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.State;
import com.example.servingwebcontent.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class StateController {

    @Autowired
    private StateRepository stateRepository;

    @GetMapping("/state")
    public Page<State> getState(Pageable pageable) {
        return stateRepository.findAll(pageable);
    }

    @GetMapping("/state/{stateId}")
    public State getStateById(@PathVariable Long stateId) {
        var state = stateRepository.findById(stateId);
        if(state.isPresent())
            return state.get();
        else throw new ResourceNotFoundException("State not found with id " + stateId);

    }

    @PostMapping("/state")
    public State createState(@Valid @RequestBody StateDTO stateRequest) {
        var state = new State();
        state.setName(stateRequest.getName());
        state.setColor(stateRequest.getColor());
        state.setPopularity(stateRequest.getPopularity());
        state.setTreasury(stateRequest.getTreasury());
        return stateRepository.save(state);
    }

    @PutMapping("/state/{stateId}")
    public State updateState(@PathVariable Long stateId,
                                   @Valid @RequestBody StateDTO stateRequest) {
        return stateRepository.findById(stateId)
                .map(state -> {
                    state.setName(stateRequest.getName());
                    state.setColor(stateRequest.getColor());
                    state.setPopularity(stateRequest.getPopularity());
                    state.setTreasury(stateRequest.getTreasury());
                    return stateRepository.save(state);
                }).orElseThrow(() -> new ResourceNotFoundException("State not found with id " + stateId));
    }


    @DeleteMapping("/state/{stateId}")
    public ResponseEntity<Object> deleteState(@PathVariable Long stateId) {
        return stateRepository.findById(stateId)
                .map(state -> {
                    stateRepository.delete(state);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("State not found with id " + stateId));
    }
}
