package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ProvinceCulturesDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.ProvinceCulturesPK;
import com.example.servingwebcontent.model.ProvinceCultures;
import com.example.servingwebcontent.repository.CultureRepository;
import com.example.servingwebcontent.repository.ProvinceCulturesRepository;
import com.example.servingwebcontent.repository.ProvinceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ProvinceCulturesController {
    private static final Logger log = LoggerFactory.getLogger(ProvinceCulturesController.class);
    private static final String PROVINCE_NOT_FOUND_WITH_ID = "Province not found with id ";
    private static final String CULTURE_NOT_FOUND_WITH_ID = "Culture not found with id ";

    @Autowired
    private ProvinceCulturesRepository provinceCulturesRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private CultureRepository cultureRepository;

    @GetMapping("/provinceCultures")
    public Page<ProvinceCultures> getProvinceCultures(Pageable pageable) {
        return provinceCulturesRepository.findAll(pageable);
    }

    @GetMapping("/provinceCultures/{provinceId}_{cultureId}")
    public ProvinceCultures getProvinceCulturesById(@PathVariable Long provinceId,
                                                      @PathVariable Long cultureId) {
        if(provinceCulturesRepository.findByIdProvinceIdAndIdCultureId(provinceId, cultureId).isPresent())
            return provinceCulturesRepository.findByIdProvinceIdAndIdCultureId(provinceId, cultureId).get();
        else throw new ResourceNotFoundException("ProvinceCultures not found with ids "
                + provinceId + " and " + cultureId);
    }


    @PostMapping("/provinceCultures")
    public ProvinceCultures createProvinceCultures(@Valid @RequestBody ProvinceCulturesDTO provinceCulturesRequest) {
        if(!provinceRepository.existsById(provinceCulturesRequest.getProvinceId())) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceCulturesRequest.getProvinceId());
        }
        if(!cultureRepository.existsById(provinceCulturesRequest.getCultureId())) {
            throw new ResourceNotFoundException(CULTURE_NOT_FOUND_WITH_ID + provinceCulturesRequest.getCultureId());
        }
        var provinceCultures = new ProvinceCultures();
        provinceCultures.setProvinceId(provinceRepository.findById(provinceCulturesRequest.getProvinceId()).get());
        provinceCultures.setCultureId(cultureRepository.findById(provinceCulturesRequest.getCultureId()).get());
        provinceCultures.setPercentage(provinceCulturesRequest.getPercentage());

        var provinceCulturesPK = new ProvinceCulturesPK();
        provinceCulturesPK.setProvinceId(provinceCulturesRequest.getProvinceId());
        provinceCulturesPK.setCultureId(provinceCulturesRequest.getCultureId());
        provinceCultures.setId(provinceCulturesPK);

        return provinceCulturesRepository.save(provinceCultures);
    }

    @PutMapping("/provinceCultures/{provinceId}_{cultureId}")
    public ProvinceCultures updateProvinceCultures(@PathVariable Long provinceId, @PathVariable Long cultureId,
                                                   @Valid @RequestBody ProvinceCulturesDTO provinceCulturesRequest) {
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!cultureRepository.existsById(cultureId)) {
            throw new ResourceNotFoundException(CULTURE_NOT_FOUND_WITH_ID + cultureId);
        }
        return provinceCulturesRepository.findByIdProvinceIdAndIdCultureId(
                provinceId, cultureId)
                .map(provinceCultures -> {
                    provinceCultures.setPercentage(provinceCulturesRequest.getPercentage());
                    return provinceCulturesRepository.save(provinceCultures);
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceCultures not found with ids "
                        + provinceId + " and " + cultureId));
    }

    @DeleteMapping("/provinceCultures/{provinceId}_{cultureId}")
    public ResponseEntity<Object> deleteProvinceCultures(@PathVariable Long provinceId,
                                                    @PathVariable Long cultureId) {
        log.info("Deleting provinceCultures with provinceId={} and cultureId={}", provinceId, cultureId);
        if(!provinceRepository.existsById(provinceId)) {
            throw new ResourceNotFoundException(PROVINCE_NOT_FOUND_WITH_ID + provinceId);
        }
        if(!cultureRepository.existsById(cultureId)) {
            throw new ResourceNotFoundException(CULTURE_NOT_FOUND_WITH_ID + cultureId);
        }

        return provinceCulturesRepository.findByIdProvinceIdAndIdCultureId(provinceId, cultureId)
                .map(provinceCultures -> {
                    provinceCulturesRepository.delete(provinceCultures);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("ProvinceCultures not found with ids "
                        + provinceId + " and " + cultureId));
    }
}
