package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dto.ClimatDTO;
import com.example.servingwebcontent.exception.ResourceNotFoundException;
import com.example.servingwebcontent.model.Climat;
import com.example.servingwebcontent.repository.ClimatRepository;
import com.example.servingwebcontent.repository.IncomeAffectableModeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class ClimatController {

    @Autowired
    private ClimatRepository climatRepository;

    @Autowired
    private IncomeAffectableModeRepository incomeAffectableModeRepository;

    @GetMapping("/climat")
    public Page<Climat> getClimat(Pageable pageable,
                                  @RequestParam(defaultValue = "id") String sortBy,
                                  @RequestParam(defaultValue = "ASC") String sortDir,
                                  @RequestParam(required = false) Long id,
                                  @RequestParam(required = false) String name,
                                  @RequestParam(required = false) Integer temperatureDegree,
                                  @RequestParam(required = false) Integer windDegree,
                                  @RequestParam(required = false) Integer humidityDegree,
                                  @RequestParam(required = false) Integer atmosphericPressureDegree,
                                  @RequestParam(required = false) Long incomeModsId) {

        return climatRepository.findAllWithFilter(pageable,
                id, name,temperatureDegree,windDegree,humidityDegree, atmosphericPressureDegree,incomeModsId, sortBy, sortDir);
    }

    @GetMapping("/climat/{climatId}")
    public Climat getClimatById(@PathVariable Long climatId) {
        var climat = climatRepository.findById(climatId);
        if(climat.isPresent())
            return climat.get();
        else throw new ResourceNotFoundException("Climat not found with id " + climatId);

    }

    @PostMapping("/climat")
    public Climat createClimat(@Valid @RequestBody ClimatDTO climatRequest) {
        var climat = new Climat();
        return incomeAffectableModeRepository.findById(climatRequest.getIncomeModsId())
                .map(incomeMods -> {
                    climat.setIncomeModsId(incomeMods);
                    climat.setName(climatRequest.getName());
                    climat.setTemperatureDegree(climatRequest.getTemperatureDegree());
                    climat.setWindDegree(climatRequest.getWindDegree());
                    climat.setHumidityDegree(climatRequest.getHumidityDegree());
                    climat.setAtmosphericPressureDegree(climatRequest.getAtmosphericPressureDegree());
                    return climatRepository.save(climat);
                }).orElseThrow(() -> new ResourceNotFoundException("IncomeMods not found with id "
                        + climatRequest.getIncomeModsId()));
    }

    @PutMapping("/climat/{climatId}")
    public Climat updateClimat(@PathVariable Long climatId,
                           @Valid @RequestBody ClimatDTO climatRequest) {
        if(!incomeAffectableModeRepository.existsById(climatRequest.getIncomeModsId())) {
            throw new ResourceNotFoundException("IncomeMods not found with id " + climatRequest.getIncomeModsId());
        }
        return climatRepository.findById(climatId)
                .map(climat -> {
                    climat.setName(climatRequest.getName());
                    climat.setTemperatureDegree(climatRequest.getTemperatureDegree());
                    climat.setWindDegree(climatRequest.getWindDegree());
                    climat.setHumidityDegree(climatRequest.getHumidityDegree());
                    climat.setAtmosphericPressureDegree(climatRequest.getAtmosphericPressureDegree());
                    climat.setIncomeModsId(incomeAffectableModeRepository.findById(climatRequest.getIncomeModsId()).get());
                    return climatRepository.save(climat);
                }).orElseThrow(() -> new ResourceNotFoundException("Climat not found with id " + climatId));
    }


    @DeleteMapping("/climat/{climatId}")
    public ResponseEntity<Object> deleteClimat(@PathVariable Long climatId) {
        return climatRepository.findById(climatId)
                .map(climat -> {
                    climatRepository.delete(climat);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Climat not found with id " + climatId));
    }
}
