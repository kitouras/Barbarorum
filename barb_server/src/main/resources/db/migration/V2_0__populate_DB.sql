INSERT INTO t_user(id, username, password) values (111, 'user', 'user');
INSERT INTO fraction(id, name, rebellion_tendency) values (211, 'merchants', 0.5);
INSERT INTO fraction(id, name, rebellion_tendency) values (212, 'military', 0.7);
INSERT INTO fraction(id, name, rebellion_tendency) values (213, 'priests', 0.2);
INSERT INTO income_affectable_mode(id, name, village_tax_multiplier, city_tax_multiplier,
food_multiplier, trade_multiplier, handicraft_multiplier, food_marketing_multiplier,
trade_marketing_multiplier, handicraft_marketing_multiplier) values (311,'test',0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5);
INSERT INTO income_affectable_mode(id, name, village_tax_multiplier, city_tax_multiplier,
food_multiplier, trade_multiplier, handicraft_multiplier, food_marketing_multiplier,
trade_marketing_multiplier, handicraft_marketing_multiplier) values (312,'test',0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5);
INSERT INTO income_affectable_mode(id, name, village_tax_multiplier, city_tax_multiplier,
food_multiplier, trade_multiplier, handicraft_multiplier, food_marketing_multiplier,
trade_marketing_multiplier, handicraft_marketing_multiplier) values (313,'test',0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5);
INSERT INTO income_affectable_mode(id, name, village_tax_multiplier, city_tax_multiplier,
food_multiplier, trade_multiplier, handicraft_multiplier, food_marketing_multiplier,
trade_marketing_multiplier, handicraft_marketing_multiplier) values (314,'test',0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5);
INSERT INTO income_affectable_mode(id, name, village_tax_multiplier, city_tax_multiplier,
food_multiplier, trade_multiplier, handicraft_multiplier, food_marketing_multiplier,
trade_marketing_multiplier, handicraft_marketing_multiplier) values (315,'test',0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5);
INSERT INTO desease(id, name, incidence_rate, mortality) values (411, 'plague', 0.2, 0.5);
INSERT INTO desease(id, name, incidence_rate, mortality) values (412, 'cholera', 0.2, 0.5);
INSERT INTO desease(id, name, incidence_rate, mortality) values (413, 'rabies', 0.2, 0.5);
INSERT INTO resource(id, name, population_growth_multiplier) values (511, 'trade', 0.2);
INSERT INTO resource(id, name, population_growth_multiplier) values (512, 'food', 2.5);
INSERT INTO resource(id, name, population_growth_multiplier) values (513, 'handicraft', 0.2);
INSERT INTO relation(id, name, fractions_connectivity) values (611, 'vassalage', 0.7);
INSERT INTO relation(id, name, fractions_connectivity) values (612, 'union', 0.2);
INSERT INTO relation(id, name, fractions_connectivity) values (613, 'federation', 0.1);
INSERT INTO state(id, name, popularity, color, treasury) values (711, 'Anacreon', 2000, 'FFB200', 2000);
INSERT INTO state(id, name, popularity, color, treasury) values (712, 'Marmandia', 1500, 'FFF838', 1500);
INSERT INTO state(id, name, popularity, color, treasury) values (713, 'Antium', 2000, '2DEAFF', 2000);
INSERT INTO specialization(id, name, income_mods_id) values (811, 'trading', 311);
INSERT INTO specialization(id, name, income_mods_id) values (812, 'military', 311);
INSERT INTO specialization(id, name, income_mods_id) values (813, 'handicrafting', 311);
INSERT INTO city(id, name, popularity, specialization_id) values (911, 'Trentor', 1000, 811);
INSERT INTO city(id, name, popularity, specialization_id) values (912, 'Naamor', 1000, 811);
INSERT INTO city(id, name, popularity, specialization_id) values (913, 'Aurlitz', 1000, 812);
INSERT INTO legal_system(id, group_name, is_tax_fixed, tax_above_multiplier,
autonomy_factor, income_mods_id) values (1011, 'barbarian', true, 0, 1, 315);
INSERT INTO legal_system(id, group_name, is_tax_fixed, tax_above_multiplier,
autonomy_factor, income_mods_id) values (1012, 'antium', false, 0.5, 0.3, 315);
INSERT INTO legal_system(id, group_name, is_tax_fixed, tax_above_multiplier,
autonomy_factor, income_mods_id) values (1013, 'mercium', false, 0.7, -0.2, 315);
INSERT INTO landscape(id, name, mountainousness, income_mods_id) values (1111, 'plain', 1, 313);
INSERT INTO landscape(id, name, mountainousness, income_mods_id) values (1112, 'lowland', 0, 313);
INSERT INTO landscape(id, name, mountainousness, income_mods_id) values (1113, 'hills', 2, 313);
INSERT INTO culture(id, name, tolerance, income_mods_id) values (1211, 'atlantium', 0.5, 313);
INSERT INTO culture(id, name, tolerance, income_mods_id) values (1212, 'nordic', -0.5, 313);
INSERT INTO culture(id, name, tolerance, income_mods_id) values (1213, 'barbarian', -1, 313);
INSERT INTO religion(id, name, tolerance, income_mods_id) values (1311, 'ivalismum', 0.5, 313);
INSERT INTO religion(id, name, tolerance, income_mods_id) values (1312, 'hadjitism', -0.5, 313);
INSERT INTO religion(id, name, tolerance, income_mods_id) values (1313, 'paganism', -1, 313);
INSERT INTO season(id, name, temperature_mod, wind_mod, humidity_mod, atmospheric_pressure_mod,
income_mods_id) values (1411, 'summer', 2, 1, 1.5, 1.5, 313);
INSERT INTO season(id, name, temperature_mod, wind_mod, humidity_mod, atmospheric_pressure_mod,
income_mods_id) values (1412, 'fall', 1, 1.5, 1, 1.5, 313);
INSERT INTO season(id, name, temperature_mod, wind_mod, humidity_mod, atmospheric_pressure_mod,
income_mods_id) values (1413, 'winter', 0.5, 1.5, 1.5, 1, 313);
INSERT INTO season(id, name, temperature_mod, wind_mod, humidity_mod, atmospheric_pressure_mod,
income_mods_id) values (1414, 'spring', 1, 1, 2, 1.5, 313);
INSERT INTO climat(id, name, temperature_degree, wind_degree, humidity_degree, atmospheric_pressure_degree,
income_mods_id) values (1511, 'tropical', 30, 20, 80, 740, 313);
INSERT INTO climat(id, name, temperature_degree, wind_degree, humidity_degree, atmospheric_pressure_degree,
income_mods_id) values (1512, 'subtropical', 25, 30, 60, 760, 313);
INSERT INTO climat(id, name, temperature_degree, wind_degree, humidity_degree, atmospheric_pressure_degree,
income_mods_id) values (1513, 'arctic', -20, 40, 60, 790, 313);
INSERT INTO waterspace(id, depth, width, season_id, climat_id) values (1611, 100, 50, 1413, 1513);
INSERT INTO waterspace(id, depth, width, season_id, climat_id) values (1612, 300, 50, 1411, 1511);
INSERT INTO waterspace(id, depth, width, season_id, climat_id) values (1613, 100, 200, 1411, 1511);
INSERT INTO neighborhood_of_waterspaces(first_waterspace_id, second_waterspace_id) values (1612, 1613);
INSERT INTO province(id, name, treasury, tax_into, tax_above, autonomy, rebellion_potential, village_popularity,
popularity_growth, city_id, state_id, legal_system_id, season_id, climat_id, landscape_id)
values (1711, 'Calambrium', 200, 20, 10, 50, 20, 20000, 100, 911, 711, 1011, 1411, 1512, 1111);
INSERT INTO province(id, name, treasury, tax_into, tax_above, autonomy, rebellion_potential, village_popularity,
popularity_growth, city_id, state_id, legal_system_id, season_id, climat_id, landscape_id)
values (1712, 'Faverra', 2000, 20, 10, 50, 20, 20000, 100, 912, 711, 1013, 1411, 1512, 1111);
INSERT INTO province(id, name, treasury, tax_into, tax_above, autonomy, rebellion_potential, village_popularity,
popularity_growth, city_id, state_id, legal_system_id, season_id, climat_id, landscape_id)
values (1713, 'Tlapana', 2000000, 20, 10, 50, 20, 20000, 100, 913, 711, 1013, 1411, 1512, 1111);



