CREATE TABLE IF NOT EXISTS t_user
(
 "id"               serial NOT NULL,
 username               text NOT NULL UNIQUE,
 password               text NOT NULL,
 CONSTRAINT t_user_pk PRIMARY KEY ( "id" )
);