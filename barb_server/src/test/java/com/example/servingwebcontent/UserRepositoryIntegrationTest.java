package com.example.servingwebcontent;

import com.example.servingwebcontent.model.User;
import com.example.servingwebcontent.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
class UserRepositoryIntegrationTest implements AbstractIntegrationTest{
    @Autowired
    UserRepository userRepository;

    @Test
    void userTest(){
        userRepository.save(new User(0, "fwefsdfa", "edfgsdfgsg"));
        userRepository.save(new User(0, "fwefsdf", "edfgsdfgsg"));
        User user = userRepository.findByUsername("fwefsdf").get();
        assertThat(user.getId(), is(2L));
    }
}
