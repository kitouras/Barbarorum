CREATE TABLE IF NOT EXISTS fraction
(
 "id"                 serial NOT NULL,
 name               text NOT NULL,
 rebellion_tendency double precision NOT NULL,
 CONSTRAINT fraction_pk PRIMARY KEY ( "id" ),
 CONSTRAINT rebellion_tendency_boundaries CHECK (rebellion_tendency >= -1 AND rebellion_tendency <= 1)
);
CREATE TABLE IF NOT EXISTS income_affectable_mode
(
 "id"                              serial NOT NULL,
 name text NOT NULL,
 village_tax_multiplier          double precision NOT NULL,
 city_tax_multiplier             double precision NOT NULL,
 food_multiplier                 double precision NOT NULL,
 trade_multiplier                double precision NOT NULL,
 handicraft_multiplier           double precision NOT NULL,
 food_marketing_multiplier       double precision NOT NULL,
 trade_marketing_multiplier      double precision NOT NULL,
 handicraft_marketing_multiplier double precision NOT NULL,
 CONSTRAINT PK_table_174 PRIMARY KEY ( "id" )
);

CREATE TABLE IF NOT EXISTS desease
(
 "id"             serial NOT NULL,
 name           text NOT NULL,
 incidence_rate double precision NOT NULL,
 mortality      double precision NOT NULL,
 CONSTRAINT desease_pk PRIMARY KEY ( "id" ),
 CONSTRAINT incidence_rate_boundaries CHECK (incidence_rate >= 0 AND incidence_rate <= 1),
 CONSTRAINT mortality_boundaries CHECK (mortality >= 0 AND mortality <= 1)
);


CREATE TABLE IF NOT EXISTS resource
(
 "id"                           serial NOT NULL,
 name                         text NOT NULL,
 population_growth_multiplier double precision NOT NULL,
 CONSTRAINT resource_pk PRIMARY KEY ( "id" )
);



CREATE TABLE IF NOT EXISTS relation
(
 "id"                     serial NOT NULL,
 name                   text NOT NULL,
 fractions_connectivity double precision NOT NULL,
 CONSTRAINT relation_pk PRIMARY KEY ( "id" )
);



CREATE TABLE IF NOT EXISTS "state"
(
 "id"         serial NOT NULL,
 name       text NOT NULL,
 popularity int NOT NULL,
 color      text NOT NULL,
 treasury   int NOT NULL,
 CONSTRAINT state_pk PRIMARY KEY ( "id" ),
 CONSTRAINT treasury_not_negativity CHECK (treasury >= 0),
 CONSTRAINT popularity_not_negativity CHECK (popularity >= 0)

);



CREATE TABLE IF NOT EXISTS specialization
(
 "id"             serial NOT NULL,
 name           text NOT NULL,
 income_mods_id integer NOT NULL,
 CONSTRAINT specialization_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_211 FOREIGN KEY ( income_mods_id ) REFERENCES income_affectable_mode ( "id" ) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS fkIdx_212 ON specialization
(
 income_mods_id
);

CREATE TABLE IF NOT EXISTS city
(
 "id"                serial NOT NULL,
 specialization_id integer NOT NULL,
 name              text NOT NULL,
 popularity        int NOT NULL,
 CONSTRAINT city_pk PRIMARY KEY ( "id" ),
 CONSTRAINT city_fk0 FOREIGN KEY ( specialization_id ) REFERENCES specialization ( "id" ) ON DELETE CASCADE,
 CONSTRAINT popularity_not_negativity CHECK (popularity >= 0)
);

CREATE TABLE IF NOT EXISTS legal_system
(
 "id"                   serial NOT NULL,
 group_name           text NOT NULL,
 is_tax_fixed         boolean NOT NULL,
 tax_above_multiplier double precision NOT NULL,
 autonomy_factor      double precision NOT NULL,
 income_mods_id       integer NOT NULL,
 CONSTRAINT legal_system_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_187 FOREIGN KEY ( income_mods_id ) REFERENCES income_affectable_mode ( "id" ) ON DELETE CASCADE,
 CONSTRAINT tax_above_multiplier_boundaries CHECK (tax_above_multiplier >= -1 AND tax_above_multiplier <= 1),
 CONSTRAINT autonomy_factor_boundaries CHECK (autonomy_factor >= -1 AND autonomy_factor <= 1)
);

CREATE INDEX IF NOT EXISTS fkIdx_188 ON legal_system
(
 income_mods_id
);


CREATE TABLE IF NOT EXISTS landscape
(
 "id"              serial NOT NULL,
 name            text NOT NULL,
 income_mods_id  integer NOT NULL,
 mountainousness int NOT NULL,
 CONSTRAINT landscape_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_196 FOREIGN KEY ( income_mods_id ) REFERENCES income_affectable_mode ( "id" ) ON DELETE CASCADE,
 CONSTRAINT mountainousness_boundaries CHECK (mountainousness >= 0 AND mountainousness <= 5)
);

CREATE INDEX IF NOT EXISTS fkIdx_197 ON landscape
(
 income_mods_id
);


CREATE TABLE IF NOT EXISTS culture
(
 "id"             serial NOT NULL,
 income_mods_id integer NOT NULL,
 name           text NOT NULL,
 tolerance      double precision NOT NULL,
 CONSTRAINT culture_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_214 FOREIGN KEY ( income_mods_id ) REFERENCES income_affectable_mode ( "id" ) ON DELETE CASCADE,
 CONSTRAINT tolerance_boundaries CHECK (tolerance >= -1 AND tolerance <= 1)
);

CREATE INDEX IF NOT EXISTS fkIdx_215 ON culture
(
 income_mods_id
);


CREATE TABLE IF NOT EXISTS religion
(
 "id"             serial NOT NULL,
 income_mods_id integer NOT NULL,
 name           text NOT NULL,
 tolerance      double precision NOT NULL,
 CONSTRAINT religion_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_217 FOREIGN KEY ( income_mods_id ) REFERENCES income_affectable_mode ( "id" ) ON DELETE CASCADE,
 CONSTRAINT tolerance_boundaries CHECK (tolerance >= -1 AND tolerance <= 1)
);

CREATE INDEX IF NOT EXISTS fkIdx_218 ON religion
(
 income_mods_id
);



CREATE TABLE IF NOT EXISTS season
(
 "id"                       serial NOT NULL,
 name                     text NOT NULL,
 income_mods_id           integer NOT NULL,
 temperature_mod          double precision NOT NULL,
 wind_mod                 double precision NOT NULL,
 humidity_mod             double precision NOT NULL,
 atmospheric_pressure_mod double precision NOT NULL,
 CONSTRAINT season_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_193 FOREIGN KEY ( income_mods_id ) REFERENCES income_affectable_mode ( "id" ) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS fkIdx_194 ON season
(
 income_mods_id
);



CREATE TABLE IF NOT EXISTS climat
(
 "id"                          serial NOT NULL,
 name                        text NOT NULL,
 income_mods_id              integer NOT NULL,
 temperature_degree          int NOT NULL,
 wind_degree                 int NOT NULL,
 humidity_degree             int NOT NULL,
 atmospheric_pressure_degree int NOT NULL,
 CONSTRAINT climat_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_190 FOREIGN KEY ( income_mods_id ) REFERENCES income_affectable_mode ( "id" ) ON DELETE CASCADE,
 CONSTRAINT wind_degree_not_negativity CHECK (wind_degree >= 0),
 CONSTRAINT humidity_degree_not_negativity CHECK (humidity_degree >= 0),
 CONSTRAINT atmospheric_pressure_degree_not_negativity CHECK (atmospheric_pressure_degree >= 0)
);

CREATE INDEX IF NOT EXISTS fkIdx_191 ON climat
(
 income_mods_id
);



CREATE TABLE IF NOT EXISTS waterspace
(
 "id"        serial NOT NULL,
 climat_id integer NOT NULL,
 season_id integer NOT NULL,
 "depth"     int NOT NULL,
 width     int NOT NULL,
 CONSTRAINT waterspace_pk PRIMARY KEY ( "id" ),
 CONSTRAINT FK_290 FOREIGN KEY ( season_id ) REFERENCES season ( "id" ) ON DELETE CASCADE,
 CONSTRAINT FK_296 FOREIGN KEY ( climat_id ) REFERENCES climat ( "id" ) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS fkIdx_291 ON waterspace
(
 season_id
);

CREATE INDEX IF NOT EXISTS fkIdx_297 ON waterspace
(
 climat_id
);



CREATE TABLE IF NOT EXISTS neighborhood_of_waterspaces
(
 first_waterspace_id  integer NOT NULL,
 second_waterspace_id integer NOT NULL,
 CONSTRAINT PK_neighborhood_of_waterspaces PRIMARY KEY ( first_waterspace_id, second_waterspace_id ),
 CONSTRAINT FK_276 FOREIGN KEY ( first_waterspace_id ) REFERENCES waterspace ( "id" ) ON DELETE CASCADE,
 CONSTRAINT FK_279 FOREIGN KEY ( second_waterspace_id ) REFERENCES waterspace ( "id" ) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS fkIdx_277 ON neighborhood_of_waterspaces
(
 first_waterspace_id
);

CREATE INDEX IF NOT EXISTS fkIdx_280 ON neighborhood_of_waterspaces
(
 second_waterspace_id
);





CREATE TABLE IF NOT EXISTS province
(
 "id"                  serial NOT NULL,
 name                text NOT NULL UNIQUE,
 treasury            double precision NOT NULL,
 tax_into            double precision NOT NULL,
 tax_above           double precision NOT NULL,
 autonomy            int NOT NULL,
 rebellion_potential int NOT NULL,
 village_popularity  int NOT NULL,
 popularity_growth   int NOT NULL,
 city_id             integer NOT NULL UNIQUE,
 state_id            integer NOT NULL,
 legal_system_id     integer NOT NULL,
 climat_id           integer NOT NULL,
 season_id           integer NOT NULL,
 landscape_id        integer NOT NULL,
 CONSTRAINT province_pk PRIMARY KEY ( "id" ),
 CONSTRAINT AK1_province UNIQUE ( name ),
 CONSTRAINT FK_136 FOREIGN KEY ( city_id ) REFERENCES city ( "id" ) ON DELETE CASCADE,
 CONSTRAINT FK_139 FOREIGN KEY ( state_id ) REFERENCES "state" ( "id" ) ON DELETE CASCADE,
 CONSTRAINT FK_142 FOREIGN KEY ( legal_system_id ) REFERENCES legal_system ( "id" ) ON DELETE CASCADE,
 CONSTRAINT FK_145 FOREIGN KEY ( climat_id ) REFERENCES climat ( "id" ) ON DELETE CASCADE,
 CONSTRAINT FK_148 FOREIGN KEY ( season_id ) REFERENCES season ( "id" ) ON DELETE CASCADE,
 CONSTRAINT FK_151 FOREIGN KEY ( landscape_id ) REFERENCES landscape ( "id" ) ON DELETE CASCADE,
 CONSTRAINT treasury_not_negativity CHECK (treasury >= 0),
 CONSTRAINT autonomy_boundaries CHECK (autonomy >= 0 AND autonomy <= 100),
 CONSTRAINT rebellion_potential_boundaries CHECK (rebellion_potential >= 0 AND rebellion_potential <= 100),
 CONSTRAINT village_popularity_not_negativity CHECK (village_popularity >= 0)
);

CREATE INDEX IF NOT EXISTS fkIdx_137 ON province
(
 city_id
);

CREATE INDEX IF NOT EXISTS fkIdx_140 ON province
(
 state_id
);

CREATE INDEX IF NOT EXISTS fkIdx_143 ON province
(
 legal_system_id
);

CREATE INDEX IF NOT EXISTS fkIdx_146 ON province
(
 climat_id
);

CREATE INDEX IF NOT EXISTS fkIdx_149 ON province
(
 season_id
);

CREATE INDEX IF NOT EXISTS fkIdx_152 ON province
(
 landscape_id
);







CREATE TABLE IF NOT EXISTS neighborhood_of_provinces
(
 first_province_id  integer NOT NULL,
 second_province_id integer NOT NULL,
 relation_id        integer NOT NULL,
 "degree"             int NOT NULL,
 CONSTRAINT neighborhood_of_provinces_pk PRIMARY KEY ( first_province_id, second_province_id ),
 CONSTRAINT neighborhood_of_provinces_fk0 FOREIGN KEY ( first_province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT neighborhood_of_provinces_fk1 FOREIGN KEY ( second_province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT neighborhood_of_provinces_fk2 FOREIGN KEY ( relation_id ) REFERENCES relation ( "id" ) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS province_cultures
(
 province_id integer NOT NULL,
 culture_id  integer NOT NULL,
 percentage  double precision NOT NULL,
 CONSTRAINT province_cultures_pk PRIMARY KEY ( province_id, culture_id ),
 CONSTRAINT province_cultures_fk0 FOREIGN KEY ( province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT province_cultures_fk1 FOREIGN KEY ( culture_id ) REFERENCES culture ( "id" ) ON DELETE CASCADE,
 CONSTRAINT percentage_boundaries CHECK (percentage >= 0 AND percentage <= 1)
);

CREATE TABLE IF NOT EXISTS province_deseases
(
 province_id         integer NOT NULL,
 desease_id          integer NOT NULL,
 prevalence          int NOT NULL,
 collective_immunity double precision NOT NULL,
 CONSTRAINT province_deseases_pk PRIMARY KEY ( province_id, desease_id ),
 CONSTRAINT province_deseases_fk0 FOREIGN KEY ( province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT province_deseases_fk1 FOREIGN KEY ( desease_id ) REFERENCES desease ( "id" ) ON DELETE CASCADE,
 CONSTRAINT prevalence_not_negativity CHECK (prevalence >= 0),
 CONSTRAINT collective_immunity_boundaries CHECK (collective_immunity >= 0 AND collective_immunity <= 1)
);


CREATE TABLE IF NOT EXISTS province_fractions
(
 province_id integer NOT NULL,
 fraction_id integer NOT NULL,
 authority   int NOT NULL,
 "integrity"   int NOT NULL,
 CONSTRAINT province_fractions_pk PRIMARY KEY ( province_id, fraction_id ),
 CONSTRAINT province_fractions_fk0 FOREIGN KEY ( province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT province_fractions_fk1 FOREIGN KEY ( fraction_id ) REFERENCES fraction ( "id" ) ON DELETE CASCADE,
 CONSTRAINT authority_boundaries CHECK (authority >= 0 AND authority <= 100),
 CONSTRAINT integrity_boundaries CHECK (integrity >= 0 AND integrity <= 100)
);


CREATE TABLE IF NOT EXISTS province_religions
(
 province_id integer NOT NULL,
 religion_id integer NOT NULL,
 percentage  double precision NOT NULL,
 CONSTRAINT province_religions_pk PRIMARY KEY ( province_id, religion_id ),
 CONSTRAINT province_religions_fk0 FOREIGN KEY ( province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT province_religions_fk1 FOREIGN KEY ( religion_id ) REFERENCES religion ( "id" ) ON DELETE CASCADE,
 CONSTRAINT percentage_boundaries CHECK (percentage >= 0 AND percentage <= 1)
);

CREATE TABLE IF NOT EXISTS province_resources_lifecycle
(
 province_id         integer NOT NULL,
 resource_id         integer NOT NULL,
 resource_stock      int NOT NULL,
 resource_production int NOT NULL,
 resource_limit      int NOT NULL,
 resource_marketing  int NOT NULL,
 CONSTRAINT province_resources_lifecycle_pk PRIMARY KEY ( province_id, resource_id ),
 CONSTRAINT province_resources_lifecycle_fk0 FOREIGN KEY ( province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT province_resources_lifecycle_fk1 FOREIGN KEY ( resource_id ) REFERENCES resource ( "id" ) ON DELETE CASCADE,
 CONSTRAINT resource_stock_not_negativity CHECK (resource_stock >= 0),
 CONSTRAINT resource_production_not_negativity CHECK (resource_production >= 0),
 CONSTRAINT resource_limit_not_negativity CHECK (resource_limit >= 0),
 CONSTRAINT resource_marketing_not_negativity CHECK (resource_marketing >= 0)
);



CREATE TABLE IF NOT EXISTS province_waterspace_neighboorhood
(
 province_id   integer NOT NULL,
 waterspace_id integer NOT NULL,
 CONSTRAINT province_waterspace_neighboorhood_pk PRIMARY KEY ( province_id, waterspace_id ),
 CONSTRAINT province_waterspace_neighboorhood_fk0 FOREIGN KEY ( province_id ) REFERENCES province ( "id" ) ON DELETE CASCADE,
 CONSTRAINT province_waterspace_neighboorhood_fk1 FOREIGN KEY ( waterspace_id ) REFERENCES waterspace ( "id" ) ON DELETE CASCADE
);

