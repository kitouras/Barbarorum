import React, { Component } from "react";
import { provinceWaterspaceNeighborhoodService, provinceService, waterspaceService } from '@/_services';

export default class AddProvinceWaterspaceNeighborhood extends Component {
    constructor(props) {
        super(props);
        this.onChangeProvinceId = this.onChangeProvinceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getProvince = this.getProvince.bind(this);
        this.onChangeProvincePage = this.onChangeProvincePage.bind(this);
        this.OnClickAddProvince = this.OnClickAddProvince.bind(this);
        this.onChangeWaterspaceId = this.onChangeWaterspaceId.bind(this);
        this.getWaterspace = this.getWaterspace.bind(this);
        this.onChangeWaterspacePage = this.onChangeWaterspacePage.bind(this);
        this.OnClickAddWaterspace = this.OnClickAddWaterspace.bind(this);

        this.state = {
            
            provinceId: 0,
            waterspaceId: 0,

            provinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentProvincePage: 0,

            waterspaceIds: [],
            waterspaceIdsTotalPages: 1,
            waterspaceIdsPages: [],
            currentWaterspacePage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getProvince(this.state.currentProvincePage);
        this.getWaterspace(this.state.currentWaterspacePage);
        if (localStorage.getItem("current_provinceWaterspaceNeighborhood_state") != null) {
            console.log(localStorage.getItem("current_provinceWaterspaceNeighborhood_state"));
            const new_state = JSON.parse(localStorage.getItem("current_provinceWaterspaceNeighborhood_state"));
            this.setState({
                
                provinceId: new_state.provinceId,
                waterspaceId: new_state.waterspaceId,
                provinceIds: new_state.provinceIds,
                provinceIdsTotalPages: new_state.provinceIdsTotalPages,
                provinceIdsPages: new_state.provinceIdsPages,
                currentProvincePage: new_state.currentProvincePage,

                waterspaceIds: new_state.waterspaceIds,
                waterspaceIdsTotalPages: new_state.waterspaceIdsTotalPages,
                waterspaceIdsPages: new_state.waterspaceIdsPages,
                currentWaterspacePage: new_state.currentProvincePage,
                message: new_state.message
            });
            localStorage.removeItem('current_provinceWaterspaceNeighborhood_state');
        }
        
    }

    
    onChangeProvinceId(e) {
        this.setState({
            provinceId: e.target.value
        });
    }
    onChangeProvincePage(e) {
        const page = e.target.value;
        this.getProvince(page);
        this.setState({
            currentProvincePage: page
        });
    }

    onChangeWaterspaceId(e) {
        this.setState({
            waterspaceId: e.target.value
        });
    }
    onChangeWaterspacePage(e) {
        const page = e.target.value;
        this.getWaterspace(page);
        this.setState({
            currentWaterspacePage: page
        });
    }

    getProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    provinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    provinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getWaterspace(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        waterspaceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    waterspaceIds: content,
                    waterspaceIdsTotalPages: totalPages,
                    waterspaceIdsPages: pages,
                    waterspaceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {
            
            provinceId: this.state.provinceId,
            waterspaceId: this.state.waterspaceId
        };

        provinceWaterspaceNeighborhoodService.createEntity(data)
            .then(response => {
                this.setState({
                    
                    provinceId: response.provinceId,
                    waterspaceId: response.waterspaceId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddProvince() {
        localStorage.setItem('current_provinceWaterspaceNeighborhood_state', JSON.stringify(this.state));
        this.props.history.push('/add/province');
    }
    OnClickAddWaterspace() {
        localStorage.setItem('current_provinceWaterspaceNeighborhood_state', JSON.stringify(this.state));
        this.props.history.push('/add/waterspace');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"ProvinceId: "}
                                <select onChange={this.onChangeProvinceId} value={this.state.provinceId}>
                                    {this.state.provinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeProvincePage} value={this.state.currentProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"WaterspaceId: "}
                                <select onChange={this.onChangeWaterspaceId} value={this.state.waterspaceId}>
                                    {this.state.waterspaceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeWaterspacePage} value={this.state.currentWaterspacePage}>
                                    {this.state.waterspaceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddWaterspace}
                                >
                                    Add
            </button>
                            </div>
                            





                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddProvinceWaterspaceNeighborhood };