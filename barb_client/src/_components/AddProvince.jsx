import React, { Component } from "react";
import { provinceService, cityService, stateService, legalSystemService, climatService, seasonService, landscapeService } from '@/_services';

export default class AddProvince extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeAutonomy = this.onChangeAutonomy.bind(this);
        this.onChangeRebellionPotential = this.onChangeRebellionPotential.bind(this);
        this.onChangeVillagePopularity = this.onChangeVillagePopularity.bind(this);
        this.onChangePopularityGrowth = this.onChangePopularityGrowth.bind(this);
        this.onChangeTreasury = this.onChangeTreasury.bind(this);
        this.onChangeTaxInto = this.onChangeTaxInto.bind(this);
        this.onChangeTaxAbove = this.onChangeTaxAbove.bind(this);
        this.onChangeClimatId = this.onChangeClimatId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getClimatId = this.getClimatId.bind(this);
        this.onChangeClimatPage = this.onChangeClimatPage.bind(this);
        this.OnClickAddClimat = this.OnClickAddClimat.bind(this);
        this.onChangeSeasonId = this.onChangeSeasonId.bind(this);
        this.getSeasonId = this.getSeasonId.bind(this);
        this.onChangeSeasonPage = this.onChangeSeasonPage.bind(this);
        this.OnClickAddSeason = this.OnClickAddSeason.bind(this);
        this.onChangeCityId = this.onChangeCityId.bind(this);
        this.getCityId = this.getCityId.bind(this);
        this.onChangeCityPage = this.onChangeCityPage.bind(this);
        this.OnClickAddCity = this.OnClickAddCity.bind(this);
        this.onChangeStateId = this.onChangeStateId.bind(this);
        this.getStateId = this.getStateId.bind(this);
        this.onChangeStatePage = this.onChangeStatePage.bind(this);
        this.OnClickAddState = this.OnClickAddState.bind(this);
        this.onChangeLegalSystemId = this.onChangeLegalSystemId.bind(this);
        this.getLegalSystemId = this.getLegalSystemId.bind(this);
        this.onChangeLegalSystemPage = this.onChangeLegalSystemPage.bind(this);
        this.OnClickAddLegalSystem = this.OnClickAddLegalSystem.bind(this);
        this.onChangeLandscapeId = this.onChangeLandscapeId.bind(this);
        this.getLandscapeId = this.getLandscapeId.bind(this);
        this.onChangeLandscapePage = this.onChangeLandscapePage.bind(this);
        this.OnClickAddLandscape = this.OnClickAddLandscape.bind(this);

        this.state = {
            id: null,
            name: "d",
            autonomy: 0,
            rebellionPotential: 0,
            villagePopularity: 0,
            popularityGrowth: 0,
            treasury: 0,
            taxInto: 0,
            taxAbove: 0,
            cityId: 0,
            stateId: 0,
            legalSystemId: 0,
            climatId: 0,
            seasonId: 0,
            landscapeId: 0,

            cityIds: [],
            cityIdsTotalPages: 1,
            cityIdsPages: [],
            currentCityPage: 0,

            stateIds: [],
            stateIdsTotalPages: 1,
            stateIdsPages: [],
            currentStatePage: 0,

            legalSystemIds: [],
            legalSystemIdsTotalPages: 1,
            legalSystemIdsPages: [],
            currentLegalSystemPage: 0,

            climatIds: [],
            climatIdsTotalPages: 1,
            climatIdsPages: [],
            currentClimatPage: 0,

            seasonIds: [],
            seasonIdsTotalPages: 1,
            seasonIdsPages: [],
            currentSeasonPage: 0,

            landscapeIds: [],
            landscapeIdsTotalPages: 1,
            landscapeIdsPages: [],
            currentLandscapePage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getCityId(this.state.currentCityPage);
        this.getStateId(this.state.currentStatePage);
        this.getLegalSystemId(this.state.currentLegalSystemPage);
        this.getClimatId(this.state.currentClimatPage);
        this.getSeasonId(this.state.currentSeasonPage);
        this.getLandscapeId(this.state.currentLandscapePage);
        if (localStorage.getItem("current_province_state") != null) {
            console.log(localStorage.getItem("current_province_state"));
            const new_state = JSON.parse(localStorage.getItem("current_province_state"));
            this.setState({
                name: new_state.name,
                autonomy: new_state.autonomy,
                rebellionPotential: new_state.rebellionPotential,
                villagePopularity: new_state.villagePopularity,
                popularityGrowth: new_state.popularityGrowth,
                treasury: new_state.treasury,
                taxInto: new_state.taxInto,
                taxAbove: new_state.taxAbove,
                cityId: new_state.cityId,
                stateId: new_state.stateId,
                legalSystemId: new_state.legalSystemId,
                climatId: new_state.climatId,
                seasonId: new_state.seasonId,
                landscapeId: new_state.landscapeId,

                cityIds: new_state.cityIds,
                cityIdsTotalPages: new_state.cityIdsTotalPages,
                cityIdsPages: new_state.cityIdsPages,
                currentCityPage: new_state.currentCityPage,

                stateIds: new_state.stateIds,
                stateIdsTotalPages: new_state.stateIdsTotalPages,
                stateIdsPages: new_state.stateIdsPages,
                currentStatePage: new_state.currentStatePage,

                legalSystemIds: new_state.legalSystemIds,
                legalSystemIdsTotalPages: new_state.legalSystemIdsTotalPages,
                legalSystemIdsPages: new_state.legalSystemIdsPages,
                currentLegalSystemPage: new_state.currentLegalSystemPage,

                climatIds: new_state.climatIds,
                climatIdsTotalPages: new_state.climatIdsTotalPages,
                climatIdsPages: new_state.climatIdsPages,
                currentClimatPage: new_state.currentClimatPage,

                seasonIds: new_state.seasonIds,
                seasonIdsTotalPages: new_state.seasonIdsTotalPages,
                seasonIdsPages: new_state.seasonIdsPages,
                currentSeasonPage: new_state.currentSeasonPage,

                landscapeIds: new_state.landscapeIds,
                landscapeIdsTotalPages: new_state.landscapeIdsTotalPages,
                landscapeIdsPages: new_state.landscapeIdsPages,
                currentLandscapePage: new_state.currentLandscapePage,

                message: new_state.message
            });
            localStorage.removeItem('current_province_state');
        }
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeAutonomy(e) {
        this.setState({
            autonomy: e.target.value
        });
    }

    onChangeRebellionPotential(e) {
        this.setState({
            rebellionPotential: e.target.value
        });
    }

    onChangeVillagePopularity(e) {
        this.setState({
            villagePopularity: e.target.value
        });
    }

    onChangePopularityGrowth(e) {
        this.setState({
            popularityGrowth: e.target.value
        });
    }

    onChangeTreasury(e) {
        this.setState({
            treasury: e.target.value
        });
    }
    onChangeTaxInto(e) {
        this.setState({
            taxInto: e.target.value
        });
    }
    onChangeTaxAbove(e) {
        this.setState({
            taxAbove: e.target.value
        });
    }


    onChangeCityId(e) {
        this.setState({
            cityId: e.target.value
        });
    }
    onChangeCityPage(e) {
        const page = e.target.value;
        this.getCityId(page);
        this.setState({
            currentCityPage: page
        });
    }
    onChangeStateId(e) {
        this.setState({
            stateId: e.target.value
        });
    }
    onChangeStatePage(e) {
        const page = e.target.value;
        this.getStateId(page);
        this.setState({
            currentStatePage: page
        });
    }
    onChangeLegalSystemId(e) {
        this.setState({
            legalSystemId: e.target.value
        });
    }
    onChangeLegalSystemPage(e) {
        const page = e.target.value;
        this.getLegalSystemId(page);
        this.setState({
            currentLegalSystemPage: page
        });
    }
    onChangeClimatId(e) {
        this.setState({
            climatId: e.target.value
        });
    }
    onChangeClimatPage(e) {
        const page = e.target.value;
        this.getClimatId(page);
        this.setState({
            currentClimatPage: page
        });
    }

    onChangeSeasonId(e) {
        this.setState({
            seasonId: e.target.value
        });
    }
    onChangeSeasonPage(e) {
        const page = e.target.value;
        this.getSeasonId(page);
        this.setState({
            currentSeasonPage: page
        });
    }
    onChangeLandscapeId(e) {
        this.setState({
            landscapeId: e.target.value
        });
    }
    onChangeLandscapePage(e) {
        const page = e.target.value;
        this.getLandscapeId(page);
        this.setState({
            currentLandscapePage: page
        });
    }

    getCityId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        cityService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    cityIds: content,
                    cityIdsTotalPages: totalPages,
                    cityIdsPages: pages,
                    cityId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getStateId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        stateService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    stateIds: content,
                    stateIdsTotalPages: totalPages,
                    stateIdsPages: pages,
                    stateId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getLegalSystemId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        legalSystemService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    legalSystemIds: content,
                    legalSystemIdsTotalPages: totalPages,
                    legalSystemIdsPages: pages,
                    legalSystemId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getSeasonId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        seasonService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    seasonIds: content,
                    seasonIdsTotalPages: totalPages,
                    seasonIdsPages: pages,
                    seasonId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getClimatId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        climatService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    climatIds: content,
                    climatIdsTotalPages: totalPages,
                    climatIdsPages: pages,
                    climatId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getLandscapeId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        landscapeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    landscapeIds: content,
                    landscapeIdsTotalPages: totalPages,
                    landscapeIdsPages: pages,
                    landscapeId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }




    saveEntity() {
        var data = {
            name: this.state.name,
            autonomy: this.state.autonomy,
            rebellionPotential: this.state.rebellionPotential,
            villagePopularity: this.state.villagePopularity,
            popularityGrowth: this.state.popularityGrowth,
            treasury: this.state.treasury,
            taxInto: this.state.taxInto,
            taxAbove: this.state.taxAbove,
            cityId: this.state.cityId,
            stateId: this.state.stateId,
            legalSystemId: this.state.legalSystemId,
            climatId: this.state.climatId,
            seasonId: this.state.seasonId,
            landscapeId: this.state.landscapeId
        };

        provinceService.createEntity(data)
            .then(response => {
                this.setState({
                    name: response.name,
                    autonomy: response.autonomy,
                    rebellionPotential: response.rebellionPotential,
                    villagePopularity: response.villagePopularity,
                    popularityGrowth: response.popularityGrowth,
                    treasury: response.treasury,
                    taxInto: response.taxInto,
                    taxAbove: response.taxAbove,
                    cityId: response.cityId,
                    stateId: response.stateId,
                    legalSystemId: response.legalSystemId,
                    climatId: response.climatId,
                    seasonId: response.seasonId,
                    landscapeId: response.landscapeId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            autonomy: 0,
            rebellionPotential: 0,
            villagePopularity: 0,
            popularityGrowth: 0,
            treasury: 0,
            taxInto: 0,
            taxAbove: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddCity() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/city');
    }
    OnClickAddState() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/state');
    }
    OnClickAddLegalSystem() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/legalSystem');
    }
    OnClickAddClimat() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/climat');
    }
    OnClickAddSeason() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/season');
    }
    OnClickAddLandscape() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/landscape');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="autonomy">autonomy</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="autonomy"
                                    required
                                    value={this.state.autonomy}
                                    onChange={this.onChangeAutonomy}
                                    name="autonomy"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="rebellionPotential">rebellionPotential</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="rebellionPotential"
                                    required
                                    value={this.state.rebellionPotential}
                                    onChange={this.onChangeRebellionPotential}
                                    name="rebellionPotential"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="villagePopularity">villagePopularity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="villagePopularity"
                                    required
                                    value={this.state.villagePopularity}
                                    onChange={this.onChangeVillagePopularity}
                                    name="villagePopularity"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="popularityGrowth">popularityGrowth</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="popularityGrowth"
                                    required
                                    value={this.state.popularityGrowth}
                                    onChange={this.onChangePopularityGrowth}
                                    name="popularityGrowth"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="treasury">treasury</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="treasury"
                                    required
                                    value={this.state.treasury}
                                    onChange={this.onChangeTreasury}
                                    name="treasury"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="taxInto">taxInto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="taxInto"
                                    required
                                    value={this.state.taxInto}
                                    onChange={this.onChangeTaxInto}
                                    name="taxInto"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="taxAbove">taxAbove</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="taxAbove"
                                    required
                                    value={this.state.taxAbove}
                                    onChange={this.onChangeTaxAbove}
                                    name="taxAbove"
                                />
                            </div>

                            <div className="mt-3 mb-1">
                                {"CityId: "}
                                <select onChange={this.onChangeCityId} value={this.state.cityId}>
                                    {this.state.cityIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeCityPage} value={this.state.currentCityPage}>
                                    {this.state.cityIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddCity}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"StateId: "}
                                <select onChange={this.onChangeStateId} value={this.state.stateId}>
                                    {this.state.stateIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeStatePage} value={this.state.currentStatePage}>
                                    {this.state.stateIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddState}
                                >
                                    Add
            </button>
                            </div>


                            <div className="mt-3 mb-1">
                                {"LegalSystemId: "}
                                <select onChange={this.onChangeLegalSystemId} value={this.state.legalSystemId}>
                                    {this.state.legalSystemIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeLegalSystemPage} value={this.state.currentLegalSystemPage}>
                                    {this.state.legalSystemIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddLegalSystem}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"ClimatId: "}
                                <select onChange={this.onChangeClimatId} value={this.state.climatId}>
                                    {this.state.climatIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeClimatPage} value={this.state.currentClimatPage}>
                                    {this.state.climatIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddClimat}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"SeasonId: "}
                                <select onChange={this.onChangeSeasonId} value={this.state.seasonId}>
                                    {this.state.seasonIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeSeasonPage} value={this.state.currentSeasonPage}>
                                    {this.state.seasonIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddSeason}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"LandscapeId: "}
                                <select onChange={this.onChangeLandscapeId} value={this.state.landscapeId}>
                                    {this.state.landscapeIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeLandscapePage} value={this.state.currentLandscapePage}>
                                    {this.state.landscapeIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddLandscape}
                                >
                                    Add
            </button>
                            </div>



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddProvince };