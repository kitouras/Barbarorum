import React, { Component } from "react";
import { provinceReligionsService, provinceService, religionService } from '@/_services';

export default class AddProvinceReligions extends Component {
    constructor(props) {
        super(props);
        this.onChangePercentage = this.onChangePercentage.bind(this);
        this.onChangeProvinceId = this.onChangeProvinceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getProvince = this.getProvince.bind(this);
        this.onChangeProvincePage = this.onChangeProvincePage.bind(this);
        this.OnClickAddProvince = this.OnClickAddProvince.bind(this);
        this.onChangeReligionId = this.onChangeReligionId.bind(this);
        this.getReligion = this.getReligion.bind(this);
        this.onChangeReligionPage = this.onChangeReligionPage.bind(this);
        this.OnClickAddReligion = this.OnClickAddReligion.bind(this);

        this.state = {
            percentage: 0,

            provinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentProvincePage: 0,

            religionIds: [],
            religionIdsTotalPages: 1,
            religionIdsPages: [],
            currentReligionPage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getProvince(this.state.currentProvincePage);
        this.getReligion(this.state.currentReligionPage);
        if (localStorage.getItem("current_provinceReligions_state") != null) {
            console.log(localStorage.getItem("current_provinceReligions_state"));
            const new_state = JSON.parse(localStorage.getItem("current_provinceReligions_state"));
            this.setState({
                percentage: new_state.percentage,
                provinceId: new_state.provinceId,
                religionId: new_state.religionId,
                provinceIds: new_state.provinceIds,
                provinceIdsTotalPages: new_state.provinceIdsTotalPages,
                provinceIdsPages: new_state.provinceIdsPages,
                currentProvincePage: new_state.currentProvincePage,

                religionIds: new_state.religionIds,
                religionIdsTotalPages: new_state.religionIdsTotalPages,
                religionIdsPages: new_state.religionIdsPages,
                currentReligionPage: new_state.currentProvincePage,
                message: new_state.message
            });
            localStorage.removeItem('current_provinceReligions_state');
        }
        
    }

    onChangePercentage(e) {
        this.setState({
            percentage: e.target.value
        });
    }
    onChangeProvinceId(e) {
        this.setState({
            provinceId: e.target.value
        });
    }
    onChangeProvincePage(e) {
        const page = e.target.value;
        this.getProvince(page);
        this.setState({
            currentProvincePage: page
        });
    }

    onChangeReligionId(e) {
        this.setState({
            religionId: e.target.value
        });
    }
    onChangeReligionPage(e) {
        const page = e.target.value;
        this.getReligion(page);
        this.setState({
            currentReligionPage: page
        });
    }

    getProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    provinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    provinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getReligion(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        religionService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    religionIds: content,
                    religionIdsTotalPages: totalPages,
                    religionIdsPages: pages,
                    religionId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {
            percentage: this.state.percentage,
            provinceId: this.state.provinceId,
            religionId: this.state.religionId
        };

        provinceReligionsService.createEntity(data)
            .then(response => {
                this.setState({
                    percentage: response.percentage,
                    provinceId: response.provinceId,
                    religionId: response.religionId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            percentage: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddProvince() {
        localStorage.setItem('current_provinceReligions_state', JSON.stringify(this.state));
        this.props.history.push('/add/province');
    }
    OnClickAddReligion() {
        localStorage.setItem('current_provinceReligions_state', JSON.stringify(this.state));
        this.props.history.push('/add/religion');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"ProvinceId: "}
                                <select onChange={this.onChangeProvinceId} value={this.state.provinceId}>
                                    {this.state.provinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeProvincePage} value={this.state.currentProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"ReligionId: "}
                                <select onChange={this.onChangeReligionId} value={this.state.religionId}>
                                    {this.state.religionIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeReligionPage} value={this.state.currentReligionPage}>
                                    {this.state.religionIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddReligion}
                                >
                                    Add
            </button>
                            </div>
                            <div className="form-group">
                                <label htmlFor="percentage">percentage</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="percentage"
                                    required
                                    value={this.state.percentage}
                                    onChange={this.onChangePercentage}
                                    name="percentage"
                                />
                            </div>






                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddProvinceReligions };