import React, { Component } from "react";
import { relationService } from '@/_services';

export default class AddRelation extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeFractionsConnectivity = this.onChangeFractionsConnectivity.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            id: null,
            name: "",
            fractionsConnectivity: 0,

            submitted: false
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeFractionsConnectivity(e) {
        this.setState({
            fractionsConnectivity: e.target.value
        });
    }

    saveEntity() {
        var data = {
            name: this.state.name,
            fractionsConnectivity: this.state.fractionsConnectivity
        };

        relationService.createEntity(data)
            .then(response => {
                this.setState({
                    id: response.id,
                    name: response.name,
                    fractionsConnectivity: response.fractionsConnectivity,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            fractionsConnectivity: 0,

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            Add
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>


                            <div className="form-group">
                                <label htmlFor="fractionsConnectivity">FractionsConnectivity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="fractionsConnectivity"
                                    required
                                    value={this.state.fractionsConnectivity}
                                    onChange={this.onChangeFractionsConnectivity}
                                    name="fractionsConnectivity"
                                />
                            </div>

                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddRelation };