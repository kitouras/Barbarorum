import React, { Component } from "react";
import { fractionService } from '@/_services';

export default class AddFraction extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeRebellionTendency = this.onChangeRebellionTendency.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            id: null,
            name: "",
            rebellionTendency: 0,

            submitted: false
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeRebellionTendency(e) {
        this.setState({
            rebellionTendency: e.target.value
        });
    }

    saveEntity() {
        var data = {
            name: this.state.name,
            rebellionTendency: this.state.rebellionTendency
        };

        fractionService.createEntity(data)
            .then(response => {
                this.setState({
                    id: response.id,
                    name: response.name,
                    rebellionTendency: response.rebellionTendency,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            rebellionTendency: 0,

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            Add
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>


                            <div className="form-group">
                                <label htmlFor="rebellionTendency">RebellionTendency</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="rebellionTendency"
                                    required
                                    value={this.state.rebellionTendency}
                                    onChange={this.onChangeRebellionTendency}
                                    name="rebellionTendency"
                                />
                            </div>

                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddFraction };