import React, { Component } from "react";
import { stateService } from '@/_services';

export default class AddState extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeColor = this.onChangeColor.bind(this);
        this.onChangePopularity = this.onChangePopularity.bind(this);
        this.onChangeTreasury = this.onChangeTreasury.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            id: null,
            name: "",
            color: "",
            popularity: 0,
            treasury: 0,

            submitted: false
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeColor(e) {
        this.setState({
            color: e.target.value
        });
    }

    onChangePopularity(e) {
        this.setState({
            popularity: e.target.value
        });
    }
    onChangeTreasury(e) {
        this.setState({
            treasury: e.target.value
        });
    }

    saveEntity() {
        var data = {
            name: this.state.name,
            color: this.state.color,
            popularity: this.state.popularity,
            treasury: this.state.treasury
        };

        stateService.createEntity(data)
            .then(response => {
                this.setState({
                    id: response.id,
                    name: response.name,
                    color: response.color,
                    popularity: response.popularity,
                    treasury: response.treasury,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            color: "",
            popularity: 0,
            treasury: 0,

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            Add
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="color">Color</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="color"
                                    required
                                    value={this.state.color}
                                    onChange={this.onChangeColor}
                                    name="color"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="popularity">Popularity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="popularity"
                                    required
                                    value={this.state.popularity}
                                    onChange={this.onChangePopularity}
                                    name="popularity"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="treasury">Treasury</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="treasury"
                                    required
                                    value={this.state.treasury}
                                    onChange={this.onChangeTreasury}
                                    name="treasury"
                                />
                            </div>

                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddState };