import React, { Component } from "react";
import { waterspaceService, climatService, seasonService } from '@/_services';

class Waterspace extends Component {
    constructor(props) {
        super(props);
        this.onChangeDepth = this.onChangeDepth.bind(this);
        this.onChangeWidth = this.onChangeWidth.bind(this);
        this.onChangeClimatId = this.onChangeClimatId.bind(this);
        this.onChangeSeasonId = this.onChangeSeasonId.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getClimatId = this.getClimatId.bind(this);
        this.getSeasonId = this.getSeasonId.bind(this);
        this.onChangeSeasonPage = this.onChangeSeasonPage.bind(this);
        this.OnClickAddClimat = this.OnClickAddClimat.bind(this);
        this.OnClickAddSeason = this.this.OnClickAddSeason.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                depth: 0,
                width: 0,
                climatId: 0,
                seasonId: 0
            },
            climatIds: [],
            climatIdsTotalPages: 1,
            climatIdsPages: [],
            currentClimatPage: 0,

            seasonIds: [],
            seasonIdsTotalPages: 1,
            seasonIdsPages: [],
            currentSeasonPage: 0,

            message: "",
        };
    }

    componentDidMount() {
        this.getClimatId(this.state.currentClimatPage);
        this.getSeasonId(this.state.currentSeasonPage);
        if (localStorage.getItem("current_waterspace_state") != null) {
            console.log(localStorage.getItem("current_waterspace_state"));
            const new_state = JSON.parse(localStorage.getItem("current_waterspace_state"));
            this.setState({
                currentEntity: new_state.currentEntity,
                climatIds: new_state.climatIds,
                climatIdsTotalPages: new_state.climatIdsTotalPages,
                climatIdsPages: new_state.climatIdsPages,
                currentClimatPage: new_state.currentClimatPage,

                seasonIds: new_state.seasonIds,
                seasonIdsTotalPages: new_state.seasonIdsTotalPages,
                seasonIdsPages: new_state.seasonIdsPages,
                currentSeasonPage: new_state.currentSeasonPage,
                message: new_state.message
            });
            localStorage.removeItem('current_waterspace_state');
        } else this.getEntity(this.props.match.params.id);
        
    }


    onChangeDepth(e) {
        const depth = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    depth: depth,
                },
            };
        });
    }
    onChangeWidth(e) {
        const width = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    width: width,
                },
            };
        });
    }

    onChangeClimatId(e) {
        const climatId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    climatId: climatId,
                },
            };
        });
    }
    onChangeClimatPage(e) {
        const page = e.target.value;
        this.getClimatId(page);
        this.setState(function (prevState) {
            return {
                currentClimatPage: page,
            };
        });
    }

    onChangeSeasonId(e) {
        const seasonId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    seasonId: seasonId,
                },
            };
        });
    }
    onChangeSeasonPage(e) {
        const page = e.target.value;
        this.getSeasonId(page);
        this.setState(function (prevState) {
            return {
                currentSeasonPage: page,
            };
        });
    }


    getEntity(id) {
        waterspaceService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const climatId = this.state.currentEntity.climatId.id;
                    const seasonId = this.state.currentEntity.seasonId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            climatId: climatId,
                            seasonId: seasonId

                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getSeasonId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        seasonService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    seasonIds: content,
                    seasonIdsTotalPages: totalPages,
                    seasonIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getClimatId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        climatService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    climatIds: content,
                    climatIdsTotalPages: totalPages,
                    climatIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        waterspaceService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The waterspace was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        waterspaceService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/waterspace");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddClimat() {
        localStorage.setItem('current_waterspace_state', JSON.stringify(this.state));
        this.props.history.push('/add/climat');
    }

    OnClickAddSeason() {
        localStorage.setItem('current_waterspace_state', JSON.stringify(this.state));
        this.props.history.push('/add/season');
    }

    render() {
        const { currentEntity, climatIds, climatIdsPages, currentClimatPage,
            seasonIds, seasonIdsPages, currentSeasonPage        } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Waterspace</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="depth">depth</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="depth"
                                    value={currentEntity.depth}
                                    onChange={this.onChangeDepth}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="width">width</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="width"
                                    value={currentEntity.width}
                                    onChange={this.onChangeWidth}
                                />
                            </div>

                            <div className="mt-3 mb-1">
                                {"ClimatId: "}
                                <select onChange={this.onChangeClimatId} value={currentEntity.climatId}>
                                    {climatIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeClimatPage} value={currentClimatPage}>
                                    {climatIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddClimat}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"SeasonId: "}
                                <select onChange={this.onChangeSeasonId} value={currentEntity.seasonId}>
                                    {seasonIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeSeasonPage} value={currentSeasonPage}>
                                    {seasonIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddSeason}
                                >
                                    Add
            </button>
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Waterspace...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Waterspace };
