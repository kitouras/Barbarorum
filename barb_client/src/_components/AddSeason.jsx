import React, { Component } from "react";
import { seasonService, incomeAffectableModeService } from '@/_services';

export default class AddSeason extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeTemperatureMod = this.onChangeTemperatureMod.bind(this);
        this.onChangeWindMod = this.onChangeWindMod.bind(this);
        this.onChangeHumidityMod = this.onChangeHumidityMod.bind(this);
        this.onChangeAtmosphericPressureMod = this.onChangeAtmosphericPressureMod.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddIncomeMode = this.OnClickAddIncomeMode.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            id: null,
            name: "",
            temperatureMod: 0,
            windMod: 0,
            humidityMod: 0,
            atmosphericPressureMod: 0,
            incomeModsId: 0,

            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,
            temperatureModValidity: true,
            windModValidity: true,
            humidityModValidity: true,
            atmosphericPressureModValidity: true,

            submitted: false
        };
    }

    checkValidity() {
        return this.state.temperatureModValidity && this.state.windModValidity
            && this.state.humidityModValidity && this.state.atmosphericPressureModValidity;
    }
    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_state") != null) {
            console.log(localStorage.getItem("current_state"));
            const new_state = JSON.parse(localStorage.getItem("current_state"));
            this.setState({
                name: new_state.name,
                temperatureMod: new_state.temperatureMod,
                windMod: new_state.windMod,
                humidityMod: new_state.humidityMod,
                atmosphericPressureMod: new_state.atmosphericPressureMod,
                incomeModsId: new_state.incomeModsId,
                incomeModsIds: new_state.incomeModsIds,
                incomeModsIdsTotalPages: new_state.incomeModsIdsTotalPages,
                incomeModsIdsPages: new_state.incomeModsIdsPages,
                currentPage: new_state.currentPage,
                temperatureModValidity: new_state.temperatureModValidity,
                windModValidity: new_state.windModValidity,
                humidityModValidity: new_state.humidityModValidity,
                atmosphericPressureModValidity: new_state.atmosphericPressureModValidity,
                message: new_state.message
            });
            localStorage.removeItem('current_state');
        }
        
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeTemperatureMod(e) {
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.temperatureModValidity = true;
        else this.state.temperatureModValidity = false;
        this.setState({
            temperatureMod: e.target.value
        });
    }
    onChangeWindMod(e) {
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.windModValidity = true;
        else this.state.windModValidity = false;
        this.setState({
            windMod: e.target.value
        });
    }
    onChangeHumidityMod(e) {
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.humidityModValidity = true;
        else this.state.humidityModValidity = false;
        this.setState({
            humidityMod: e.target.value
        });
    }
    onChangeAtmosphericPressureMod(e) {
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.atmosphericPressureModValidity = true;
        else this.state.atmosphericPressureModValidity = false;
        this.setState({
            atmosphericPressureMod: e.target.value
        });
    }
    onChangeIncomeModsId(e) {
        this.setState({
            incomeModsId: e.target.value
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState({
            currentPage: page
        });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages,
                    incomeModsId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        if (this.checkValidity()) {
            var data = {
                name: this.state.name,
                temperatureMod: this.state.temperatureMod,
                windMod: this.state.windMod,
                humidityMod: this.state.humidityMod,
                atmosphericPressureMod: this.state.atmosphericPressureMod,
                incomeModsId: this.state.incomeModsId
            };

            seasonService.createEntity(data)
                .then(response => {
                    this.setState({
                        name: response.name,
                        temperatureMod: response.temperatureMod,
                        windMod: response.windMod,
                        humidityMod: response.humidityMod,
                        atmosphericPressureMod: response.atmosphericPressureMod,
                        incomeModsId: response.incomeModsId,

                        submitted: true
                    });
                    console.log(response);
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            temperatureMod: 0,
            windMod: 0,
            humidityMod: 0,
            atmosphericPressureMod: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddIncomeMode() {
        localStorage.setItem('current_state', JSON.stringify(this.state));
        this.props.history.push('/add/incomeAffectableMode');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="temperatureMod">temperatureMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="temperatureMod"
                                    required
                                    value={this.state.temperatureMod}
                                    onChange={this.onChangeTemperatureMod}
                                    name="temperatureMod"
                                />
                            </div>
                            {!this.state.temperatureModValidity ? (
                                <p><font size="2" color="red">temperatureMod should be float.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="windMod">windMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="windMod"
                                    required
                                    value={this.state.windMod}
                                    onChange={this.onChangeWindMod}
                                    name="windMod"
                                />
                            </div>
                            {!this.state.windModValidity ? (
                                <p><font size="2" color="red">windMod should be float.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="humidityMod">humidityMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="humidityMod"
                                    required
                                    value={this.state.humidityMod}
                                    onChange={this.onChangeHumidityMod}
                                    name="humidityMod"
                                />
                            </div>
                            {!this.state.humidityModValidity ? (
                                <p><font size="2" color="red">humidityMod should be float.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="atmosphericPressureMod">atmosphericPressureMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="atmosphericPressureMod"
                                    required
                                    value={this.state.atmosphericPressureMod}
                                    onChange={this.onChangeAtmosphericPressureMod}
                                    name="atmosphericPressureMod"
                                />
                            </div>
                            {!this.state.atmosphericPressureModValidity ? (
                                <p><font size="2" color="red">atmosphericPressureMod should be float.</font></p>)
                                : (<div />)}
                            <div className="mt-3 mb-1">
                                {"IncomeModsId: "}
                                <select onChange={this.onChangeIncomeModsId} value={this.state.incomeModsId}>
                                    {this.state.incomeModsIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={this.state.currentPage}>
                                    {this.state.incomeModsIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddIncomeMode}
                                >
                                    Add
            </button>
                            </div>



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddSeason };