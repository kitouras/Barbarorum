import React, { Component } from "react";
import { Link } from "react-router-dom";
import { seasonService, incomeAffectableModeService } from '@/_services';
import Pagination from "@material-ui/lab/Pagination";

class SeasonPage extends Component {
    constructor(props) {
        super(props);
        this.retrieveEntities = this.retrieveEntities.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveEntities = this.setActiveEntities.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handlePageSizeChange = this.handlePageSizeChange.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeTemperatureMod = this.onChangeTemperatureMod.bind(this);
        this.onChangeWindMod = this.onChangeWindMod.bind(this);
        this.onChangeHumidityMod = this.onChangeHumidityMod.bind(this);
        this.onChangeAtmosphericPressureMod = this.onChangeAtmosphericPressureMod.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.onChangeSortBy = this.onChangeSortBy.bind(this);
        this.onChangeSortDir = this.onChangeSortDir.bind(this);

        this.state = {
            entities: [],
            currentEntities: null,
            currentIndex: -1,
            searchName: "",

            id: null,
            name: null,
            temperatureMod: null,
            windMod: null,
            humidityMod: null,
            atmosphericPressureMod: null,
            incomeModsId: null,

            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,

            sortBy: 'id',
            sortBySelectable: ['id', 'name', 'temperatureMod', 'windMod', 'humidityMod', 'atmosphericPressureMod',
                'incomeModsId'],
            sortDir: 'ASC',
            sortDirSelectable: ['ASC', 'DESC'],

            page: 1,
            count: 0,
            pageSize: 6,
        };

        this.pageSizes = [3, 6, 9];
    }

    componentDidMount() {
        this.retrieveEntities();
        this.getIncomeMods();
    }

    onChangeSortBy(e) {
        this.setState({
            sortBy: e.target.value
        });
    }
    onChangeSortDir(e) {
        this.setState({
            sortDir: e.target.value
        });
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeTemperatureMod(e) {
        
        this.setState({
            temperatureMod: e.target.value
        });
    }
    onChangeWindMod(e) {
        
        this.setState({
            windMod: e.target.value
        });
    }
    onChangeHumidityMod(e) {
        
        this.setState({
            humidityMod: e.target.value
        });
    }
    onChangeAtmosphericPressureMod(e) {
        
        this.setState({
            atmosphericPressureMod: e.target.value
        });
    }
    onChangeIncomeModsId(e) {
        this.setState({
            incomeModsId: e.target.value
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState({
            currentPage: page
        });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    refreshList() {
        this.retrieveEntities();
        this.setState({
            currentEntities: null,
            currentIndex: -1
        });
    }

    setActiveEntities(entities, index) {
        this.setState({
            currentEntities: entities,
            currentIndex: index
        });
    }

    getRequestParams(searchName, page, pageSize, sortBy, sortDir, id, name, temperatureMod, windMod, humidityMod,
        atmosphericPressureMod, incomeModsId) {
        let params = {};

        if (searchName) {
            params["name"] = searchName;
        }

        if (page) {
            params["page"] = page - 1;
        }

        if (pageSize) {
            params["size"] = pageSize;
        }
        params["sortBy"] = sortBy;
        params["sortDir"] = sortDir;
        params["id"] = id;
        params["name"] = name;
        params["temperatureMod"] = temperatureMod;
        params["windMod"] = windMod;
        params["humidityMod"] = humidityMod;
        params["atmosphericPressureMod"] = atmosphericPressureMod;
        params["incomeModsId"] = incomeModsId;

        return params;
    }

    retrieveEntities() {
        const { searchName, page, pageSize, sortBy, sortDir, id, name, temperatureMod, windMod, humidityMod,
            atmosphericPressureMod, incomeModsId } = this.state;
        const params = this.getRequestParams(searchName, page, pageSize, sortBy, sortDir, id, name, temperatureMod, windMod,
            humidityMod, atmosphericPressureMod, incomeModsId);


        seasonService.getEntities(params)
            .then((response) => {

                const content = response.content;
                const totalPages = response.totalPages;

                this.setState({
                    entities: content,
                    count: totalPages,
                });
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    handlePageChange(event, value) {
        this.setState(
            {
                page: value,
            },
            () => {
                this.retrieveEntities();
            }
        );
    }

    handlePageSizeChange(event) {
        this.setState(
            {
                pageSize: event.target.value,
                page: 1
            },
            () => {
                this.retrieveEntities();
            }
        );
    }


    render() {
        const {
            searchName,
            entities,
            currentEntities,
            currentIndex,
            page,
            count,
            pageSize,
        } = this.state;

        return (
            <div className="list row">
                
                <div className="col-md-6">
                    <h4>Season List:</h4>

                    <div className="mt-3">
                        {"Items per Page: "}
                        <select onChange={this.handlePageSizeChange} value={pageSize}>
                            {this.pageSizes.map((size) => (
                                <option key={size} value={size}>
                                    {size}
                                </option>
                            ))}
                        </select>

                        <Pagination
                            className="my-3"
                            count={count}
                            page={page}
                            siblingCount={1}
                            boundaryCount={1}
                            variant="outlined"
                            shape="rounded"
                            onChange={this.handlePageChange}
                        />
                    </div>

                    <ul className="list-group">
                        {entities &&
                            entities.map((entity, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActiveEntities(entity, index)}
                                    key={index}
                                >
                                    {entity.id}
                                </li>
                            ))}
                    </ul>

                    <Link
                        to={"/add/season"}
                        className="m-2 btn btn-light"
                    >
                        Add
              </Link>

                    <button
                        className="m-4 btn btn-sm btn-danger"
                        onClick={this.retrieveEntities}
                    >
                        Filter
          </button>
                    <div className="ml-3">
                        <div className="mt-3 mb-1">
                            {"SortBy: "}
                            <select onChange={this.onChangeSortBy} value={this.state.sortBy}>
                                {this.state.sortBySelectable.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="mt-3 mb-1">
                            {" SortDir: "}
                            <select onChange={this.onChangeSortDir} value={this.state.sortDir}>
                                {this.state.sortDirSelectable.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">name</label>
                            <input
                                type="text"
                                className="form-control"
                                id="name"
                                required
                                value={this.state.name}
                                onChange={this.onChangeName}
                                name="name"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="temperatureMod">temperatureMod</label>
                            <input
                                type="text"
                                className="form-control"
                                id="temperatureMod"
                                required
                                value={this.state.temperatureMod}
                                onChange={this.onChangeTemperatureMod}
                                name="temperatureMod"
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="windMod">windMod</label>
                            <input
                                type="text"
                                className="form-control"
                                id="windMod"
                                required
                                value={this.state.windMod}
                                onChange={this.onChangeWindMod}
                                name="windMod"
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="humidityMod">humidityMod</label>
                            <input
                                type="text"
                                className="form-control"
                                id="humidityMod"
                                required
                                value={this.state.humidityMod}
                                onChange={this.onChangeHumidityMod}
                                name="humidityMod"
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="atmosphericPressureMod">atmosphericPressureMod</label>
                            <input
                                type="text"
                                className="form-control"
                                id="atmosphericPressureMod"
                                required
                                value={this.state.atmosphericPressureMod}
                                onChange={this.onChangeAtmosphericPressureMod}
                                name="atmosphericPressureMod"
                            />
                        </div>

                        <div className="mt-3 mb-1">
                            {"IncomeModsId: "}
                            <select onChange={this.onChangeIncomeModsId} value={this.state.incomeModsId}>
                                {this.state.incomeModsIds.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                            {" Page: "}
                            <select onChange={this.onChangePage} value={this.state.currentPage}>
                                {this.state.incomeModsIdsPages.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>


                        </div>
                    </div>

                </div>

                <div className="col-md-6">
                    {currentEntities ? (
                        <div>
                            <h4>Season</h4>
                            <div>
                                <label>
                                    <strong>name:</strong>
                                </label>{" "}
                                {currentEntities.name}
                            </div>
                            <div>
                                <label>
                                    <strong>temperatureMod:</strong>
                                </label>{" "}
                                {currentEntities.temperatureMod}
                            </div>
                            <div>
                                <label>
                                    <strong>windMod:</strong>
                                </label>{" "}
                                {currentEntities.windMod}
                            </div>
                            <div>
                                <label>
                                    <strong>humidityMod:</strong>
                                </label>{" "}
                                {currentEntities.humidityMod}
                            </div>
                            <div>
                                <label>
                                    <strong>atmosphericPressureMod:</strong>
                                </label>{" "}
                                {currentEntities.atmosphericPressureMod}
                            </div>
                            <div>
                                <label>
                                    <strong>incomeModsId:</strong>
                                </label>{" "}
                                {currentEntities.incomeModsId.id}
                            </div>
                            <Link
                                to={"/edit/season/" + currentEntities.id}
                                className="badge badge-warning"
                            >
                                Edit
              </Link>
                        </div>
                    ) : (
                            <div>
                                <br />
                                <p>Please click on a Season...</p>
                            </div>
                        )}

                </div>
            </div>
        );
    }
}

export { SeasonPage };