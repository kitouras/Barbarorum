import React, { Component } from "react";
import { provinceResourcesLifecycleService} from '@/_services';

class ProvinceResourcesLifecycle extends Component {
    constructor(props) {
        super(props);
        this.onChangeResourceStock = this.onChangeResourceStock.bind(this);
        this.onChangeResourceProduction = this.onChangeResourceProduction.bind(this);
        this.onChangeResourceLimit = this.onChangeResourceLimit.bind(this);
        this.onChangeResourceMarketing = this.onChangeResourceMarketing.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            currentEntity: {
                provinceId: null,
                resourceId: null,
                resourceStock: 0,
                resourceProduction: 0,
                resourceLimit: 0,
                resourceMarketing: 0
            },
            resourceStockValidity: true,
            resourceProductionValidity: true,
            resourceLimitValidity: true,
            resourceMarketingValidity: true,

            message: "",
        };
        
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.provinceId, this.props.match.params.resourceId);
    }


    onChangeResourceStock(e) {
        const resourceStock = e.target.value;
        if (parseInt(resourceStock) >= 0) this.state.resourceStockValidity = true;
        else this.state.resourceStockValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    resourceStock: resourceStock,
                },
            };
        });
    }
    onChangeResourceProduction(e) {
        const resourceProduction = e.target.value;
        if (parseInt(resourceProduction) >= 0) this.state.resourceProductionValidity = true;
        else this.state.resourceProductionValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    resourceProduction: resourceProduction,
                },
            };
        });
    }
    onChangeResourceLimit(e) {
        const resourceLimit = e.target.value;
        if (parseInt(resourceLimit) >= 0) this.state.resourceLimitValidity = true;
        else this.state.resourceLimitValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    resourceLimit: resourceLimit,
                },
            };
        });
    }
    onChangeResourceMarketing(e) {
        const resourceMarketing = e.target.value;
        if (parseInt(resourceMarketing) >= 0) this.state.resourceMarketingValidity = true;
        else this.state.resourceMarketingValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    resourceMarketing: resourceMarketing,
                },
            };
        });
    }

    checkValidity() {
        return this.state.resourceLimitValidity && this.state.resourceMarketingValidity
            && this.state.resourceProductionValidity && this.state.resourceStockValidity;
    }

    getEntity(provinceId, resourceId) {
        provinceResourcesLifecycleService.getEntity(provinceId, resourceId)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const provinceId = this.state.currentEntity.provinceId.id;
                    const resourceId = this.state.currentEntity.resourceId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            provinceId: provinceId,
                            resourceId: resourceId
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    updateEntity() {
        if (this.checkValidity())
            provinceResourcesLifecycleService
                .updateEntity(this.state.currentEntity.provinceId, this.state.currentEntity.resourceId, this.state.currentEntity)
                .then((reponse) => {
                    console.log(reponse);

                    this.setState({ message: "The provinceResourcesLifecycle was updated successfully!" });
                })
                .catch((e) => {
                    console.log(e);
                });
    }

    removeEntity() {
        provinceResourcesLifecycleService
            .deleteEntity(this.state.currentEntity.provinceId, this.state.currentEntity.resourceId)
            .then(() => {
                this.props.history.push("/provinceResourcesLifecycle");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>ProvinceResourcesLifecycle</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="resourceStock">resourceStock</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceStock"
                                    value={currentEntity.resourceStock}
                                    onChange={this.onChangeResourceStock}
                                />
                            </div>
                            {!this.state.resourceStockValidity ? (
                                <p><font size="2" color="red">resourceStock should be non-negative integer</font></p>) : (<div />)}
                            <div className="form-group">
                                <label htmlFor="resourceProduction">resourceProduction</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceProduction"
                                    value={currentEntity.resourceProduction}
                                    onChange={this.onChangeResourceProduction}
                                />
                            </div>
                            {!this.state.resourceProductionValidity ? (
                                <p><font size="2" color="red">resourceProduction should be non-negative integer</font></p>) : (<div />)}
                            <div className="form-group">
                                <label htmlFor="resourceLimit">resourceLimit</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceLimit"
                                    value={currentEntity.resourceLimit}
                                    onChange={this.onChangeResourceLimit}
                                />
                            </div>
                            {!this.state.resourceLimitValidity ? (
                                <p><font size="2" color="red">resourceLimit should be non-negative integer</font></p>) : (<div />)}
                            <div className="form-group">
                                <label htmlFor="resourceMarketing">resourceMarketing</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceMarketing"
                                    value={currentEntity.resourceMarketing}
                                    onChange={this.onChangeResourceMarketing}
                                />
                            </div>
                            {!this.state.resourceMarketingValidity ? (
                                <p><font size="2" color="red">resourceMarketing should be non-negative integer</font></p>) : (<div />)}
                           

                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a ProvinceResourcesLifecycle...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { ProvinceResourcesLifecycle };
