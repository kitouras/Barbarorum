import React, { Component } from "react";
import { neighborhoodOfWaterspacesService, waterspaceService} from '@/_services';

export default class AddNeighborhoodOfWaterspaces extends Component {
    constructor(props) {
        super(props);
        this.onChangeFirstWaterspaceId = this.onChangeFirstWaterspaceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getFirstWaterspace = this.getFirstWaterspace.bind(this);
        this.onChangeFirstWaterspacePage = this.onChangeFirstWaterspacePage.bind(this);
        this.OnClickAddWaterspace = this.OnClickAddWaterspace.bind(this);
        this.onChangeSecondWaterspaceId = this.onChangeSecondWaterspaceId.bind(this);
        this.getSecondWaterspace = this.getSecondWaterspace.bind(this);
        this.onChangeSecondWaterspacePage = this.onChangeSecondWaterspacePage.bind(this);

        this.state = {
            
            firstWaterspaceId: 0,
            secondWaterspaceId: 0,
            currentFirstWaterspacePage: 0,
            firstWaterspaceIds: [],
            secondWaterspaceIds: [],
            waterspaceIdsTotalPages: 1,
            waterspaceIdsPages: [],
            currentSecondWaterspacePage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getFirstWaterspace(this.state.currentWaterspacePage);
        this.getSecondWaterspace(this.state.currentWaterspacePage);
        if (localStorage.getItem("current_neighborhoodOfWaterspaces_state") != null) {
            console.log(localStorage.getItem("current_neighborhoodOfWaterspaces_state"));
            const new_state = JSON.parse(localStorage.getItem("current_neighborhoodOfWaterspaces_state"));
            this.setState({
                
                firstWaterspaceId: new_state.firstWaterspaceId,
                secondWaterspaceId: new_state.secondWaterspaceId,
                currentFirstWaterspacePage: new_state.currentWaterspacePage,

                firstWaterspaceIds: new_state.firstWaterspaceIds,
                secondWaterspaceIds: new_state.secondWaterspaceIds,
                waterspaceIdsTotalPages: new_state.waterspaceIdsTotalPages,
                waterspaceIdsPages: new_state.waterspaceIdsPages,
                currentWaterspacePage: new_state.currentWaterspacePage,
                message: new_state.message
            });
            localStorage.removeItem('current_neighborhoodOfWaterspaces_state');
        }
        
    }

    
    onChangeFirstWaterspaceId(e) {
        this.setState({
            firstWaterspaceId: e.target.value
        });
    }
    onChangeFirstWaterspacePage(e) {
        const page = e.target.value;
        this.getFirstWaterspace(page);
        this.setState({
            currentFirstWaterspacePage: page
        });
    }

    onChangeSecondWaterspaceId(e) {
        this.setState({
            secondWaterspaceId: e.target.value
        });
    }
    onChangeSecondWaterspacePage(e) {
        const page = e.target.value;
        this.getSecondWaterspace(page);
        this.setState({
            currentSecondWaterspacePage: page
        });
    }

    getFirstWaterspace(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        waterspaceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    firstWaterspaceIds: content,
                    waterspaceIdsTotalPages: totalPages,
                    waterspaceIdsPages: pages,
                    firstWaterspaceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getSecondWaterspace(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        waterspaceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    secondWaterspaceIds: content,
                    waterspaceIdsTotalPages: totalPages,
                    waterspaceIdsPages: pages,
                    secondWaterspaceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {
            
            firstWaterspaceId: this.state.firstWaterspaceId,
            secondWaterspaceId: this.state.secondWaterspaceId
        };

        neighborhoodOfWaterspacesService.createEntity(data)
            .then(response => {
                this.setState({
                    
                    firstWaterspaceId: response.firstWaterspaceId,
                    secondWaterspaceId: response.secondWaterspaceId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddWaterspace() {
        localStorage.setItem('current_neighborhoodOfWaterspaces_state', JSON.stringify(this.state));
        this.props.history.push('/add/waterspace');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"FirstWaterspaceId: "}
                                <select onChange={this.onChangeFirstWaterspaceId} value={this.state.firstWaterspaceId}>
                                    {this.state.firstWaterspaceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeFirstWaterspacePage} value={this.state.currentFirstWaterspacePage}>
                                    {this.state.waterspaceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddWaterspace}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"SecondWaterspaceId: "}
                                <select onChange={this.onChangeSecondWaterspaceId} value={this.state.secondWaterspaceId}>
                                    {this.state.secondWaterspaceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeSecondWaterspacePage} value={this.state.currentSecondWaterspacePage}>
                                    {this.state.waterspaceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddWaterspace}
                                >
                                    Add
            </button>
                            </div>
                            





                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddNeighborhoodOfWaterspaces };