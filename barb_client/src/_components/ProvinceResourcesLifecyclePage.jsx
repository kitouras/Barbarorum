import React, { Component } from "react";
import { Link } from "react-router-dom";
import { provinceResourcesLifecycleService, provinceService, resourceService } from '@/_services';
import Pagination from "@material-ui/lab/Pagination";

class ProvinceResourcesLifecyclePage extends Component {
    constructor(props) {
        super(props);
        this.retrieveEntities = this.retrieveEntities.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveEntities = this.setActiveEntities.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handlePageSizeChange = this.handlePageSizeChange.bind(this);
        this.onChangeResourceStock = this.onChangeResourceStock.bind(this);
        this.onChangeResourceProduction = this.onChangeResourceProduction.bind(this);
        this.onChangeResourceLimit = this.onChangeResourceLimit.bind(this);
        this.onChangeResourceMarketing = this.onChangeResourceMarketing.bind(this);
        this.onChangeProvinceId = this.onChangeProvinceId.bind(this);
        this.getProvince = this.getProvince.bind(this);
        this.onChangeProvincePage = this.onChangeProvincePage.bind(this);
        this.onChangeResourceId = this.onChangeResourceId.bind(this);
        this.getResource = this.getResource.bind(this);
        this.onChangeResourcePage = this.onChangeResourcePage.bind(this);
        this.onChangeSortBy = this.onChangeSortBy.bind(this);
        this.onChangeSortDir = this.onChangeSortDir.bind(this);


        this.state = {
            entities: [],
            currentEntities: null,
            currentIndex: -1,
            searchName: "",
            
            provinceId: null,
            resourceId: null,
            resourceStock: null,
            resourceProduction: null,
            resourceLimit: null,
            resourceMarketing: null,

            provinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentProvincePage: 0,

            resourceIds: [],
            resourceIdsTotalPages: 1,
            resourceIdsPages: [],
            currentResourcePage: 0,

            sortBy: 'provinceId',
            sortBySelectable: ['provinceId', 'resourceId', 'resourceStock', 'resourceProduction', 'resourceLimit', 'resourceMarketing'],
            sortDir: 'ASC',
            sortDirSelectable: ['ASC', 'DESC'],
            page: 1,
            count: 0,
            pageSize: 6,
        };

        this.pageSizes = [3, 6, 9];
    }

    componentDidMount() {
        this.retrieveEntities();
        this.getProvince();
        this.getResource();
    }

    onChangeSortBy(e) {
        this.setState({
            sortBy: e.target.value
        });
    }
    onChangeSortDir(e) {
        this.setState({
            sortDir: e.target.value
        });
    }

    onChangeResourceStock(e) {
        this.setState({
            resourceStock: e.target.value
        });
    }

    onChangeResourceProduction(e) {
        this.setState({
            resourceProduction: e.target.value
        });
    }

    onChangeResourceLimit(e) {
        this.setState({
            resourceLimit: e.target.value
        });
    }

    onChangeResourceMarketing(e) {
        this.setState({
            resourceMarketing: e.target.value
        });
    }
    onChangeProvinceId(e) {
        this.setState({
            provinceId: e.target.value
        });
    }
    onChangeProvincePage(e) {
        const page = e.target.value;
        this.getProvince(page);
        this.setState({
            currentProvincePage: page
        });
    }

    onChangeResourceId(e) {
        this.setState({
            resourceId: e.target.value
        });
    }
    onChangeResourcePage(e) {
        const page = e.target.value;
        this.getResource(page);
        this.setState({
            currentResourcePage: page
        });
    }

    getProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    provinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getResource(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        resourceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    resourceIds: content,
                    resourceIdsTotalPages: totalPages,
                    resourceIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    refreshList() {
        this.retrieveEntities();
        this.setState({
            currentEntities: null,
            currentIndex: -1
        });
    }

    setActiveEntities(entities, index) {
        this.setState({
            currentEntities: entities,
            currentIndex: index
        });
    }

    getRequestParams(searchName, page, pageSize, sortBy, sortDir, provinceId, resourceId, resourceStock, resourceProduction,
resourceLimit, resourceMarketing) {
        let params = {};

        if (searchName) {
            params["sortBy"] = searchName;
        }

        if (page) {
            params["page"] = page - 1;
        }

        if (pageSize) {
            params["size"] = pageSize;
        }
        params["sortBy"] = sortBy;
        params["sortDir"] = sortDir;
        params["provinceId"] = provinceId;
        params["resourceId"] = resourceId;
        params["resourceStock"] = resourceStock;
        params["resourceProduction"] = resourceProduction;
        params["resourceLimit"] = resourceLimit;
        params["resourceMarketing"] = resourceMarketing;

        return params;
    }

    retrieveEntities() {
        const { searchName, page, pageSize, sortBy, sortDir, provinceId, resourceId, resourceStock, resourceProduction,
            resourceLimit, resourceMarketing } = this.state;
        const params = this.getRequestParams(searchName, page, pageSize, sortBy, sortDir, provinceId, resourceId, resourceStock, resourceProduction,
            resourceLimit, resourceMarketing);

        provinceResourcesLifecycleService.getEntities(params)
            .then((response) => {

                const content = response.content;
                const totalPages = response.totalPages;

                this.setState({
                    entities: content,
                    count: totalPages,
                });
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    handlePageChange(event, value) {
        this.setState(
            {
                page: value,
            },
            () => {
                this.retrieveEntities();
            }
        );
    }

    handlePageSizeChange(event) {
        this.setState(
            {
                pageSize: event.target.value,
                page: 1
            },
            () => {
                this.retrieveEntities();
            }
        );
    }


    render() {
        const {
            entities,
            currentEntities,
            currentIndex,
            page,
            count,
            pageSize,
        } = this.state;

        return (
            <div className="list row">
                
                <div className="col-md-6">
                    <h4>ProvinceResourcesLifecycle List:</h4>

                    <div className="mt-3">
                        {"Items per Page: "}
                        <select onChange={this.handlePageSizeChange} value={pageSize}>
                            {this.pageSizes.map((size) => (
                                <option key={size} value={size}>
                                    {size}
                                </option>
                            ))}
                        </select>

                        <Pagination
                            className="my-3"
                            count={count}
                            page={page}
                            siblingCount={1}
                            boundaryCount={1}
                            variant="outlined"
                            shape="rounded"
                            onChange={this.handlePageChange}
                        />
                    </div>

                    <ul className="list-group">
                        {entities &&
                            entities.map((entity, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActiveEntities(entity, index)}
                                    key={index}
                                >
                                    {JSON.stringify(entity.provinceId.name).concat(':').concat(JSON.stringify(entity.resourceId.name))}
                                </li>
                            ))}
                    </ul>

                    <Link
                        to={"/add/provinceResourcesLifecycle"}
                        className="m-2 btn btn-light"
                    >
                        Add
              </Link>

                    <button
                        className="m-4 btn btn-sm btn-danger"
                        onClick={this.retrieveEntities}
                    >
                        Filter
          </button>
                    <div className="ml-3">
                        <div className="mt-3 mb-1">
                            {"SortBy: "}
                            <select onChange={this.onChangeSortBy} value={this.state.sortBy}>
                                {this.state.sortBySelectable.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="mt-3 mb-1">
                            {" SortDir: "}
                            <select onChange={this.onChangeSortDir} value={this.state.sortDir}>
                                {this.state.sortDirSelectable.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                        </div>

                    
                        <div className="mt-3 mb-1">
                            {"ProvinceId: "}
                            <select onChange={this.onChangeProvinceId} value={this.state.provinceId}>
                                {this.state.provinceIds.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                            {" Page: "}
                            <select onChange={this.onChangeProvincePage} value={this.state.currentProvincePage}>
                                {this.state.provinceIdsPages.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                        </div>


                        <div className="mt-3 mb-1">
                            {"ResourceId: "}
                            <select onChange={this.onChangeResourceId} value={this.state.resourceId}>
                                {this.state.resourceIds.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                            {" Page: "}
                            <select onChange={this.onChangeResourcePage} value={this.state.currentResourcePage}>
                                {this.state.resourceIdsPages.map((entity) => (
                                    <option key={entity} value={entity}>
                                        {entity}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="resourceStock">resourceStock</label>
                            <input
                                type="text"
                                className="form-control"
                                id="resourceStock"
                                required
                                value={this.state.resourceStock}
                                onChange={this.onChangeResourceStock}
                                name="resourceStock"
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="resourceProduction">resourceProduction</label>
                            <input
                                type="text"
                                className="form-control"
                                id="resourceProduction"
                                required
                                value={this.state.resourceProduction}
                                onChange={this.onChangeResourceProduction}
                                name="resourceProduction"
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="resourceLimit">resourceLimit</label>
                            <input
                                type="text"
                                className="form-control"
                                id="resourceLimit"
                                required
                                value={this.state.resourceLimit}
                                onChange={this.onChangeResourceLimit}
                                name="resourceLimit"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="resourceMarketing">resourceMarketing</label>
                            <input
                                type="text"
                                className="form-control"
                                id="resourceMarketing"
                                required
                                value={this.state.resourceMarketing}
                                onChange={this.onChangeResourceMarketing}
                                name="resourceMarketing"
                            />
                        </div>
                    </div>
                </div>
                
                <div className="col-md-6">
                    {currentEntities ? (
                        <div>
                            <h4>ProvinceResourcesLifecycle</h4>
                            <div>
                                <label>
                                    <strong>resourceStock:</strong>
                                </label>{" "}
                                {currentEntities.resourceStock}
                            </div>
                            <div>
                                <label>
                                    <strong>resourceProduction:</strong>
                                </label>{" "}
                                {currentEntities.resourceProduction}
                            </div>
                            <div>
                                <label>
                                    <strong>resourceLimit:</strong>
                                </label>{" "}
                                {currentEntities.resourceLimit}
                            </div>
                            <div>
                                <label>
                                    <strong>resourceMarketing:</strong>
                                </label>{" "}
                                {currentEntities.resourceMarketing}
                            </div>
                          
                            <Link
                                to={"/edit/provinceResourcesLifecycle/" + currentEntities.provinceId.id + '/' + currentEntities.resourceId.id}
                                className="badge badge-warning"
                            >
                                Edit
              </Link>
                        </div>
                    ) : (
                            <div>
                                <br />
                                <p>Please click on a ProvinceResourcesLifecycle...</p>
                            </div>
                        )}
                   

                </div>
               
            </div>
        );
    }
}

export { ProvinceResourcesLifecyclePage };