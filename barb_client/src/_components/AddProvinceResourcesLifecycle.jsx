import React, { Component } from "react";
import { provinceResourcesLifecycleService, provinceService, resourceService } from '@/_services';

export default class AddProvinceResourcesLifecycle extends Component {
    constructor(props) {
        super(props);
        this.onChangeResourceStock = this.onChangeResourceStock.bind(this);
        this.onChangeResourceProduction = this.onChangeResourceProduction.bind(this);
        this.onChangeResourceLimit = this.onChangeResourceLimit.bind(this);
        this.onChangeResourceMarketing = this.onChangeResourceMarketing.bind(this);
        this.onChangeProvinceId = this.onChangeProvinceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getProvince = this.getProvince.bind(this);
        this.onChangeProvincePage = this.onChangeProvincePage.bind(this);
        this.OnClickAddProvince = this.OnClickAddProvince.bind(this);
        this.onChangeResourceId = this.onChangeResourceId.bind(this);
        this.getResource = this.getResource.bind(this);
        this.onChangeResourcePage = this.onChangeResourcePage.bind(this);
        this.OnClickAddResource = this.OnClickAddResource.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            resourceStock: 0,
            resourceProduction: 0,
            resourceLimit: 0,
            resourceMarketing: 0,
            provinceId: 0,
            resourceId: 0,

            provinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentProvincePage: 0,

            resourceIds: [],
            resourceIdsTotalPages: 1,
            resourceIdsPages: [],
            currentResourcePage: 0,

            resourceStockValidity: true,
            resourceProductionValidity: true,
            resourceLimitValidity: true,
            resourceMarketingValidity: true,

            submitted: false
        };
    }

    componentDidMount() {
        this.getProvince(this.state.currentProvincePage);
        this.getResource(this.state.currentResourcePage);
        if (localStorage.getItem("current_provinceResourcesLifecycle_state") != null) {
            console.log(localStorage.getItem("current_provinceResourcesLifecycle_state"));
            const new_state = JSON.parse(localStorage.getItem("current_provinceResourcesLifecycle_state"));
            this.setState({
                resourceStock: new_state.resourceStock,
                resourceProduction: new_state.resourceProduction,
                resourceLimit: new_state.resourceLimit,
                resourceMarketing: new_state.resourceMarketing,
                provinceId: new_state.provinceId,
                resourceId: new_state.resourceId,
                provinceIds: new_state.provinceIds,
                provinceIdsTotalPages: new_state.provinceIdsTotalPages,
                provinceIdsPages: new_state.provinceIdsPages,
                currentProvincePage: new_state.currentProvincePage,

                resourceIds: new_state.resourceIds,
                resourceIdsTotalPages: new_state.resourceIdsTotalPages,
                resourceIdsPages: new_state.resourceIdsPages,
                currentResourcePage: new_state.currentProvincePage,
                message: new_state.message
            });
            localStorage.removeItem('current_provinceResourcesLifecycle_state');
        }
        
    }

    onChangeResourceStock(e) {
        if (parseInt(e.target.value) >= 0) this.state.resourceStockValidity = true;
        else this.state.resourceStockValidity = false;
        this.setState({
            resourceStock: e.target.value
        });
    }

    onChangeResourceProduction(e) {
        if (parseInt(e.target.value) >= 0) this.state.resourceProductionValidity = true;
        else this.state.resourceProductionValidity = false;
        this.setState({
            resourceProduction: e.target.value
        });
    }

    onChangeResourceLimit(e) {
        if (parseInt(e.target.value) >= 0) this.state.resourceLimitValidity = true;
        else this.state.resourceLimitValidity = false;
        this.setState({
            resourceLimit: e.target.value
        });
    }

    onChangeResourceMarketing(e) {
        if (parseInt(e.target.value) >= 0) this.state.resourceMarketingValidity = true;
        else this.state.resourceMarketingValidity = false;
        this.setState({
            resourceMarketing: e.target.value
        });
    }
    onChangeProvinceId(e) {
        this.setState({
            provinceId: e.target.value
        });
    }
    onChangeProvincePage(e) {
        const page = e.target.value;
        this.getProvince(page);
        this.setState({
            currentProvincePage: page
        });
    }

    onChangeResourceId(e) {
        this.setState({
            resourceId: e.target.value
        });
    }
    onChangeResourcePage(e) {
        const page = e.target.value;
        this.getResource(page);
        this.setState({
            currentResourcePage: page
        });
    }

    checkValidity() {
        return this.state.resourceLimitValidity && this.state.resourceMarketingValidity
            && this.state.resourceProductionValidity && this.state.resourceStockValidity;
    }

    getProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    provinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    provinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getResource(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        resourceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    resourceIds: content,
                    resourceIdsTotalPages: totalPages,
                    resourceIdsPages: pages,
                    resourceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        if (this.checkValidity()) {
            var data = {
                resourceStock: this.state.resourceStock,
                resourceProduction: this.state.resourceProduction,
                resourceLimit: this.state.resourceLimit,
                resourceMarketing: this.state.resourceMarketing,
                provinceId: this.state.provinceId,
                resourceId: this.state.resourceId
            };

            provinceResourcesLifecycleService.createEntity(data)
                .then(response => {
                    this.setState({
                        resourceStock: response.resourceStock,
                        resourceProduction: response.resourceProduction,
                        resourceLimit: response.resourceLimit,
                        resourceMarketing: response.resourceMarketing,
                        provinceId: response.provinceId,
                        resourceId: response.resourceId,

                        submitted: true
                    });
                    console.log(response);
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    newEntity() {
        this.setState({
            id: null,
            resourceStock: 0,
            resourceProduction: 0,
            resourceLimit: 0,
            resourceMarketing: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddProvince() {
        localStorage.setItem('current_provinceResourcesLifecycle_state', JSON.stringify(this.state));
        this.props.history.push('/add/province');
    }
    OnClickAddResource() {
        localStorage.setItem('current_provinceResourcesLifecycle_state', JSON.stringify(this.state));
        this.props.history.push('/add/resource');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"ProvinceId: "}
                                <select onChange={this.onChangeProvinceId} value={this.state.provinceId}>
                                    {this.state.provinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeProvincePage} value={this.state.currentProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"ResourceId: "}
                                <select onChange={this.onChangeResourceId} value={this.state.resourceId}>
                                    {this.state.resourceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeResourcePage} value={this.state.currentResourcePage}>
                                    {this.state.resourceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddResource}
                                >
                                    Add
            </button>
                            </div>
                            <div className="form-group">
                                <label htmlFor="resourceStock">resourceStock</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceStock"
                                    required
                                    value={this.state.resourceStock}
                                    onChange={this.onChangeResourceStock}
                                    name="resourceStock"
                                />
                            </div>
                            {!this.state.resourceStockValidity ? (
                                <p><font size="2" color="red">resourceStock should be non-negative integer</font></p>) : (<div />)}
                            <div className="form-group">
                                <label htmlFor="resourceProduction">resourceProduction</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceProduction"
                                    required
                                    value={this.state.resourceProduction}
                                    onChange={this.onChangeResourceProduction}
                                    name="resourceProduction"
                                />
                            </div>
                            {!this.state.resourceProductionValidity ? (
                                <p><font size="2" color="red">resourceProduction should be non-negative integer</font></p>) : (<div />)}
                            <div className="form-group">
                                <label htmlFor="resourceLimit">resourceLimit</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceLimit"
                                    required
                                    value={this.state.resourceLimit}
                                    onChange={this.onChangeResourceLimit}
                                    name="resourceLimit"
                                />
                            </div>
                            {!this.state.resourceLimitValidity ? (
                                <p><font size="2" color="red">resourceLimit should be non-negative integer</font></p>) : (<div />)}
                            <div className="form-group">
                                <label htmlFor="resourceMarketing">resourceMarketing</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="resourceMarketing"
                                    required
                                    value={this.state.resourceMarketing}
                                    onChange={this.onChangeResourceMarketing}
                                    name="resourceMarketing"
                                />
                            </div>
                            {!this.state.resourceMarketingValidity ? (
                                <p><font size="2" color="red">resourceMarketing should be non-negative integer</font></p>) : (<div />)}

                            



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddProvinceResourcesLifecycle };