import React, { Component } from "react";
import { provinceDeseasesService, provinceService, deseaseService } from '@/_services';

export default class AddProvinceDeseases extends Component {
    constructor(props) {
        super(props);
        this.onChangePrevalence = this.onChangePrevalence.bind(this);
        this.onChangeCollectiveImmunity = this.onChangeCollectiveImmunity.bind(this);
        this.onChangeProvinceId = this.onChangeProvinceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getProvince = this.getProvince.bind(this);
        this.onChangeProvincePage = this.onChangeProvincePage.bind(this);
        this.OnClickAddProvince = this.OnClickAddProvince.bind(this);
        this.onChangeDeseaseId = this.onChangeDeseaseId.bind(this);
        this.getDesease = this.getDesease.bind(this);
        this.onChangeDeseasePage = this.onChangeDeseasePage.bind(this);
        this.OnClickAddDesease = this.OnClickAddDesease.bind(this);

        this.state = {
            prevalence: 0,
            collectiveImmunity: 0,
            provinceId: 0,
            deseaseId: 0,

            provinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentProvincePage: 0,

            deseaseIds: [],
            deseaseIdsTotalPages: 1,
            deseaseIdsPages: [],
            currentDeseasePage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getProvince(this.state.currentProvincePage);
        this.getDesease(this.state.currentDeseasePage);
        if (localStorage.getItem("current_provinceDeseases_state") != null) {
            console.log(localStorage.getItem("current_provinceDeseases_state"));
            const new_state = JSON.parse(localStorage.getItem("current_provinceDeseases_state"));
            this.setState({
                prevalence: new_state.prevalence,
                collectiveImmunity: new_state.collectiveImmunity,
                provinceId: new_state.provinceId,
                deseaseId: new_state.deseaseId,
                provinceIds: new_state.provinceIds,
                provinceIdsTotalPages: new_state.provinceIdsTotalPages,
                provinceIdsPages: new_state.provinceIdsPages,
                currentProvincePage: new_state.currentProvincePage,

                deseaseIds: new_state.deseaseIds,
                deseaseIdsTotalPages: new_state.deseaseIdsTotalPages,
                deseaseIdsPages: new_state.deseaseIdsPages,
                currentDeseasePage: new_state.currentProvincePage,
                message: new_state.message
            });
            localStorage.removeItem('current_provinceDeseases_state');
        }
        
    }

    onChangePrevalence(e) {
        this.setState({
            prevalence: e.target.value
        });
    }

    onChangeCollectiveImmunity(e) {
        this.setState({
            collectiveImmunity: e.target.value
        });
    }

    onChangeProvinceId(e) {
        this.setState({
            provinceId: e.target.value
        });
    }
    onChangeProvincePage(e) {
        const page = e.target.value;
        this.getProvince(page);
        this.setState({
            currentProvincePage: page
        });
    }

    onChangeDeseaseId(e) {
        this.setState({
            deseaseId: e.target.value
        });
    }
    onChangeDeseasePage(e) {
        const page = e.target.value;
        this.getDesease(page);
        this.setState({
            currentDeseasePage: page
        });
    }

    getProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    provinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    provinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getDesease(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        deseaseService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    deseaseIds: content,
                    deseaseIdsTotalPages: totalPages,
                    deseaseIdsPages: pages,
                    deseaseId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {
            prevalence: this.state.prevalence,
            collectiveImmunity: this.state.collectiveImmunity,
            provinceId: this.state.provinceId,
            deseaseId: this.state.deseaseId
        };

        provinceDeseasesService.createEntity(data)
            .then(response => {
                this.setState({
                    prevalence: response.prevalence,
                    collectiveImmunity: response.collectiveImmunity,
                    provinceId: response.provinceId,
                    deseaseId: response.deseaseId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            prevalence: 0,
            collectiveImmunity: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddProvince() {
        localStorage.setItem('current_provinceDeseases_state', JSON.stringify(this.state));
        this.props.history.push('/add/province');
    }
    OnClickAddDesease() {
        localStorage.setItem('current_provinceDeseases_state', JSON.stringify(this.state));
        this.props.history.push('/add/desease');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"ProvinceId: "}
                                <select onChange={this.onChangeProvinceId} value={this.state.provinceId}>
                                    {this.state.provinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeProvincePage} value={this.state.currentProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"DeseaseId: "}
                                <select onChange={this.onChangeDeseaseId} value={this.state.deseaseId}>
                                    {this.state.deseaseIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeDeseasePage} value={this.state.currentDeseasePage}>
                                    {this.state.deseaseIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddDesease}
                                >
                                    Add
            </button>
                            </div>
                            <div className="form-group">
                                <label htmlFor="prevalence">prevalence</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="prevalence"
                                    required
                                    value={this.state.prevalence}
                                    onChange={this.onChangePrevalence}
                                    name="prevalence"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="collectiveImmunity">collectiveImmunity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="collectiveImmunity"
                                    required
                                    value={this.state.collectiveImmunity}
                                    onChange={this.onChangeCollectiveImmunity}
                                    name="collectiveImmunity"
                                />
                            </div>






                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddProvinceDeseases };