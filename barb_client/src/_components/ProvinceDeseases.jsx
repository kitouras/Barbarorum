import React, { Component } from "react";
import { provinceDeseasesService } from '@/_services';

class ProvinceDeseases extends Component {
    constructor(props) {
        super(props);
        this.onChangePrevalence = this.onChangePrevalence.bind(this);
        this.onChangeCollectiveImmunity = this.onChangeCollectiveImmunity.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                provinceId: null,
                deseaseId: null,
                prevalence: 0,
                collectiveImmunity: 0
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.provinceId, this.props.match.params.deseaseId);
    }


    onChangePrevalence(e) {
        const prevalence = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    prevalence: prevalence,
                },
            };
        });
    }
    onChangeCollectiveImmunity(e) {
        const collectiveImmunity = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    collectiveImmunity: collectiveImmunity,
                },
            };
        });
    }



    getEntity(provinceId, deseaseId) {
        provinceDeseasesService.getEntity(provinceId, deseaseId)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const provinceId = this.state.currentEntity.provinceId.id;
                    const deseaseId = this.state.currentEntity.deseaseId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            provinceId: provinceId,
                            deseaseId: deseaseId
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    updateEntity() {
        provinceDeseasesService
            .updateEntity(this.state.currentEntity.provinceId, this.state.currentEntity.deseaseId, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The provinceDeseases was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        provinceDeseasesService
            .deleteEntity(this.state.currentEntity.provinceId, this.state.currentEntity.deseaseId)
            .then(() => {
                this.props.history.push("/provinceDeseases");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity, incomeModsIds, incomeModsIdsPages, currentPage } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>ProvinceDeseases</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="prevalence">prevalence</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="prevalence"
                                    value={currentEntity.prevalence}
                                    onChange={this.onChangePrevalence}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="collectiveImmunity">collectiveImmunity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="collectiveImmunity"
                                    value={currentEntity.collectiveImmunity}
                                    onChange={this.onChangeCollectiveImmunity}
                                />
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a ProvinceDeseases...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { ProvinceDeseases };
