import React, { Component } from "react";
import { legalSystemService, incomeAffectableModeService } from '@/_services';

export default class AddLegalSystem extends Component {
    constructor(props) {
        super(props);
        this.onChangeGroupName = this.onChangeGroupName.bind(this);
        this.onChangeIsTaxFixed = this.onChangeIsTaxFixed.bind(this);
        this.onChangeTaxAboveMultiplier = this.onChangeTaxAboveMultiplier.bind(this);
        this.onChangeAutonomyFactor = this.onChangeAutonomyFactor.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddIncomeMode = this.OnClickAddIncomeMode.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            id: null,
            groupName: "",
            taxFixed: true,
            taxAboveMultiplier: 0,
            autonomyFactor: 0,
            incomeModsId: 0,

            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,

            taxFixedSelectable: ['true', 'false'],

            taxAboveMultiplierValidity: true,
            autonomyFactorValidity: true,

            submitted: false
        };
    }
    checkValidity() {
        return this.state.taxAboveMultiplierValidity && this.state.autonomyFactorValidity;
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_state") != null) {
            console.log(localStorage.getItem("current_state"));
            const new_state = JSON.parse(localStorage.getItem("current_state"));
            this.setState({
                groupName: new_state.groupName,
                taxFixed: new_state.taxFixed,
                taxAboveMultiplier: new_state.taxAboveMultiplier,
                autonomyFactor: new_state.autonomyFactor,
                incomeModsId: new_state.incomeModsId,
                incomeModsIds: new_state.incomeModsIds,
                incomeModsIdsTotalPages: new_state.incomeModsIdsTotalPages,
                incomeModsIdsPages: new_state.incomeModsIdsPages,
                currentPage: new_state.currentPage,
                taxAboveMultiplierValidity: new_state.taxAboveMultiplierValidity,
                autonomyFactorValidity: new_state.autonomyFactorValidity,
                message: new_state.message
            });
            localStorage.removeItem('current_state');
        }
        
    }

    onChangeGroupName(e) {
        this.setState({
            groupName: e.target.value
        });
    }

    onChangeIsTaxFixed(e) {
        this.setState({
            taxFixed: e.target.value
        });
    }
    onChangeTaxAboveMultiplier(e) {
        if (parseFloat(e.target.value) >= -1 && parseFloat(e.target.value) <= 1) this.state.taxAboveMultiplierValidity = true;
        else this.state.taxAboveMultiplierValidity = false;
        this.setState({
            taxAboveMultiplier: e.target.value
        });
    }
    onChangeAutonomyFactor(e) {
        if (parseFloat(e.target.value) >= -1 && parseFloat(e.target.value) <= 1) this.state.autonomyFactorValidity = true;
        else this.state.autonomyFactorValidity = false;
        this.setState({
            autonomyFactor: e.target.value
        });
    }
    onChangeIncomeModsId(e) {

        this.setState({
            incomeModsId: e.target.value
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState({
            currentPage: page
        });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages,
                    incomeModsId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        if (this.checkValidity()) {
            var data = {
                groupName: this.state.groupName,
                taxFixed: this.state.taxFixed,
                taxAboveMultiplier: this.state.taxAboveMultiplier,
                autonomyFactor: this.state.autonomyFactor,
                incomeModsId: this.state.incomeModsId
            };

            legalSystemService.createEntity(data)
                .then(response => {
                    this.setState({
                        groupName: response.groupName,
                        taxFixed: response.taxFixed,
                        taxAboveMultiplier: response.taxAboveMultiplier,
                        autonomyFactor: response.autonomyFactor,
                        incomeModsId: response.incomeModsId,

                        submitted: true
                    });
                    console.log(response);
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    newEntity() {
        this.setState({
            id: null,
            groupName: "",
            taxFixed: true,
            taxAboveMultiplier: 0,
            autonomyFactor: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddIncomeMode() {
        localStorage.setItem('current_state', JSON.stringify(this.state));
        this.props.history.push('/add/incomeAffectableMode');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="groupName">groupName</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="groupName"
                                    required
                                    value={this.state.groupName}
                                    onChange={this.onChangeGroupName}
                                    name="groupName"
                                />
                            </div>
                            <div className="form-group">
                                {"TaxFixed: "}
                                <select onChange={this.onChangeIsTaxFixed} value={this.state.taxFixed}>
                                    {this.state.taxFixedSelectable.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="taxAboveMultiplier">taxAboveMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="taxAboveMultiplier"
                                    required
                                    value={this.state.taxAboveMultiplier}
                                    onChange={this.onChangeTaxAboveMultiplier}
                                    name="taxAboveMultiplier"
                                />
                            </div>
                            {!this.state.taxAboveMultiplierValidity ? (
                                <p><font size="2" color="red">taxAboveMultiplier should be float less than 1 and bigger than -1</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="autonomyFactor">autonomyFactor</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="autonomyFactor"
                                    required
                                    value={this.state.autonomyFactor}
                                    onChange={this.onChangeAutonomyFactor}
                                    name="autonomyFactor"
                                />
                            </div>
                            {!this.state.autonomyFactorValidity ? (
                                <p><font size="2" color="red">autonomyFactor should be float less than 1 and bigger than -1</font></p>)
                                : (<div />)}
                  
                            <div className="mt-3 mb-1">
                                {"IncomeModsId: "}
                                <select onChange={this.onChangeIncomeModsId} value={this.state.incomeModsId}>
                                    {this.state.incomeModsIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={this.state.currentPage}>
                                    {this.state.incomeModsIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddIncomeMode}
                                >
                                    Add
            </button>
                            </div>



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddLegalSystem };