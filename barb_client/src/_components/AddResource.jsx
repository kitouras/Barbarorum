import React, { Component } from "react";
import { resourceService } from '@/_services';

export default class AddResource extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePopulationGrowthMultiplier = this.onChangePopulationGrowthMultiplier.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            id: null,
            name: "",
            populationGrowthMultiplier: 0,

            submitted: false
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangePopulationGrowthMultiplier(e) {
        this.setState({
            populationGrowthMultiplier: e.target.value
        });
    }

    saveEntity() {
        var data = {
            name: this.state.name,
            populationGrowthMultiplier: this.state.populationGrowthMultiplier
        };

        resourceService.createEntity(data)
            .then(response => {
                this.setState({
                    id: response.id,
                    name: response.name,
                    populationGrowthMultiplier: response.populationGrowthMultiplier,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            populationGrowthMultiplier: 0,

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            Add
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>


                            <div className="form-group">
                                <label htmlFor="populationGrowthMultiplier">PopulationGrowthMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="populationGrowthMultiplier"
                                    required
                                    value={this.state.populationGrowthMultiplier}
                                    onChange={this.onChangePopulationGrowthMultiplier}
                                    name="populationGrowthMultiplier"
                                />
                            </div>

                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddResource };