import React, { Component } from "react";
import { provinceFractionsService } from '@/_services';

class ProvinceFractions extends Component {
    constructor(props) {
        super(props);
        this.onChangeAuthority = this.onChangeAuthority.bind(this);
        this.onChangeIntegrity = this.onChangeIntegrity.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                provinceId: null,
                fractionId: null,
                authority: 0,
                integrity: 0
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.provinceId, this.props.match.params.fractionId);
    }


    onChangeAuthority(e) {
        const authority = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    authority: authority,
                },
            };
        });
    }
    onChangeIntegrity(e) {
        const integrity = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    integrity: integrity,
                },
            };
        });
    }
  


    getEntity(provinceId, fractionId) {
        provinceFractionsService.getEntity(provinceId, fractionId)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const provinceId = this.state.currentEntity.provinceId.id;
                    const fractionId = this.state.currentEntity.fractionId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            provinceId: provinceId,
                            fractionId: fractionId
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    updateEntity() {
        provinceFractionsService
            .updateEntity(this.state.currentEntity.provinceId, this.state.currentEntity.fractionId, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The provinceFractions was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        provinceFractionsService
            .deleteEntity(this.state.currentEntity.provinceId, this.state.currentEntity.fractionId)
            .then(() => {
                this.props.history.push("/provinceFractions");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity, incomeModsIds, incomeModsIdsPages, currentPage } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>ProvinceFractions</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="authority">authority</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="authority"
                                    value={currentEntity.authority}
                                    onChange={this.onChangeAuthority}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="integrity">integrity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="integrity"
                                    value={currentEntity.integrity}
                                    onChange={this.onChangeIntegrity}
                                />
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a ProvinceFractions...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { ProvinceFractions };
