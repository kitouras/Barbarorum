import React, { Component } from "react";
import { Link } from "react-router-dom";
import { provinceService } from '@/_services';
import Pagination from "@material-ui/lab/Pagination";

class ProvincePage extends Component {
    constructor(props) {
        super(props);
        this.onChangeSearchName = this.onChangeSearchName.bind(this);
        this.retrieveEntities = this.retrieveEntities.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveEntities = this.setActiveEntities.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handlePageSizeChange = this.handlePageSizeChange.bind(this);

        this.state = {
            entities: [],
            currentEntities: null,
            currentIndex: -1,
            searchName: "",

            page: 1,
            count: 0,
            pageSize: 6,
        };

        this.pageSizes = [3, 6, 9];
    }

    componentDidMount() {
        this.retrieveEntities();
    }

    onChangeSearchName(e) {
        const searchName = e.target.value;

        this.setState({
            searchName: searchName,
        });
    }

    refreshList() {
        this.retrieveEntities();
        this.setState({
            currentEntities: null,
            currentIndex: -1
        });
    }

    setActiveEntities(entities, index) {
        this.setState({
            currentEntities: entities,
            currentIndex: index
        });
    }

    getRequestParams(searchName, page, pageSize) {
        let params = {};

        if (searchName) {
            params["name"] = searchName;
        }

        if (page) {
            params["page"] = page - 1;
        }

        if (pageSize) {
            params["size"] = pageSize;
        }

        return params;
    }

    retrieveEntities() {
        const { searchName, page, pageSize } = this.state;
        const params = this.getRequestParams(searchName, page, pageSize);

        provinceService.getEntities(params)
            .then((response) => {

                const content = response.content;
                const totalPages = response.totalPages;

                this.setState({
                    entities: content,
                    count: totalPages,
                });
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    handlePageChange(event, value) {
        this.setState(
            {
                page: value,
            },
            () => {
                this.retrieveEntities();
            }
        );
    }

    handlePageSizeChange(event) {
        this.setState(
            {
                pageSize: event.target.value,
                page: 1
            },
            () => {
                this.retrieveEntities();
            }
        );
    }


    render() {
        const {
            searchName,
            entities,
            currentEntities,
            currentIndex,
            page,
            count,
            pageSize,
        } = this.state;

        return (
            <div className="list row">
                <div className="col-md-8">
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Search by title"
                            value={searchName}
                            onChange={this.onChangeSearchName}
                        />
                        <div className="input-group-append">
                            <button
                                className="btn btn-outline-secondary"
                                type="button"
                                onClick={this.retrieveEntities}
                            >
                                Search
              </button>
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <h4>Province List:</h4>

                    <div className="mt-3">
                        {"Items per Page: "}
                        <select onChange={this.handlePageSizeChange} value={pageSize}>
                            {this.pageSizes.map((size) => (
                                <option key={size} value={size}>
                                    {size}
                                </option>
                            ))}
                        </select>

                        <Pagination
                            className="my-3"
                            count={count}
                            page={page}
                            siblingCount={1}
                            boundaryCount={1}
                            variant="outlined"
                            shape="rounded"
                            onChange={this.handlePageChange}
                        />
                    </div>

                    <ul className="list-group">
                        {entities &&
                            entities.map((entity, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActiveEntities(entity, index)}
                                    key={index}
                                >
                                    {entity.id}
                                </li>
                            ))}
                    </ul>

                    <Link
                        to={"/add/province"}
                        className="m-2 btn btn-light"
                    >
                        Add
              </Link>

                    <button
                        className="m-4 btn btn-sm btn-danger"
                    >
                        Remove All
          </button>



                </div>
                <div className="col-md-6">
                    {currentEntities ? (
                        <div>
                            <h4>Province</h4>
                            <div>
                                <label>
                                    <strong>name:</strong>
                                </label>{" "}
                                {currentEntities.name}
                            </div>
                            <div>
                                <label>
                                    <strong>autonomy:</strong>
                                </label>{" "}
                                {currentEntities.autonomy}
                            </div>
                            <div>
                                <label>
                                    <strong>rebellionPotential:</strong>
                                </label>{" "}
                                {currentEntities.rebellionPotential}
                            </div>
                            <div>
                                <label>
                                    <strong>villagePopularity:</strong>
                                </label>{" "}
                                {currentEntities.villagePopularity}
                            </div>
                            <div>
                                <label>
                                    <strong>popularityGrowth:</strong>
                                </label>{" "}
                                {currentEntities.popularityGrowth}
                            </div>
                            <div>
                                <label>
                                    <strong>treasury:</strong>
                                </label>{" "}
                                {currentEntities.treasury}
                            </div>
                            <div>
                                <label>
                                    <strong>taxInto:</strong>
                                </label>{" "}
                                {currentEntities.taxInto}
                            </div>
                            <div>
                                <label>
                                    <strong>taxAbove:</strong>
                                </label>{" "}
                                {currentEntities.taxAbove}
                            </div>

                            <div>
                                <label>
                                    <strong>cityId:</strong>
                                </label>{" "}
                                {currentEntities.cityId.id}
                            </div>
                            <div>
                                <label>
                                    <strong>stateId:</strong>
                                </label>{" "}
                                {currentEntities.stateId.id}
                            </div>
                            <div>
                                <label>
                                    <strong>legalSystemId:</strong>
                                </label>{" "}
                                {currentEntities.legalSystemId.id}
                            </div>
                            <div>
                                <label>
                                    <strong>climatId:</strong>
                                </label>{" "}
                                {currentEntities.climatId.id}
                            </div>
                            <div>
                                <label>
                                    <strong>seasonId:</strong>
                                </label>{" "}
                                {currentEntities.seasonId.id}
                            </div>
                            <div>
                                <label>
                                    <strong>landscapeId:</strong>
                                </label>{" "}
                                {currentEntities.landscapeId.id}
                            </div>
                            <Link
                                to={"/edit/province/" + currentEntities.id}
                                className="badge badge-warning"
                            >
                                Edit
              </Link>
                        </div>
                    ) : (
                            <div>
                                <br />
                                <p>Please click on a Province...</p>
                            </div>
                        )}

                </div>
            </div>
        );
    }
}

export { ProvincePage };