import React, { Component } from "react";
import { religionService, incomeAffectableModeService } from '@/_services';

export default class AddReligion extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeTolerance = this.onChangeTolerance.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddIncomeMode = this.OnClickAddIncomeMode.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            id: null,
            name: "",
            tolerance: 0,
            incomeModsId: 0,

            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,

            toleranceValidity: true,

            submitted: false
        };
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_state") != null) {
            console.log(localStorage.getItem("current_state"));
            const new_state = JSON.parse(localStorage.getItem("current_state"));
            this.setState({
                name: new_state.name,
                tolerance: new_state.tolerance,
                incomeModsId: new_state.incomeModsId,
                incomeModsIds: new_state.incomeModsIds,
                incomeModsIdsTotalPages: new_state.incomeModsIdsTotalPages,
                incomeModsIdsPages: new_state.incomeModsIdsPages,
                currentPage: new_state.currentPage,
                message: new_state.message,
                toleranceValidity: new_state.toleranceValidity
            });
            localStorage.removeItem('current_state');
        }
        
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeTolerance(e) {
        if (parseFloat(e.target.value) >= -1 && parseFloat(e.target.value) <= 1) this.state.toleranceValidity = true;
        else this.state.toleranceValidity = false;
        this.setState({
            tolerance: e.target.value
        });
    }
    onChangeIncomeModsId(e) {
        this.setState({
            incomeModsId: e.target.value
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState({
            currentPage: page
        });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages,
                    incomeModsId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        if (this.checkValidity()) {
            var data = {
                name: this.state.name,
                tolerance: this.state.tolerance,
                incomeModsId: this.state.incomeModsId
            };

            religionService.createEntity(data)
                .then(response => {
                    this.setState({
                        name: response.name,
                        tolerance: response.tolerance,
                        incomeModsId: response.incomeModsId,

                        submitted: true
                    });
                    console.log(response);
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            tolerance: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddIncomeMode() {
        localStorage.setItem('current_state', JSON.stringify(this.state));
        this.props.history.push('/add/incomeAffectableMode');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="tolerance">tolerance</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tolerance"
                                    required
                                    value={this.state.tolerance}
                                    onChange={this.onChangeTolerance}
                                    name="tolerance"
                                />
                            </div>
                            {!this.state.toleranceValidity ? (
                                <p><font size="2" color="red">tolerance should be float less than 1 and bigger than -1.</font></p>)
                                : (<div />)}
                            <div className="mt-3 mb-1">
                                {"IncomeModsId: "}
                                <select onChange={this.onChangeIncomeModsId} value={this.state.incomeModsId}>
                                    {this.state.incomeModsIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={this.state.currentPage}>
                                    {this.state.incomeModsIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddIncomeMode}
                                >
                                    Add
            </button>
                            </div>



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddReligion };