import React, { Component } from "react";
import { seasonService, incomeAffectableModeService } from '@/_services';

class Season extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeTemperatureMod = this.onChangeTemperatureMod.bind(this);
        this.onChangeWindMod = this.onChangeWindMod.bind(this);
        this.onChangeHumidityMod = this.onChangeHumidityMod.bind(this);
        this.onChangeAtmosphericPressureMod = this.onChangeAtmosphericPressureMod.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddIncomeMode = this.OnClickAddIncomeMode.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                temperatureMod: 0,
                windMod: 0,
                humidityMod: 0,
                atmosphericPressureMod: 0,
                incomeModsId: 0
            },
            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,

            temperatureModValidity: true,
            windModValidity: true,
            humidityModValidity: true,
            atmosphericPressureModValidity: true,
            message: "",
        };
    }
    checkValidity() {
        return this.state.temperatureModValidity && this.state.windModValidity
            && this.state.humidityModValidity && this.state.atmosphericPressureModValidity;
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_state") != null) {
            console.log(localStorage.getItem("current_state"));
            const new_state = JSON.parse(localStorage.getItem("current_state"));
            this.setState({
                currentEntity: new_state.currentEntity,
                incomeModsIds: new_state.incomeModsIds,
                incomeModsIdsTotalPages: new_state.incomeModsIdsTotalPages,
                incomeModsIdsPages: new_state.incomeModsIdsPages,
                currentPage: new_state.currentPage,
                temperatureModValidity: new_state.temperatureModValidity,
                windModValidity: new_state.windModValidity,
                humidityModValidity: new_state.humidityModValidity,
                atmosphericPressureModValidity: new_state.atmosphericPressureModValidity,
                message: new_state.message
            });
            localStorage.removeItem('current_state');
        } else this.getEntity(this.props.match.params.id);
        
    }


    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }
    onChangeTemperatureMod(e) {
        const temperatureMod = e.target.value;
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.temperatureModValidity = true;
        else this.state.temperatureModValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    temperatureMod: temperatureMod,
                },
            };
        });
    }
    onChangeWindMod(e) {
        const windMod = e.target.value;
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.windModValidity = true;
        else this.state.windModValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    windMod: windMod,
                },
            };
        });
    }
    onChangeHumidityMod(e) {
        const humidityMod = e.target.value;
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.humidityModValidity = true;
        else this.state.humidityModValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    humidityMod: humidityMod,
                },
            };
        });
    }
    onChangeAtmosphericPressureMod(e) {
        const atmosphericPressureMod = e.target.value;
        if (parseFloat(e.target.value) >= 0 || parseFloat(e.target.value) < 0) this.state.atmosphericPressureModValidity = true;
        else this.state.atmosphericPressureModValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    atmosphericPressureMod: atmosphericPressureMod,
                },
            };
        });
    }
    onChangeIncomeModsId(e) {
        const incomeModsId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    incomeModsId: incomeModsId,
                },
            };
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState(function (prevState) {
            return {
                currentPage: page,
            };
        });
    }


    getEntity(id) {
        seasonService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const incomeModsId = this.state.currentEntity.incomeModsId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            incomeModsId: incomeModsId,
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        if (this.checkValidity()) 
            seasonService
                .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
                .then((reponse) => {
                    console.log(reponse);

                    this.setState({ message: "The season was updated successfully!" });
                })
                .catch((e) => {
                    console.log(e);
                });
    }

    removeEntity() {
        seasonService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/season");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddIncomeMode() {
        localStorage.setItem('current_state', JSON.stringify(this.state));
        this.props.history.push('/add/incomeAffectableMode');
    }

    render() {
        const { currentEntity, incomeModsIds, incomeModsIdsPages, currentPage } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Season</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="temperatureMod">temperatureMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="temperatureMod"
                                    value={currentEntity.temperatureMod}
                                    onChange={this.onChangeTemperatureMod}
                                />
                            </div>
                            {!this.state.temperatureModValidity ? (
                                <p><font size="2" color="red">temperatureMod should be float.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="windMod">windMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="windMod"
                                    value={currentEntity.windMod}
                                    onChange={this.onChangeWindMod}
                                />
                            </div>
                            {!this.state.windModValidity ? (
                                <p><font size="2" color="red">windMod should be float.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="humidityMod">humidityMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="humidityMod"
                                    value={currentEntity.humidityMod}
                                    onChange={this.onChangeHumidityMod}
                                />
                            </div>
                            {!this.state.humidityModValidity ? (
                                <p><font size="2" color="red">humidityMod should be float.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="atmosphericPressureMod">atmosphericPressureMod</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="atmosphericPressureMod"
                                    value={currentEntity.atmosphericPressureMod}
                                    onChange={this.onChangeAtmosphericPressureMod}
                                />
                            </div>
                            {!this.state.atmosphericPressureModValidity ? (
                                <p><font size="2" color="red">atmosphericPressureMod should be float.</font></p>)
                                : (<div />)}
                            <div className="mt-3 mb-1">
                                {"IncomeModsId: "}
                                <select onChange={this.onChangeIncomeModsId} value={currentEntity.incomeModsId}>
                                    {incomeModsIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={currentPage}>
                                    {incomeModsIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddIncomeMode}
                                >
                                    Add
            </button>
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Season...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Season };
