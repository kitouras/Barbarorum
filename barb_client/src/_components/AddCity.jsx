import React, { Component } from "react";
import { cityService, specializationService } from '@/_services';

export default class AddCity extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePopularity = this.onChangePopularity.bind(this);
        this.onChangeSpecializationId = this.onChangeSpecializationId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddSpecialization = this.OnClickAddSpecialization.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            id: null,
            name: "",
            popularity: 0,
            specializationId: 0,

            specializationIds: [],
            specializationIdsTotalPages: 1,
            specializationIdsPages: [],
            currentPage: 0,

            popularityValidity: true,

            submitted: false
        };
    }

    checkValidity() {
        return this.state.popularityValidity;
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_city_state") != null) {
            console.log(localStorage.getItem("current_city_state"));
            const new_state = JSON.parse(localStorage.getItem("current_city_state"));
            this.setState({
                name: new_state.name,
                popularity: new_state.popularity,
                specializationId: new_state.specializationId,
                specializationIds: new_state.specializationIds,
                specializationIdsTotalPages: new_state.specializationIdsTotalPages,
                specializationIdsPages: new_state.specializationIdsPages,
                currentPage: new_state.currentPage,
                message: new_state.message,
                popularityValidity: new_state.popularityValidity
            });
            localStorage.removeItem('current_city_state');
        }
        
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangePopularity(e) {
        if (parseInt(e.target.value) >= 0) this.state.popularityValidity = true;
        else this.state.popularityValidity = false;
        this.setState({
            popularity: e.target.value
        });
    }
    onChangeSpecializationId(e) {
        this.setState({
            specializationId: e.target.value
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState({
            currentPage: page
        });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        specializationService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    specializationIds: content,
                    specializationIdsTotalPages: totalPages,
                    specializationIdsPages: pages,
                    specializationId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        if (this.checkValidity()) {
            var data = {
                name: this.state.name,
                popularity: this.state.popularity,
                specializationId: this.state.specializationId
            };

            cityService.createEntity(data)
                .then(response => {
                    this.setState({
                        name: response.name,
                        popularity: response.popularity,
                        specializationId: response.specializationId,

                        submitted: true
                    });
                    console.log(response);
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            popularity: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddSpecialization() {
        localStorage.setItem('current_city_state', JSON.stringify(this.state));
        this.props.history.push('/add/specialization');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="popularity">popularity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="popularity"
                                    required
                                    value={this.state.popularity}
                                    onChange={this.onChangePopularity}
                                    name="popularity"
                                />
                            </div>
                            {!this.state.popularityValidity ? (
                                <p><font size="2" color="red">popularity should be non-negative integer.</font></p>)
                                : (<div />)}
                            <div className="mt-3 mb-1">
                                {"SpecializationId: "}
                                <select onChange={this.onChangeSpecializationId} value={this.state.specializationId}>
                                    {this.state.specializationIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={this.state.currentPage}>
                                    {this.state.specializationIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddSpecialization}
                                >
                                    Add
            </button>
                            </div>



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddCity };