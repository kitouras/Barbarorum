import React, { Component } from "react";
import { stateService } from '@/_services';

class State extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeColor = this.onChangeColor.bind(this);
        this.onChangePopularity = this.onChangePopularity.bind(this);
        this.onChangeTreasury = this.onChangeTreasury.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                color: "",
                popularity: 0,
                treasury: 0
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }

    onChangeColor(e) {
        const color = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                color: color,
            },
        }));
    }

    onChangePopularity(e) {
        const popularity = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                popularity: popularity,
            },
        }));
    }

    onChangeTreasury(e) {
        const treasury = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                treasury: treasury,
            },
        }));
    }

    getEntity(id) {
        stateService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        stateService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The state was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        stateService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/state");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>State</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="color">Color</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="color"
                                    value={currentEntity.color}
                                    onChange={this.onChangeColor}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="popularity">Popularity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="popularity"
                                    value={currentEntity.popularity}
                                    onChange={this.onChangePopularity}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="treasury">Treasury</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="treasury"
                                    value={currentEntity.treasury}
                                    onChange={this.onChangeTreasury}
                                />
                            </div>
                        </form>

                        <button
                            className="badge badge-light mr-2"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a State...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { State };
