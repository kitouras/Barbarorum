import React, { Component } from "react";
import { landscapeService, incomeAffectableModeService } from '@/_services';

class Landscape extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeMountainousness = this.onChangeMountainousness.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddIncomeMode = this.OnClickAddIncomeMode.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                mountainousness: 0,
                incomeModsId: 0
            },
            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,
            message: "",
        };
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_state") != null) {
            console.log(localStorage.getItem("current_state"));
            const new_state = JSON.parse(localStorage.getItem("current_state"));
            this.setState({
                currentEntity: new_state.currentEntity,
                incomeModsIds: new_state.incomeModsIds,
                incomeModsIdsTotalPages: new_state.incomeModsIdsTotalPages,
                incomeModsIdsPages: new_state.incomeModsIdsPages,
                currentPage: new_state.currentPage,
                message: new_state.message
            });
            localStorage.removeItem('current_state');
        } else this.getEntity(this.props.match.params.id);
        
    }


    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }
    onChangeMountainousness(e) {
        const mountainousness = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    mountainousness: mountainousness,
                },
            };
        });
    }

    onChangeIncomeModsId(e) {
        const incomeModsId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    incomeModsId: incomeModsId,
                },
            };
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState(function (prevState) {
            return {
                currentPage: page,
            };
        });
    }


    getEntity(id) {
        landscapeService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const incomeModsId = this.state.currentEntity.incomeModsId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            incomeModsId: incomeModsId,
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        landscapeService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The landscape was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        landscapeService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/landscape");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddIncomeMode() {
        localStorage.setItem('current_state', JSON.stringify(this.state));
        this.props.history.push('/add/incomeAffectableMode');
    }

    render() {
        const { currentEntity, incomeModsIds, incomeModsIdsPages, currentPage } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Landscape</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="mountainousness">mountainousness</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="mountainousness"
                                    value={currentEntity.mountainousness}
                                    onChange={this.onChangeMountainousness}
                                />
                            </div>

                            <div className="mt-3 mb-1">
                                {"IncomeModsId: "}
                                <select onChange={this.onChangeIncomeModsId} value={currentEntity.incomeModsId}>
                                    {incomeModsIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={currentPage}>
                                    {incomeModsIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddIncomeMode}
                                >
                                    Add
            </button>
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Landscape...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Landscape };
