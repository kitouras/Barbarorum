import React, { Component } from "react";
import { neighborhoodOfProvincesService, provinceService, relationService } from '@/_services';

export default class AddNeighborhoodOfProvinces extends Component {
    constructor(props) {
        super(props);
        this.onChangeFirstProvinceId = this.onChangeFirstProvinceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getFirstProvince = this.getFirstProvince.bind(this);
        this.onChangeFirstProvincePage = this.onChangeFirstProvincePage.bind(this);
        this.OnClickAddProvince = this.OnClickAddProvince.bind(this);
        this.onChangeSecondProvinceId = this.onChangeSecondProvinceId.bind(this);
        this.getSecondProvince = this.getSecondProvince.bind(this);
        this.onChangeSecondProvincePage = this.onChangeSecondProvincePage.bind(this);
        this.onChangeRelationId = this.onChangeRelationId.bind(this);
        this.getRelation = this.getRelation.bind(this);
        this.onChangeRelationPage = this.onChangeRelationPage.bind(this);
        this.OnClickAddRelation = this.OnClickAddRelation.bind(this);

        this.state = {

            firstProvinceId: 0,
            secondProvinceId: 0,
            currentFirstProvincePage: 0,
            firstProvinceIds: [],
            secondProvinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentSecondProvincePage: 0,

            relationIds: [],
            relationIdsTotalPages: 1,
            relationIdsPages: [],
            currentRelationPage: 0,
            submitted: false
        };
    }

    componentDidMount() {
        this.getFirstProvince(this.state.currentProvincePage);
        this.getSecondProvince(this.state.currentProvincePage);
        this.getRelation(this.state.currentRelationPage);
        if (localStorage.getItem("current_neighborhoodOfProvinces_state") != null) {
            console.log(localStorage.getItem("current_neighborhoodOfProvinces_state"));
            const new_state = JSON.parse(localStorage.getItem("current_neighborhoodOfProvinces_state"));
            this.setState({

                firstProvinceId: new_state.firstProvinceId,
                secondProvinceId: new_state.secondProvinceId,
                relationId: new_state.firstProvinceId,
                currentFirstProvincePage: new_state.currentProvincePage,

                firstProvinceIds: new_state.firstProvinceIds,
                secondProvinceIds: new_state.secondProvinceIds,
                provinceIdsTotalPages: new_state.provinceIdsTotalPages,
                provinceIdsPages: new_state.provinceIdsPages,
                currentProvincePage: new_state.currentProvincePage,

                relationIds: new_state.relationIds,
                relationIdsTotalPages: new_state.relationIdsTotalPages,
                relationIdsPages: new_state.relationIdsPages,
                currentRelationPage: new_state.currentRelationPage,
                message: new_state.message
            });
            localStorage.removeItem('current_neighborhoodOfProvinces_state');
        }

    }


    onChangeFirstProvinceId(e) {
        this.setState({
            firstProvinceId: e.target.value
        });
    }
    onChangeFirstProvincePage(e) {
        const page = e.target.value;
        this.getFirstProvince(page);
        this.setState({
            currentFirstProvincePage: page
        });
    }

    onChangeRelationId(e) {
        this.setState({
            relationId: e.target.value
        });
    }
    onChangeRelationPage(e) {
        const page = e.target.value;
        this.getRelation(page);
        this.setState({
            currentRelationPage: page
        });
    }

    onChangeSecondProvinceId(e) {
        this.setState({
            secondProvinceId: e.target.value
        });
    }
    onChangeSecondProvincePage(e) {
        const page = e.target.value;
        this.getSecondProvince(page);
        this.setState({
            currentSecondProvincePage: page
        });
    }

    getFirstProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    firstProvinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    firstProvinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getSecondProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    secondProvinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    secondProvinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getRelation(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        relationService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    relationIds: content,
                    relationIdsTotalPages: totalPages,
                    relationIdsPages: pages,
                    relationId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {

            firstProvinceId: this.state.firstProvinceId,
            secondProvinceId: this.state.secondProvinceId,
            relationId: this.state.relationId
        };

        neighborhoodOfProvincesService.createEntity(data)
            .then(response => {
                this.setState({

                    firstProvinceId: response.firstProvinceId,
                    secondProvinceId: response.secondProvinceId,
                    relationId: response.relationId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,

            relationId: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddProvince() {
        localStorage.setItem('current_neighborhoodOfProvinces_state', JSON.stringify(this.state));
        this.props.history.push('/add/province');
    }
    OnClickAddRelation() {
        localStorage.setItem('current_neighborhoodOfProvinces_state', JSON.stringify(this.state));
        this.props.history.push('/add/relation');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"FirstProvinceId: "}
                                <select onChange={this.onChangeFirstProvinceId} value={this.state.firstProvinceId}>
                                    {this.state.firstProvinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeFirstProvincePage} value={this.state.currentFirstProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"SecondProvinceId: "}
                                <select onChange={this.onChangeSecondProvinceId} value={this.state.secondProvinceId}>
                                    {this.state.secondProvinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeSecondProvincePage} value={this.state.currentSecondProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>
                            <div className="mt-3 mb-1">
                                {"RelationId: "}
                                <select onChange={this.onChangeRelationId} value={this.state.relationId}>
                                    {this.state.relationIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeRelationPage} value={this.state.currentRelationPage}>
                                    {this.state.relationIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddRelation}
                                >
                                    Add
            </button>
                            </div>






                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddNeighborhoodOfProvinces };