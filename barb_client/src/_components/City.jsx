import React, { Component } from "react";
import { cityService, specializationService } from '@/_services';

class City extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePopularity = this.onChangePopularity.bind(this);
        this.onChangeSpecializationId = this.onChangeSpecializationId.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddSpecialization = this.OnClickAddSpecialization.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                popularity: 0,
                specializationId: 0
            },
            specializationIds: [],
            specializationIdsTotalPages: 1,
            specializationIdsPages: [],
            currentPage: 0,

            popularityValidity: true,

            message: "",
        };
    }

    checkValidity() {
        return this.state.popularityValidity;
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_city_state") != null) {
            console.log(localStorage.getItem("current_city_state"));
            const new_state = JSON.parse(localStorage.getItem("current_city_state"));
            this.setState({
                currentEntity: new_state.currentEntity,
                specializationIds: new_state.specializationIds,
                specializationIdsTotalPages: new_state.specializationIdsTotalPages,
                specializationIdsPages: new_state.specializationIdsPages,
                currentPage: new_state.currentPage,
                message: new_state.message,
                popularityValidity: new_state.popularityValidity
            });
            localStorage.removeItem('current_city_state');
        } else this.getEntity(this.props.match.params.id);
        
    }


    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }
    onChangePopularity(e) {
        const popularity = e.target.value;
        if (parseInt(e.target.value) >= 0) this.state.popularityValidity = true;
        else this.state.popularityValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    popularity: popularity,
                },
            };
        });
    }

    onChangeSpecializationId(e) {
        const specializationId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    specializationId: specializationId,
                },
            };
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState(function (prevState) {
            return {
                currentPage: page,
            };
        });
    }


    getEntity(id) {
        cityService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const specializationId = this.state.currentEntity.specializationId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            specializationId: specializationId,
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        specializationService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    specializationIds: content,
                    specializationIdsTotalPages: totalPages,
                    specializationIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        if (this.checkValidity()) 
            cityService
                .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
                .then((reponse) => {
                    console.log(reponse);

                    this.setState({ message: "The city was updated successfully!" });
                })
                .catch((e) => {
                    console.log(e);
                });
    }

    removeEntity() {
        cityService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/city");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddSpecialization() {
        localStorage.setItem('current_city_state', JSON.stringify(this.state));
        this.props.history.push('/add/specialization');
    }

    render() {
        const { currentEntity, specializationIds, specializationIdsPages, currentPage } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>City</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="popularity">popularity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="popularity"
                                    value={currentEntity.popularity}
                                    onChange={this.onChangePopularity}
                                />
                            </div>
                            {!this.state.popularityValidity ? (
                                <p><font size="2" color="red">popularity should be non-negative integer.</font></p>)
                                : (<div />)}
                            <div className="mt-3 mb-1">
                                {"SpecializationId: "}
                                <select onChange={this.onChangeSpecializationId} value={currentEntity.specializationId}>
                                    {specializationIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={currentPage}>
                                    {specializationIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddSpecialization}
                                >
                                    Add
            </button>
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a City...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { City };
