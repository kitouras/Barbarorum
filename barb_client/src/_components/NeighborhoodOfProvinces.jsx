import React, { Component } from "react";
import { neighborhoodOfProvincesService, relationService } from '@/_services';

class NeighborhoodOfProvinces extends Component {
    constructor(props) {
        super(props);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.onChangeRelationId = this.onChangeRelationId.bind(this);
        this.getRelation = this.getRelation.bind(this);
        this.onChangeRelationPage = this.onChangeRelationPage.bind(this);
        this.OnClickAddRelation = this.OnClickAddRelation.bind(this);

        this.state = {
            currentEntity: {
                firstProvinceId: null,
                secondProvinceId: null,
                relationId: 0,
            },
            relationIds: [],
            relationIdsTotalPages: 1,
            relationIdsPages: [],
            currentRelationPage: 0,
            message: "",
        };
    }

    componentDidMount() {
        this.getRelation(this.state.currentRelationPage);
        if (localStorage.getItem("current_neighborhoodOfProvinces_state") != null) {
            console.log(localStorage.getItem("current_neighborhoodOfProvinces_state"));
            const new_state = JSON.parse(localStorage.getItem("current_neighborhoodOfProvinces_state"));
            this.setState({

                firstProvinceId: new_state.firstProvinceId,
                secondProvinceId: new_state.secondProvinceId,
                relationId: new_state.firstProvinceId,

                relationIds: new_state.relationIds,
                relationIdsTotalPages: new_state.relationIdsTotalPages,
                relationIdsPages: new_state.relationIdsPages,
                currentRelationPage: new_state.currentRelationPage,
                message: new_state.message
            });
            localStorage.removeItem('current_neighborhoodOfProvinces_state');
        } else this.getEntity(this.props.match.params.firstProvinceId, this.props.match.params.secondProvinceId);
    }


    onChangeRelationId(e) {
        const relationId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    relationId: relationId,
                },
            };
        });
    }

    onChangeRelationPage(e) {
        const page = e.target.value;
        this.getRelationId(page);
        this.setState(function (prevState) {
            return {
                currentRelationPage: page,
            };
        });
    }


    getEntity(firstProvinceId, secondProvinceId) {
        neighborhoodOfProvincesService.getEntity(firstProvinceId, secondProvinceId)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const firstProvinceId = this.state.currentEntity.firstProvinceId.id;
                    const secondProvinceId = this.state.currentEntity.secondProvinceId.id;
                    const relationId = this.state.currentEntity.relationId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            firstProvinceId: firstProvinceId,
                            secondProvinceId: secondProvinceId,
                            relationId: relationId
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getRelation(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        relationService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    relationIds: content,
                    relationIdsTotalPages: totalPages,
                    relationIdsPages: pages,
                    relationId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    updateEntity() {
        neighborhoodOfProvincesService
            .updateEntity(this.state.currentEntity.firstProvinceId, this.state.currentEntity.secondProvinceId, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The neighborhoodOfProvinces was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        neighborhoodOfProvincesService
            .deleteEntity(this.state.currentEntity.firstProvinceId, this.state.currentEntity.secondProvinceId)
            .then(() => {
                this.props.history.push("/neighborhoodOfProvinces");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }
    OnClickAddRelation() {
        localStorage.setItem('current_neighborhoodOfProvinces_state', JSON.stringify(this.state));
        this.props.history.push('/add/relation');
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>NeighborhoodOfProvinces</h4>
                        <form>
                            <div className="mt-3 mb-1">
                                {"RelationId: "}
                                <select onChange={this.onChangeRelationId} value={this.state.currentEntity.relationId}>
                                    {this.state.relationIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeRelationPage} value={this.state.currentRelationPage}>
                                    {this.state.relationIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddRelation}
                                >
                                    Add
            </button>
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a NeighborhoodOfProvinces...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { NeighborhoodOfProvinces };
