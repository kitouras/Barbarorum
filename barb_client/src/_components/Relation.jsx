import React, { Component } from "react";
import { relationService } from '@/_services';

class Relation extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeFractionsConnectivity = this.onChangeFractionsConnectivity.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                fractionsConnectivity: 0,
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }


    onChangeFractionsConnectivity(e) {
        const fractionsConnectivity = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                fractionsConnectivity: fractionsConnectivity,
            },
        }));
    }

    getEntity(id) {
        relationService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        relationService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The relation was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        relationService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/relation");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Relation</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="fractionsConnectivity">FractionsConnectivity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="fractionsConnectivity"
                                    value={currentEntity.fractionsConnectivity}
                                    onChange={this.onChangeFractionsConnectivity}
                                />
                            </div>
                        </form>

                        <button
                            className="badge badge-light mr-2"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Relation...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Relation };
