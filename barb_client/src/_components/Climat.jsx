import React, { Component } from "react";
import { climatService, incomeAffectableModeService } from '@/_services';

class Climat extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeTemperatureDegree = this.onChangeTemperatureDegree.bind(this);
        this.onChangeWindDegree = this.onChangeWindDegree.bind(this);
        this.onChangeHumidityDegree = this.onChangeHumidityDegree.bind(this);
        this.onChangeAtmosphericPressureDegree = this.onChangeAtmosphericPressureDegree.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddIncomeMode = this.OnClickAddIncomeMode.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                temperatureDegree: 0,
                windDegree: 0,
                humidityDegree: 0,
                atmosphericPressureDegree: 0,
                incomeModsId: 0
            },
            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,

            temperatureDegreeValidity: true,
            windDegreeValidity: true,
            humidityDegreeValidity: true, 
            atmosphericPressureDegreeValidity: true,

            message: "",
        };
    }
    checkValidity() {
        return this.state.temperatureDegreeValidity && this.state.windDegreeValidity
            && this.state.humidityDegreeValidity && this.state.atmosphericPressureDegreeValidity;
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_state") != null) {
            console.log(localStorage.getItem("current_state"));
            const new_state = JSON.parse(localStorage.getItem("current_state"));
            this.setState({
                currentEntity: new_state.currentEntity,
                incomeModsIds: new_state.incomeModsIds,
                incomeModsIdsTotalPages: new_state.incomeModsIdsTotalPages,
                incomeModsIdsPages: new_state.incomeModsIdsPages,
                currentPage: new_state.currentPage,
                temperatureDegreeValidity: new_state.temperatureDegreeValidity,
                windDegreeValidity: new_state.windDegreeValidity,
                humidityDegreeValidity: new_state.humidityDegreeValidity,
                atmosphericPressureDegreeValidity: new_state.atmosphericPressureDegreeValidity,
                message: new_state.message
            });
            localStorage.removeItem('current_state');
        } else this.getEntity(this.props.match.params.id);
        
    }


    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }
    onChangeTemperatureDegree(e) {
        const temperatureDegree = e.target.value;
        if (parseInt(temperatureDegree) >= 0 || parseInt(e.target.value) < 0) this.state.temperatureDegreeValidity = true;
        else this.state.temperatureDegreeValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    temperatureDegree: temperatureDegree,
                },
            };
        });
    }
    onChangeWindDegree(e) {
        const windDegree = e.target.value;
        if (parseInt(windDegree) >= 0) this.state.windDegreeValidity = true;
        else this.state.windDegreeValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    windDegree: windDegree,
                },
            };
        });
    }
    onChangeHumidityDegree(e) {
        const humidityDegree = e.target.value;
        if (parseInt(humidityDegree) >= 0) this.state.humidityDegreeValidity = true;
        else this.state.humidityDegreeValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    humidityDegree: humidityDegree,
                },
            };
        });
    }
    onChangeAtmosphericPressureDegree(e) {
        const atmosphericPressureDegree = e.target.value;
        if (parseInt(atmosphericPressureDegree) >= 0) this.state.atmosphericPressureDegreeValidity = true;
        else this.state.atmosphericPressureDegreeValidity = false;
        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    atmosphericPressureDegree: atmosphericPressureDegree,
                },
            };
        });
    }
    onChangeIncomeModsId(e) {
        const incomeModsId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    incomeModsId: incomeModsId,
                },
            };
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState(function (prevState) {
            return {
                currentPage: page,
            };
        });
    }


    getEntity(id) {
        climatService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const incomeModsId = this.state.currentEntity.incomeModsId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            incomeModsId: incomeModsId,
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        if (this.checkValidity())
            climatService
                .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
                .then((reponse) => {
                    console.log(reponse);

                    this.setState({ message: "The climat was updated successfully!" });
                })
                .catch((e) => {
                    console.log(e);
                });
    }

    removeEntity() {
        climatService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/climat");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddIncomeMode() {
        localStorage.setItem('current_state', JSON.stringify(this.state));
        this.props.history.push('/add/incomeAffectableMode');
    }

    render() {
        const { currentEntity, incomeModsIds, incomeModsIdsPages , currentPage} = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Climat</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="temperatureDegree">temperatureDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="temperatureDegree"
                                    value={currentEntity.temperatureDegree}
                                    onChange={this.onChangeTemperatureDegree}
                                />
                            </div>
                            {!this.state.temperatureDegreeValidity ? (
                                <p><font size="2" color="red">temperatureDegree should be integer.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="windDegree">windDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="windDegree"
                                    value={currentEntity.windDegree}
                                    onChange={this.onChangeWindDegree}
                                />
                            </div>
                            {!this.state.windDegreeValidity ? (
                                <p><font size="2" color="red">windDegree should be integer bigger than 0.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="humidityDegree">humidityDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="humidityDegree"
                                    value={currentEntity.humidityDegree}
                                    onChange={this.onChangeHumidityDegree}
                                />
                            </div>
                            {!this.state.humidityDegreeValidity ? (
                                <p><font size="2" color="red">humidityDegree should be integer bigger than 0.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="atmosphericPressureDegree">atmosphericPressureDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="atmosphericPressureDegree"
                                    value={currentEntity.atmosphericPressureDegree}
                                    onChange={this.onChangeAtmosphericPressureDegree}
                                />
                            </div>
                            {!this.state.atmosphericPressureDegreeValidity ? (
                                <p><font size="2" color="red">atmosphericPressureDegree should be integer bigger than 0.</font></p>)
                                : (<div />)}
                            <div className="mt-3 mb-1">
                                {"IncomeModsId: "}
                                <select onChange={this.onChangeIncomeModsId} value={currentEntity.incomeModsId}>
                                    {incomeModsIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={currentPage}>
                                    {incomeModsIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddIncomeMode}
                                >
                                    Add
            </button>
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Climat...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Climat };
