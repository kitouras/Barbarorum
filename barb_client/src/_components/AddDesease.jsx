import React, { Component } from "react";
import { deseaseService } from '@/_services';

export default class AddDesease extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeIncidenceRate = this.onChangeIncidenceRate.bind(this);
        this.onChangeMortality = this.onChangeMortality.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            id: null,
            name: "",
            incidenceRate: 0,
            mortality: 0,

            submitted: false
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeIncidenceRate(e) {
        this.setState({
            incidenceRate: e.target.value
        });
    }

    onChangeMortality(e) {
        this.setState({
            mortality: e.target.value
        });
    }

    saveEntity() {
        var data = {
            name: this.state.name,
            incidenceRate: this.state.incidenceRate,
            mortality: this.state.mortality
        };

        deseaseService.createEntity(data)
            .then(response => {
                this.setState({
                    id: response.id,
                    name: response.name,
                    incidenceRate: response.incidenceRate,
                    mortality: response.mortality,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            incidenceRate: 0,
            mortality: 0,

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            Add
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="incidenceRate">Incidence rate</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="incidenceRate"
                                    required
                                    value={this.state.incidenceRate}
                                    onChange={this.onChangeIncidenceRate}
                                    name="incidenceRate"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="mortality">Mortality</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="mortality"
                                    required
                                    value={this.state.mortality}
                                    onChange={this.onChangeMortality}
                                    name="mortality"
                                />
                            </div>

                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddDesease };