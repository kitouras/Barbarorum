import React, { Component } from "react";
import { climatService, incomeAffectableModeService } from '@/_services';

export default class AddClimat extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeTemperatureDegree = this.onChangeTemperatureDegree.bind(this);
        this.onChangeWindDegree = this.onChangeWindDegree.bind(this);
        this.onChangeHumidityDegree = this.onChangeHumidityDegree.bind(this);
        this.onChangeAtmosphericPressureDegree = this.onChangeAtmosphericPressureDegree.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.OnClickAddIncomeMode = this.OnClickAddIncomeMode.bind(this);
        this.checkValidity = this.checkValidity.bind(this);

        this.state = {
            id: null,
            name: "",
            temperatureDegree: 0,
            windDegree: 0,
            humidityDegree: 0,
            atmosphericPressureDegree: 0,
            incomeModsId: 0,

            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,

            temperatureDegreeValidity: true,
            windDegreeValidity: true,
            humidityDegreeValidity: true,
            atmosphericPressureDegreeValidity: true,

            submitted: false
        };
    }

    checkValidity() {
        return this.state.temperatureDegreeValidity && this.state.windDegreeValidity
            && this.state.humidityDegreeValidity && this.state.atmosphericPressureDegreeValidity;
    }

    componentDidMount() {
        this.getIncomeMods(this.state.currentPage);
        if (localStorage.getItem("current_state") != null) {
            console.log(localStorage.getItem("current_state"));
            const new_state = JSON.parse(localStorage.getItem("current_state"));
            this.setState({
                name: new_state.name,
                temperatureDegree: new_state.temperatureDegree,
                windDegree: new_state.windDegree,
                humidityDegree: new_state.humidityDegree,
                atmosphericPressureDegree: new_state.atmosphericPressureDegree,
                incomeModsId: new_state.incomeModsId,
                incomeModsIds: new_state.incomeModsIds,
                incomeModsIdsTotalPages: new_state.incomeModsIdsTotalPages,
                incomeModsIdsPages: new_state.incomeModsIdsPages,
                currentPage: new_state.currentPage,
                temperatureDegreeValidity: new_state.temperatureDegreeValidity,
                windDegreeValidity: new_state.windDegreeValidity,
                humidityDegreeValidity: new_state.humidityDegreeValidity,
                atmosphericPressureDegreeValidity: new_state.atmosphericPressureDegreeValidity,
                message: new_state.message
            });
            localStorage.removeItem('current_state');
        }
        
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeTemperatureDegree(e) {
        if (parseInt(e.target.value) >= 0 || parseInt(e.target.value) < 0) this.state.temperatureDegreeValidity = true;
        else this.state.temperatureDegreeValidity = false;
        this.setState({
            temperatureDegree: e.target.value
        });
    }
    onChangeWindDegree(e) {
        if (parseInt(e.target.value) >= 0) this.state.windDegreeValidity = true;
        else this.state.windDegreeValidity = false;
        this.setState({
            windDegree: e.target.value
        });
    }
    onChangeHumidityDegree(e) {
        if (parseInt(e.target.value) >= 0) this.state.humidityDegreeValidity = true;
        else this.state.humidityDegreeValidity = false;
        this.setState({
            humidityDegree: e.target.value
        });
    }
    onChangeAtmosphericPressureDegree(e) {
        if (parseInt(e.target.value) >= 0) this.state.atmosphericPressureDegreeValidity = true;
        else this.state.atmosphericPressureDegreeValidity = false;
        this.setState({
            atmosphericPressureDegree: e.target.value
        });
    }
    onChangeIncomeModsId(e) {
        this.setState({
            incomeModsId: e.target.value
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState({
            currentPage: page
        });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages,
                    incomeModsId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        if (this.checkValidity()) {
            var data = {
                name: this.state.name,
                temperatureDegree: this.state.temperatureDegree,
                windDegree: this.state.windDegree,
                humidityDegree: this.state.humidityDegree,
                atmosphericPressureDegree: this.state.atmosphericPressureDegree,
                incomeModsId: this.state.incomeModsId
            };

            climatService.createEntity(data)
                .then(response => {
                    this.setState({
                        name: response.name,
                        temperatureDegree: response.temperatureDegree,
                        windDegree: response.windDegree,
                        humidityDegree: response.humidityDegree,
                        atmosphericPressureDegree: response.atmosphericPressureDegree,
                        incomeModsId: response.incomeModsId,

                        submitted: true
                    });
                    console.log(response);
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            temperatureDegree: 0,
            windDegree: 0,
            humidityDegree: 0,
            atmosphericPressureDegree: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddIncomeMode() {
        localStorage.setItem('current_state', JSON.stringify(this.state));
        this.props.history.push('/add/incomeAffectableMode');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="temperatureDegree">temperatureDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="temperatureDegree"
                                    required
                                    value={this.state.temperatureDegree}
                                    onChange={this.onChangeTemperatureDegree}
                                    name="temperatureDegree"
                                />
                            </div>
                            {!this.state.temperatureDegreeValidity ? (
                                <p><font size="2" color="red">temperatureDegree should be integer.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="windDegree">windDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="windDegree"
                                    required
                                    value={this.state.windDegree}
                                    onChange={this.onChangeWindDegree}
                                    name="windDegree"
                                />
                            </div>
                            {!this.state.windDegreeValidity ? (
                                <p><font size="2" color="red">windDegree should be integer bigger than 0.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="humidityDegree">humidityDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="humidityDegree"
                                    required
                                    value={this.state.humidityDegree}
                                    onChange={this.onChangeHumidityDegree}
                                    name="humidityDegree"
                                />
                            </div>
                            {!this.state.humidityDegreeValidity ? (
                                <p><font size="2" color="red">humidityDegree should be integer bigger than 0.</font></p>)
                                : (<div />)}
                            <div className="form-group">
                                <label htmlFor="atmosphericPressureDegree">atmosphericPressureDegree</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="atmosphericPressureDegree"
                                    required
                                    value={this.state.atmosphericPressureDegree}
                                    onChange={this.onChangeAtmosphericPressureDegree}
                                    name="atmosphericPressureDegree"
                                />
                            </div>
                            {!this.state.atmosphericPressureDegreeValidity ? (
                                <p><font size="2" color="red">atmosphericPressureDegree should be integer bigger than 0.</font></p>)
                                : (<div />)}
                            <div className="mt-3 mb-1">
                                {"IncomeModsId: "}
                                <select onChange={this.onChangeIncomeModsId} value={this.state.incomeModsId}>
                                    {this.state.incomeModsIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangePage} value={this.state.currentPage}>
                                    {this.state.incomeModsIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddIncomeMode}
                                >
                                    Add
            </button>
                            </div>



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddClimat };