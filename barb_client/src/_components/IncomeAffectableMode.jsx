import React, { Component } from "react";
import { incomeAffectableModeService } from '@/_services';

class IncomeAffectableMode extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeVillageTaxMultiplier = this.onChangeVillageTaxMultiplier.bind(this);
        this.onChangeCityTaxMultiplier = this.onChangeCityTaxMultiplier.bind(this);
        this.onChangeFoodMultiplier = this.onChangeFoodMultiplier.bind(this);
        this.onChangeTradeMultiplier = this.onChangeTradeMultiplier.bind(this);
        this.onChangeHandicraftMultiplier = this.onChangeHandicraftMultiplier.bind(this);
        this.onChangeFoodMarketingMultiplier = this.onChangeFoodMarketingMultiplier.bind(this);
        this.onChangeTradeMarketingMultiplier = this.onChangeTradeMarketingMultiplier.bind(this);
        this.onChangeHandicraftMarketingMultiplier = this.onChangeHandicraftMarketingMultiplier.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                villageTaxMultiplier: 0,
                cityTaxMultiplier: 0,
                foodMultiplier: 0,
                tradeMultiplier: 0,
                handicraftMultiplier: 0,
                foodMarketingMultiplier: 0,
                tradeMarketingMultiplier: 0,
                handicraftMarketingMultiplier: 0
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }

    onChangeVillageTaxMultiplier(e) {
        const villageTaxMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    villageTaxMultiplier: villageTaxMultiplier,
                },
            };
        });
    }

    onChangeCityTaxMultiplier(e) {
        const cityTaxMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    cityTaxMultiplier: cityTaxMultiplier,
                },
            };
        });
    }
    onChangeFoodMultiplier(e) {
        const foodMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    foodMultiplier: foodMultiplier,
                },
            };
        });
    }
    onChangeTradeMultiplier(e) {
        const tradeMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    tradeMultiplier: tradeMultiplier,
                },
            };
        });
    }
    onChangeHandicraftMultiplier(e) {
        const handicraftMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    handicraftMultiplier: handicraftMultiplier,
                },
            };
        });
    }
    onChangeFoodMarketingMultiplier(e) {
        const foodMarketingMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    foodMarketingMultiplier: foodMarketingMultiplier,
                },
            };
        });
    }
    onChangeTradeMarketingMultiplier(e) {
        const tradeMarketingMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    tradeMarketingMultiplier: tradeMarketingMultiplier,
                },
            };
        });
    }
    onChangeHandicraftMarketingMultiplier(e) {
        const handicraftMarketingMultiplier = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    handicraftMarketingMultiplier: handicraftMarketingMultiplier,
                },
            };
        });
    }


    getEntity(id) {
        incomeAffectableModeService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        incomeAffectableModeService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The incomeAffectableMode was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        incomeAffectableModeService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/incomeAffectableMode");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.push("/incomeAffectableMode");
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Income Affectable Mode</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="villageTaxMultiplier">villageTaxMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="villageTaxMultiplier"
                                    value={currentEntity.villageTaxMultiplier}
                                    onChange={this.onChangeVillageTaxMultiplier}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="cityTaxMultiplier">cityTaxMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="cityTaxMultiplier"
                                    value={currentEntity.cityTaxMultiplier}
                                    onChange={this.onChangeCityTaxMultiplier}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="foodMultiplier">foodMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="foodMultiplier"
                                    value={currentEntity.foodMultiplier}
                                    onChange={this.onChangeFoodMultiplier}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="tradeMultiplier">tradeMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tradeMultiplier"
                                    value={currentEntity.tradeMultiplier}
                                    onChange={this.onChangeTradeMultiplier}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="handicraftMultiplier">handicraftMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="handicraftMultiplier"
                                    value={currentEntity.handicraftMultiplier}
                                    onChange={this.onChangeHandicraftMultiplier}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="foodMarketingMultiplier">foodMarketingMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="foodMarketingMultiplier"
                                    value={currentEntity.foodMarketingMultiplier}
                                    onChange={this.onChangeFoodMarketingMultiplier}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="tradeMarketingMultiplier">tradeMarketingMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tradeMarketingMultiplier"
                                    value={currentEntity.tradeMarketingMultiplier}
                                    onChange={this.onChangeTradeMarketingMultiplier}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="handicraftMarketingMultiplier">handicraftMarketingMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="handicraftMarketingMultiplier"
                                    value={currentEntity.handicraftMarketingMultiplier}
                                    onChange={this.onChangeHandicraftMarketingMultiplier}
                                />
                            </div>

                        </form>

                        <button
                            className="badge badge-light mr-2"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a IncomeAffectableMode...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { IncomeAffectableMode };
