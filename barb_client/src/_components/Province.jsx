import React, { Component } from "react";
import { provinceService, cityService, stateService, legalSystemService, climatService, seasonService, landscapeService } from '@/_services';

class Province extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeAutonomy = this.onChangeAutonomy.bind(this);
        this.onChangeRebellionPotential = this.onChangeRebellionPotential.bind(this);
        this.onChangeVillagePopularity = this.onChangeVillagePopularity.bind(this);
        this.onChangePopularityGrowth = this.onChangePopularityGrowth.bind(this);
        this.onChangeTreasury = this.onChangeTreasury.bind(this);
        this.onChangeTaxInto = this.onChangeTaxInto.bind(this);
        this.onChangeTaxAbove = this.onChangeTaxAbove.bind(this);
        this.onChangeClimatId = this.onChangeClimatId.bind(this);
        this.onChangeSeasonId = this.onChangeSeasonId.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getClimatId = this.getClimatId.bind(this);
        this.getSeasonId = this.getSeasonId.bind(this);
        this.onChangeSeasonPage = this.onChangeSeasonPage.bind(this);
        this.OnClickAddClimat = this.OnClickAddClimat.bind(this);
        this.OnClickAddSeason = this.OnClickAddSeason.bind(this);
        this.onChangeCityId = this.onChangeCityId.bind(this);
        this.getCityId = this.getCityId.bind(this);
        this.onChangeCityPage = this.onChangeCityPage.bind(this);
        this.OnClickAddCity = this.OnClickAddCity.bind(this);
        this.onChangeStateId = this.onChangeStateId.bind(this);
        this.getStateId = this.getStateId.bind(this);
        this.onChangeStatePage = this.onChangeStatePage.bind(this);
        this.OnClickAddState = this.OnClickAddState.bind(this);
        this.onChangeLegalSystemId = this.onChangeLegalSystemId.bind(this);
        this.getLegalSystemId = this.getLegalSystemId.bind(this);
        this.onChangeLegalSystemPage = this.onChangeLegalSystemPage.bind(this);
        this.OnClickAddLegalSystem = this.OnClickAddLegalSystem.bind(this);
        this.onChangeLandscapeId = this.onChangeLandscapeId.bind(this);
        this.getLandscapeId = this.getLandscapeId.bind(this);
        this.onChangeLandscapePage = this.onChangeLandscapePage.bind(this);
        this.OnClickAddLandscape = this.OnClickAddLandscape.bind(this);


        this.state = {
            currentEntity: {
                id: null,
                name: "",
                autonomy: 0,
                rebellionPotential: 0,
                villagePopularity: 0,
                popularityGrowth: 0,
                treasury: 0,
                taxInto: 0,
                taxAbove: 0,
                cityId: 0,
                stateId: 0,
                legalSystemId: 0,
                climatId: 0,
                seasonId: 0,
                landscapeId: 0
            },

            cityIds: [],
            cityIdsTotalPages: 1,
            cityIdsPages: [],
            currentCityPage: 0,

            stateIds: [],
            stateIdsTotalPages: 1,
            stateIdsPages: [],
            currentStatePage: 0,

            legalSystemIds: [],
            legalSystemIdsTotalPages: 1,
            legalSystemIdsPages: [],
            currentLegalSystemPage: 0,

            climatIds: [],
            climatIdsTotalPages: 1,
            climatIdsPages: [],
            currentClimatPage: 0,

            seasonIds: [],
            seasonIdsTotalPages: 1,
            seasonIdsPages: [],
            currentSeasonPage: 0,

            landscapeIds: [],
            landscapeIdsTotalPages: 1,
            landscapeIdsPages: [],
            currentLandscapePage: 0,

            message: "",
        };
    }

    componentDidMount() {
        this.getCityId(this.state.currentCityPage);
        this.getStateId(this.state.currentStatePage);
        this.getLegalSystemId(this.state.currentLegalSystemPage);
        this.getClimatId(this.state.currentClimatPage);
        this.getSeasonId(this.state.currentSeasonPage);
        this.getLandscapeId(this.state.currentLandscapePage);
        if (localStorage.getItem("current_province_state") != null) {
            console.log(localStorage.getItem("current_province_state"));
            const new_state = JSON.parse(localStorage.getItem("current_province_state"));
            this.setState({
                currentEntity: new_state.currentEntity,

                cityIds: new_state.cityIds,
                cityIdsTotalPages: new_state.cityIdsTotalPages,
                cityIdsPages: new_state.cityIdsPages,
                currentCityPage: new_state.currentCityPage,

                stateIds: new_state.stateIds,
                stateIdsTotalPages: new_state.stateIdsTotalPages,
                stateIdsPages: new_state.stateIdsPages,
                currentStatePage: new_state.currentStatePage,

                legalSystemIds: new_state.legalSystemIds,
                legalSystemIdsTotalPages: new_state.legalSystemIdsTotalPages,
                legalSystemIdsPages: new_state.legalSystemIdsPages,
                currentLegalSystemPage: new_state.currentLegalSystemPage,

                climatIds: new_state.climatIds,
                climatIdsTotalPages: new_state.climatIdsTotalPages,
                climatIdsPages: new_state.climatIdsPages,
                currentClimatPage: new_state.currentClimatPage,

                seasonIds: new_state.seasonIds,
                seasonIdsTotalPages: new_state.seasonIdsTotalPages,
                seasonIdsPages: new_state.seasonIdsPages,
                currentSeasonPage: new_state.currentSeasonPage,

                landscapeIds: new_state.landscapeIds,
                landscapeIdsTotalPages: new_state.landscapeIdsTotalPages,
                landscapeIdsPages: new_state.landscapeIdsPages,
                currentLandscapePage: new_state.currentLandscapePage,

                message: new_state.message
            });
            localStorage.removeItem('current_province_state');
        } else this.getEntity(this.props.match.params.id);
        
    }

    onChangeLandscapeId(e) {
        const landscapeId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    landscapeId: landscapeId,
                },
            };
        });
    }
    onChangeLandscapePage(e) {
        const page = e.target.value;
        this.getLandscapeId(page);
        this.setState(function (prevState) {
            return {
                currentLandscapePage: page,
            };
        });
    }

    onChangeLegalSystemId(e) {
        const legalSystemId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    legalSystemId: legalSystemId,
                },
            };
        });
    }
    onChangeLegalSystemPage(e) {
        const page = e.target.value;
        this.getLegalSystemId(page);
        this.setState(function (prevState) {
            return {
                currentLegalSystemPage: page,
            };
        });
    }

    onChangeStateId(e) {
        const stateId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    stateId: stateId,
                },
            };
        });
    }
    onChangeStatePage(e) {
        const page = e.target.value;
        this.getStateId(page);
        this.setState(function (prevState) {
            return {
                currentStatePage: page,
            };
        });
    }

    onChangeCityId(e) {
        const cityId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    cityId: cityId,
                },
            };
        });
    }
    onChangeCityPage(e) {
        const page = e.target.value;
        this.getCityId(page);
        this.setState(function (prevState) {
            return {
                currentCityPage: page,
            };
        });
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }
    onChangeAutonomy(e) {
        const autonomy = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    autonomy: autonomy,
                },
            };
        });
    }

    onChangeRebellionPotential(e) {
        const rebellionPotential = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    rebellionPotential: rebellionPotential,
                },
            };
        });
    }

    onChangeVillagePopularity(e) {
        const villagePopularity = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    villagePopularity: villagePopularity,
                },
            };
        });
    }

    onChangePopularityGrowth(e) {
        const popularityGrowth = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    popularityGrowth: popularityGrowth,
                },
            };
        });
    }

    onChangeTreasury(e) {
        const treasury = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    treasury: treasury,
                },
            };
        });
    }

    onChangeTaxInto(e) {
        const taxInto = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    taxInto: taxInto,
                },
            };
        });
    }

    onChangeTaxAbove(e) {
        const taxAbove = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    taxAbove: taxAbove,
                },
            };
        });
    }

    onChangeClimatId(e) {
        const climatId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    climatId: climatId,
                },
            };
        });
    }
    onChangeClimatPage(e) {
        const page = e.target.value;
        this.getClimatId(page);
        this.setState(function (prevState) {
            return {
                currentClimatPage: page,
            };
        });
    }

    onChangeSeasonId(e) {
        const seasonId = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    seasonId: seasonId,
                },
            };
        });
    }
    onChangeSeasonPage(e) {
        const page = e.target.value;
        this.getSeasonId(page);
        this.setState(function (prevState) {
            return {
                currentSeasonPage: page,
            };
        });
    }


    getEntity(id) {
        provinceService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const cityId = this.state.currentEntity.cityId.id;
                    const stateId = this.state.currentEntity.stateId.id;
                    const legalSystemId = this.state.currentEntity.legalSystemId.id;
                    const climatId = this.state.currentEntity.climatId.id;
                    const seasonId = this.state.currentEntity.seasonId.id;
                    const landscapeId = this.state.currentEntity.landscapeId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            cityId: cityId,
                            stateId: stateId,
                            legalSystemId: legalSystemId,
                            climatId: climatId,
                            seasonId: seasonId,
                            landscapeId: landscapeId

                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getCityId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        cityService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    cityIds: content,
                    cityIdsTotalPages: totalPages,
                    cityIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getStateId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        stateService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    stateIds: content,
                    stateIdsTotalPages: totalPages,
                    stateIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getLegalSystemId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        legalSystemService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    legalSystemIds: content,
                    legalSystemIdsTotalPages: totalPages,
                    legalSystemIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getSeasonId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        seasonService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    seasonIds: content,
                    seasonIdsTotalPages: totalPages,
                    seasonIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getClimatId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        climatService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    climatIds: content,
                    climatIdsTotalPages: totalPages,
                    climatIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }
    getLandscapeId(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        landscapeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    landscapeIds: content,
                    landscapeIdsTotalPages: totalPages,
                    landscapeIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        provinceService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The province was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        provinceService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/province");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddCity() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/city');
    }
    OnClickAddState() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/state');
    }
    OnClickAddLegalSystem() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/legalSystem');
    }
    OnClickAddClimat() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/climat');
    }

    OnClickAddSeason() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/season');
    }

    OnClickAddLandscape() {
        localStorage.setItem('current_province_state', JSON.stringify(this.state));
        this.props.history.push('/add/landscape');
    }

    render() {
        const { currentEntity, climatIds, climatIdsPages, currentClimatPage,
            seasonIds, seasonIdsPages, currentSeasonPage,
            cityIds, cityIdsPages, currentCityPage,
            stateIds, stateIdsPages, currentStatePage,
            legalSystemIds, legalSystemIdsPages, currentLegalSystemPage,
            landscapeIds, landscapeIdsPages, currentLandscapePage,
        } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Province</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="autonomy">autonomy</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="autonomy"
                                    value={currentEntity.autonomy}
                                    onChange={this.onChangeAutonomy}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="rebellionPotential">rebellionPotential</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="rebellionPotential"
                                    value={currentEntity.rebellionPotential}
                                    onChange={this.onChangeRebellionPotential}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="villagePopularity">villagePopularity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="villagePopularity"
                                    value={currentEntity.villagePopularity}
                                    onChange={this.onChangeVillagePopularity}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="popularityGrowth">popularityGrowth</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="popularityGrowth"
                                    value={currentEntity.popularityGrowth}
                                    onChange={this.onChangePopularityGrowth}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="treasury">treasury</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="treasury"
                                    value={currentEntity.treasury}
                                    onChange={this.onChangeTreasury}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="taxInto">taxInto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="taxInto"
                                    value={currentEntity.taxInto}
                                    onChange={this.onChangeTaxInto}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="taxAbove">taxAbove</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="taxAbove"
                                    value={currentEntity.taxAbove}
                                    onChange={this.onChangeTaxAbove}
                                />
                            </div>

                           

                            <div className="mt-3 mb-1">
                                {"CityId: "}
                                <select onChange={this.onChangeCityId} value={currentEntity.cityId}>
                                    {cityIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeCityPage} value={currentCityPage}>
                                    {cityIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddCity}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"StateId: "}
                                <select onChange={this.onChangeStateId} value={currentEntity.stateId}>
                                    {stateIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeStatePage} value={currentStatePage}>
                                    {stateIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddState}
                                >
                                    Add
            </button>
                            </div>


                            <div className="mt-3 mb-1">
                                {"LegalSystemId: "}
                                <select onChange={this.onChangeLegalSystemId} value={currentEntity.legalSystemId}>
                                    {legalSystemIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeLegalSystemPage} value={currentLegalSystemPage}>
                                    {legalSystemIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddLegalSystem}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"ClimatId: "}
                                <select onChange={this.onChangeClimatId} value={currentEntity.climatId}>
                                    {climatIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeClimatPage} value={currentClimatPage}>
                                    {climatIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddClimat}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"SeasonId: "}
                                <select onChange={this.onChangeSeasonId} value={currentEntity.seasonId}>
                                    {seasonIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeSeasonPage} value={currentSeasonPage}>
                                    {seasonIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddSeason}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"LandscapeId: "}
                                <select onChange={this.onChangeLandscapeId} value={currentEntity.landscapeId}>
                                    {landscapeIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeLandscapePage} value={currentLandscapePage}>
                                    {landscapeIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddLandscape}
                                >
                                    Add
            </button>
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Province...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Province };
