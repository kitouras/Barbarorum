import React, { Component } from "react";
import { resourceService } from '@/_services';

class Resource extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePopulationGrowthMultiplier = this.onChangePopulationGrowthMultiplier.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                populationGrowthMultiplier: 0,
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }


    onChangePopulationGrowthMultiplier(e) {
        const populationGrowthMultiplier = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                populationGrowthMultiplier: populationGrowthMultiplier,
            },
        }));
    }

    getEntity(id) {
        resourceService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        resourceService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The resource was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        resourceService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/resource");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Resource</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="populationGrowthMultiplier">PopulationGrowthMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="populationGrowthMultiplier"
                                    value={currentEntity.populationGrowthMultiplier}
                                    onChange={this.onChangePopulationGrowthMultiplier}
                                />
                            </div>
                        </form>

                        <button
                            className="badge badge-light mr-2"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Resource...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Resource };
