import React, { Component } from "react";
import { provinceReligionsService } from '@/_services';

class ProvinceReligions extends Component {
    constructor(props) {
        super(props);
        this.onChangePercentage = this.onChangePercentage.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                provinceId: null,
                religionId: null,
                percentage: 0,
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.provinceId, this.props.match.params.religionId);
    }


    onChangePercentage(e) {
        const percentage = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    percentage: percentage,
                },
            };
        });
    }
    

    getEntity(provinceId, religionId) {
        provinceReligionsService.getEntity(provinceId, religionId)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const provinceId = this.state.currentEntity.provinceId.id;
                    const religionId = this.state.currentEntity.religionId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            provinceId: provinceId,
                            religionId: religionId
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    updateEntity() {
        provinceReligionsService
            .updateEntity(this.state.currentEntity.provinceId, this.state.currentEntity.religionId, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The provinceReligions was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        provinceReligionsService
            .deleteEntity(this.state.currentEntity.provinceId, this.state.currentEntity.religionId)
            .then(() => {
                this.props.history.push("/provinceReligions");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>ProvinceReligions</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="percentage">percentage</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="percentage"
                                    value={currentEntity.percentage}
                                    onChange={this.onChangePercentage}
                                />
                            </div>
                            

                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a ProvinceReligions...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { ProvinceReligions };
