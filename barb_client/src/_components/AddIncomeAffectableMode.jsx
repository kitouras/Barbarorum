import React, { Component } from "react";
import { incomeAffectableModeService } from '@/_services';

export default class AddIncomeAffectableMode extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeVillageTaxMultiplier = this.onChangeVillageTaxMultiplier.bind(this);
        this.onChangeCityTaxMultiplier = this.onChangeCityTaxMultiplier.bind(this);
        this.onChangeFoodMultiplier = this.onChangeFoodMultiplier.bind(this);
        this.onChangeTradeMultiplier = this.onChangeTradeMultiplier.bind(this);
        this.onChangeHandicraftMultiplier = this.onChangeHandicraftMultiplier.bind(this);
        this.onChangeFoodMarketingMultiplier = this.onChangeFoodMarketingMultiplier.bind(this);
        this.onChangeTradeMarketingMultiplier = this.onChangeTradeMarketingMultiplier.bind(this);
        this.onChangeHandicraftMarketingMultiplier = this.onChangeHandicraftMarketingMultiplier.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            id: null,
            name: "",
            villageTaxMultiplier: 0,
            cityTaxMultiplier: 0,
            foodMultiplier: 0,
            tradeMultiplier: 0,
            handicraftMultiplier: 0,
            foodMarketingMultiplier: 0,
            tradeMarketingMultiplier: 0,
            handicraftMarketingMultiplier: 0,

            submitted: false
        };
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeVillageTaxMultiplier(e) {
        this.setState({
            villageTaxMultiplier: e.target.value
        });
    }

    onChangeCityTaxMultiplier(e) {
        this.setState({
            cityTaxMultiplier: e.target.value
        });
    }
    onChangeFoodMultiplier(e) {
        this.setState({
            foodMultiplier: e.target.value
        });
    }
    onChangeTradeMultiplier(e) {
        this.setState({
            tradeMultiplier: e.target.value
        });
    }
    onChangeHandicraftMultiplier(e) {
        this.setState({
            handicraftMultiplier: e.target.value
        });
    }
    onChangeFoodMarketingMultiplier(e) {
        this.setState({
            foodMarketingMultiplier: e.target.value
        });
    }
    onChangeTradeMarketingMultiplier(e) {
        this.setState({
            tradeMarketingMultiplier: e.target.value
        });
    }
    onChangeHandicraftMarketingMultiplier(e) {
        this.setState({
            handicraftMarketingMultiplier: e.target.value
        });
    }


    saveEntity() {
        var data = {
            name: this.state.name,
            villageTaxMultiplier: this.state.villageTaxMultiplier,
            cityTaxMultiplier: this.state.cityTaxMultiplier,
            foodMultiplier: this.state.foodMultiplier,
            tradeMultiplier: this.state.tradeMultiplier,
            handicraftMultiplier: this.state.handicraftMultiplier,
            foodMarketingMultiplier: this.state.foodMarketingMultiplier,
            tradeMarketingMultiplier: this.state.tradeMarketingMultiplier,
            handicraftMarketingMultiplier: this.state.handicraftMarketingMultiplier
        };

        incomeAffectableModeService.createEntity(data)
            .then(response => {
                this.setState({
                    name: response.name,
                    villageTaxMultiplier: response.villageTaxMultiplier,
                    cityTaxMultiplier: response.cityTaxMultiplier,
                    foodMultiplier: response.foodMultiplier,
                    tradeMultiplier: response.tradeMultiplier,
                    handicraftMultiplier: response.handicraftMultiplier,
                    foodMarketingMultiplier: response.foodMarketingMultiplier,
                    tradeMarketingMultiplier: response.tradeMarketingMultiplier,
                    handicraftMarketingMultiplier: response.handicraftMarketingMultiplier,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            name: "",
            villageTaxMultiplier: 0,
            cityTaxMultiplier: 0,
            foodMultiplier: 0,
            tradeMultiplier: 0,
            handicraftMultiplier: 0,
            foodMarketingMultiplier: 0,
            tradeMarketingMultiplier: 0,
            handicraftMarketingMultiplier: 0,

            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            Add
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                    name="name"
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="villageTaxMultiplier">villageTaxMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="villageTaxMultiplier"
                                    required
                                    value={this.state.villageTaxMultiplier}
                                    onChange={this.onChangeVillageTaxMultiplier}
                                    name="villageTaxMultiplier"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="cityTaxMultiplier">cityTaxMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="cityTaxMultiplier"
                                    required
                                    value={this.state.cityTaxMultiplier}
                                    onChange={this.onChangeCityTaxMultiplier}
                                    name="cityTaxMultiplier"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="foodMultiplier">foodMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="foodMultiplier"
                                    required
                                    value={this.state.foodMultiplier}
                                    onChange={this.onChangeFoodMultiplier}
                                    name="foodMultiplier"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="tradeMultiplier">tradeMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tradeMultiplier"
                                    required
                                    value={this.state.tradeMultiplier}
                                    onChange={this.onChangeTradeMultiplier}
                                    name="tradeMultiplier"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="handicraftMultiplier">handicraftMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="handicraftMultiplier"
                                    required
                                    value={this.state.handicraftMultiplier}
                                    onChange={this.onChangeHandicraftMultiplier}
                                    name="handicraftMultiplier"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="foodMarketingMultiplier">foodMarketingMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="foodMarketingMultiplier"
                                    required
                                    value={this.state.foodMarketingMultiplier}
                                    onChange={this.onChangeFoodMarketingMultiplier}
                                    name="foodMarketingMultiplier"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="tradeMarketingMultiplier">tradeMarketingMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tradeMarketingMultiplier"
                                    required
                                    value={this.state.tradeMarketingMultiplier}
                                    onChange={this.onChangeTradeMarketingMultiplier}
                                    name="tradeMarketingMultiplier"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="handicraftMarketingMultiplier">handicraftMarketingMultiplier</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="handicraftMarketingMultiplier"
                                    required
                                    value={this.state.handicraftMarketingMultiplier}
                                    onChange={this.onChangeHandicraftMarketingMultiplier}
                                    name="handicraftMarketingMultiplier"
                                />
                            </div>


                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddIncomeAffectableMode };