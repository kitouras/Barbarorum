import React, { Component } from "react";
import { provinceCulturesService } from '@/_services';

class ProvinceCultures extends Component {
    constructor(props) {
        super(props);
        this.onChangePercentage = this.onChangePercentage.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                provinceId: null,
                cultureId: null,
                percentage: 0,
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.provinceId, this.props.match.params.cultureId);
    }


    onChangePercentage(e) {
        const percentage = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    percentage: percentage,
                },
            };
        });
    }


    getEntity(provinceId, cultureId) {
        provinceCulturesService.getEntity(provinceId, cultureId)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                this.setState(function (prevState) {
                    const provinceId = this.state.currentEntity.provinceId.id;
                    const cultureId = this.state.currentEntity.cultureId.id;
                    return {
                        currentEntity: {
                            ...prevState.currentEntity,
                            provinceId: provinceId,
                            cultureId: cultureId
                        },
                    };
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    updateEntity() {
        provinceCulturesService
            .updateEntity(this.state.currentEntity.provinceId, this.state.currentEntity.cultureId, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The provinceCultures was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        provinceCulturesService
            .deleteEntity(this.state.currentEntity.provinceId, this.state.currentEntity.cultureId)
            .then(() => {
                this.props.history.push("/provinceCultures");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>ProvinceCultures</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="percentage">percentage</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="percentage"
                                    value={currentEntity.percentage}
                                    onChange={this.onChangePercentage}
                                />
                            </div>


                        </form>

                        <button
                            className="badge badge-light mr-2 mt-3"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2 mt-3"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success mt-3"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a ProvinceCultures...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { ProvinceCultures };
