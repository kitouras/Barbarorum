import React, { Component } from "react";
import { deseaseService } from '@/_services';

class Desease extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeIncidenceRate = this.onChangeIncidenceRate.bind(this);
        this.onChangeMortality = this.onChangeMortality.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                incidenceRate: 0,
                mortality: 0,
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }

    onChangeIncidenceRate(e) {
        const incidenceRate = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                incidenceRate: incidenceRate,
            },
        }));
    }

    onChangeMortality(e) {
        const mortality = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                mortality: mortality,
            },
        }));
    }

    getEntity(id) {
        deseaseService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        deseaseService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The desease was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        deseaseService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/desease");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Desease</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="incidenceRate">Incidence rate</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="incidenceRate"
                                    value={currentEntity.incidenceRate}
                                    onChange={this.onChangeIncidenceRate}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="mortality">Mortality</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="mortality"
                                    value={currentEntity.mortality}
                                    onChange={this.onChangeMortality}
                                />
                            </div>
                        </form>

                        <button
                            className="badge badge-light mr-2"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>

                    
                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Desease...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Desease };
