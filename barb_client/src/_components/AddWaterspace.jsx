import React, { Component } from "react";
import { waterspaceService, climatService, seasonService } from '@/_services';

export default class AddWaterspace extends Component {
    constructor(props) {
        super(props);
        this.onChangeDepth = this.onChangeDepth.bind(this);
        this.onChangeWidth = this.onChangeWidth.bind(this);
        this.onChangeClimatId = this.onChangeClimatId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getClimat = this.getClimat.bind(this);
        this.onChangeClimatPage = this.onChangeClimatPage.bind(this);
        this.OnClickAddClimat = this.OnClickAddClimat.bind(this);
        this.onChangeSeasonId = this.onChangeSeasonId.bind(this);
        this.getSeason = this.getSeason.bind(this);
        this.onChangeSeasonPage = this.onChangeSeasonPage.bind(this);
        this.OnClickAddSeason = this.OnClickAddSeason.bind(this);

        this.state = {
            id: null,
            depth: 0,
            width: 0,
            climatId: 0,
            seasonId: 0,

            climatIds: [],
            climatIdsTotalPages: 1,
            climatIdsPages: [],
            currentClimatPage: 0,

            seasonIds: [],
            seasonIdsTotalPages: 1,
            seasonIdsPages: [],
            currentSeasonPage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getClimat(this.state.currentClimatPage);
        this.getSeason(this.state.currentSeasonPage);
        if (localStorage.getItem("current_waterspace_state") != null) {
            console.log(localStorage.getItem("current_waterspace_state"));
            const new_state = JSON.parse(localStorage.getItem("current_waterspace_state"));
            this.setState({
                depth: new_state.depth,
                width: new_state.width,
                climatId: new_state.climatId,
                seasonId: new_state.seasonId,
                climatIds: new_state.climatIds,
                climatIdsTotalPages: new_state.climatIdsTotalPages,
                climatIdsPages: new_state.climatIdsPages,
                currentClimatPage: new_state.currentClimatPage,

                seasonIds: new_state.seasonIds,
                seasonIdsTotalPages: new_state.seasonIdsTotalPages,
                seasonIdsPages: new_state.seasonIdsPages,
                currentSeasonPage: new_state.currentClimatPage,
                message: new_state.message
            });
            localStorage.removeItem('current_waterspace_state');
        }
        
    }

    onChangeDepth(e) {
        this.setState({
            depth: e.target.value
        });
    }

    onChangeWidth(e) {
        this.setState({
            width: e.target.value
        });
    }
    onChangeClimatId(e) {
        this.setState({
            climatId: e.target.value
        });
    }
    onChangeClimatPage(e) {
        const page = e.target.value;
        this.getClimat(page);
        this.setState({
            currentClimatPage: page
        });
    }

    onChangeSeasonId(e) {
        this.setState({
            seasonId: e.target.value
        });
    }
    onChangeSeasonPage(e) {
        const page = e.target.value;
        this.getSeason(page);
        this.setState({
            currentSeasonPage: page
        });
    }

    getClimat(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        climatService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    climatIds: content,
                    climatIdsTotalPages: totalPages,
                    climatIdsPages: pages,
                    climatId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getSeason(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        seasonService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    seasonIds: content,
                    seasonIdsTotalPages: totalPages,
                    seasonIdsPages: pages,
                    seasonId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {
            depth: this.state.depth,
            width: this.state.width,
            climatId: this.state.climatId,
            seasonId: this.state.seasonId
        };

        waterspaceService.createEntity(data)
            .then(response => {
                this.setState({
                    depth: response.depth,
                    width: response.width,
                    climatId: response.climatId,
                    seasonId: response.seasonId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            id: null,
            depth: 0,
            width: 0,


            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddClimat() {
        localStorage.setItem('current_waterspace_state', JSON.stringify(this.state));
        this.props.history.push('/add/climat');
    }
    OnClickAddSeason() {
        localStorage.setItem('current_waterspace_state', JSON.stringify(this.state));
        this.props.history.push('/add/season');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="form-group">
                                <label htmlFor="depth">depth</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="depth"
                                    required
                                    value={this.state.depth}
                                    onChange={this.onChangeDepth}
                                    name="depth"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="width">width</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="width"
                                    required
                                    value={this.state.width}
                                    onChange={this.onChangeWidth}
                                    name="width"
                                />
                            </div>

                            <div className="mt-3 mb-1">
                                {"ClimatId: "}
                                <select onChange={this.onChangeClimatId} value={this.state.climatId}>
                                    {this.state.climatIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeClimatPage} value={this.state.currentClimatPage}>
                                    {this.state.climatIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddClimat}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"SeasonId: "}
                                <select onChange={this.onChangeSeasonId} value={this.state.seasonId}>
                                    {this.state.seasonIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeSeasonPage} value={this.state.currentSeasonPage}>
                                    {this.state.seasonIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddSeason}
                                >
                                    Add
            </button>
                            </div>



                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddWaterspace };