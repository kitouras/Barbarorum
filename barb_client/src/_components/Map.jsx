import React, { Component } from "react";
import OlMap from "ol/map";
import OlView from "ol/view";
import Layer from "ol/layer/Layer";
import { composeCssTransform } from "ol/transform";

class Map extends Component {
    constructor(props) {
        super(props);
        this.createSvgContainer = this.createSvgContainer.bind(this);

        this.state = { center: [-150, 40], zoom: 2 };
        this.svgContainer = this.createSvgContainer();
        this.olmap = new OlMap({
            target: null,
            layers: [
                new Layer({
                    render: (frameState) => {
                        var width = 2560;
                        var height = 1280;
                        var svgResolution = 360 / width;
                        var scale = svgResolution / frameState.viewState.resolution;
                        var center = frameState.viewState.center;
                        var size = frameState.size;
                        var cssTransform = composeCssTransform(
                            size[0] / 2,
                            size[1] / 2,
                            scale,
                            scale,
                            frameState.viewState.rotation,
                            -center[0] / svgResolution - width / 2,
                            center[1] / svgResolution - height / 2
                        );
                        this.svgContainer.style.transform = cssTransform;
                        return this.svgContainer;
                    },
                })
            ],
            view: new OlView({
                center: this.state.center,
                extent: [-180, -90, 180, 90],
                projection: 'EPSG:4326',
                zoom: this.state.zoom
            })
        });
    }




    createSvgContainer() {
        var svgContainer = document.createElement('div');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'title1.svg');
        xhr.addEventListener('load', function () {
            var svg = xhr.responseXML.documentElement;
            svgContainer.ownerDocument.importNode(svg);
            svgContainer.appendChild(svg);
        });
        xhr.send();
        var width = 2560;
        var height = 1280;
        svgContainer.style.width = width + 'px';
        svgContainer.style.height = height + 'px';
        svgContainer.style.transformOrigin = 'top left';
        svgContainer.className = 'svg-layer';
        return svgContainer;
    }
    componentDidMount() {
        this.olmap.setTarget("map");
        this.olmap.on('click', (evt) => {
            console.log(evt.originalEvent);
            console.log(evt.originalEvent.path[0].id);
        });
    }
   
    render() {
        return (
            <div id="map_page">
                <div id="map" style={{ width: "100%", height: "500px"}}>
                    <h4>Map</h4>
                </div>
             </div>
        );
    }
}

export { Map };