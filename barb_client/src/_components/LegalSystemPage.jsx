import React, { Component } from "react";
import { Link } from "react-router-dom";
import { legalSystemService, incomeAffectableModeService } from '@/_services';
import Pagination from "@material-ui/lab/Pagination";

class LegalSystemPage extends Component {
    constructor(props) {
        super(props);
        this.retrieveEntities = this.retrieveEntities.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveEntities = this.setActiveEntities.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handlePageSizeChange = this.handlePageSizeChange.bind(this);
        this.onChangeGroupName = this.onChangeGroupName.bind(this);
        this.onChangeIsTaxFixed = this.onChangeIsTaxFixed.bind(this);
        this.onChangeTaxAboveMultiplier = this.onChangeTaxAboveMultiplier.bind(this);
        this.onChangeAutonomyFactor = this.onChangeAutonomyFactor.bind(this);
        this.onChangeIncomeModsId = this.onChangeIncomeModsId.bind(this);
        this.getIncomeMods = this.getIncomeMods.bind(this);
        this.onChangePage = this.onChangePage.bind(this);

        this.state = {
            entities: [],
            currentEntities: null,
            currentIndex: -1,

            id: null,
            groupName: null,
            taxFixed: null,
            taxAboveMultiplier: null,
            autonomyFactor: null,
            incomeModsId: null,

            taxFixedSelectable: ['true', 'false'],

            sortBy: 'id',
            sortBySelectable: ['id', 'groupName', 'taxFixed', 'taxAboveMultiplier', 'autonomyFactor', 'incomeModsId'],
            sortDir: 'ASC',
            sortDirSelectable: ['ASC', 'DESC'],

            incomeModsIds: [],
            incomeModsIdsTotalPages: 1,
            incomeModsIdsPages: [],
            currentPage: 0,

            page: 1,
            count: 0,
            pageSize: 6,
        };

        this.pageSizes = [3, 6, 9];
    }

    componentDidMount() {
        this.retrieveEntities();
        this.getIncomeMods();
    }

    onChangeGroupName(e) {
        this.setState({
            groupName: e.target.value
        });
    }

    onChangeIsTaxFixed(e) {
        this.setState({
            taxFixed: e.target.value
        });
    }
    onChangeTaxAboveMultiplier(e) {
        
        this.setState({
            taxAboveMultiplier: e.target.value
        });
    }
    onChangeAutonomyFactor(e) {
        
        this.setState({
            autonomyFactor: e.target.value
        });
    }
    onChangeIncomeModsId(e) {

        this.setState({
            incomeModsId: e.target.value
        });
    }
    onChangePage(e) {
        const page = e.target.value;
        this.getIncomeMods(page);
        this.setState({
            currentPage: page
        });
    }

    getIncomeMods(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        incomeAffectableModeService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    incomeModsIds: content,
                    incomeModsIdsTotalPages: totalPages,
                    incomeModsIdsPages: pages
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    refreshList() {
        this.retrieveEntities();
        this.setState({
            currentEntities: null,
            currentIndex: -1
        });
    }

    setActiveEntities(entities, index) {
        this.setState({
            currentEntities: entities,
            currentIndex: index
        });
    }

    getRequestParams(searchName, page, pageSize, sortBy, sortDir, id, groupName, taxFixed, taxAboveMultiplier, autonomyFactor,
        incomeModsId) {
        let params = {};


        if (page) {
            params["page"] = page - 1;
        }

        if (pageSize) {
            params["size"] = pageSize;
        }
        params["sortBy"] = sortBy;
        params["sortDir"] = sortDir;
        params["id"] = id;
        params["groupName"] = groupName;
        params["taxFixed"] = taxFixed;
        params["taxAboveMultiplier"] = taxAboveMultiplier;
        params["autonomyFactor"] = autonomyFactor;
        params["incomeModsId"] = incomeModsId;

        return params;
    }

    retrieveEntities() {
        const { searchName, page, pageSize, sortBy, sortDir, id, groupName, taxFixed, taxAboveMultiplier, autonomyFactor,
            incomeModsId } = this.state;
        const params = this.getRequestParams(searchName, page, pageSize, sortBy, sortDir, id, groupName, taxFixed, taxAboveMultiplier, autonomyFactor,
            incomeModsId);

        legalSystemService.getEntities(params)
            .then((response) => {

                const content = response.content;
                const totalPages = response.totalPages;

                this.setState({
                    entities: content,
                    count: totalPages,
                });
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }


    handlePageChange(event, value) {
        this.setState(
            {
                page: value,
            },
            () => {
                this.retrieveEntities();
            }
        );
    }

    handlePageSizeChange(event) {
        this.setState(
            {
                pageSize: event.target.value,
                page: 1
            },
            () => {
                this.retrieveEntities();
            }
        );
    }


    render() {
        const {
            searchName,
            entities,
            currentEntities,
            currentIndex,
            page,
            count,
            pageSize,
        } = this.state;

        return (
            <div className="list row">
                <div className="col-md-8">
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Search by title"
                            value={searchName}
                            onChange={this.onChangeSearchName}
                        />
                        <div className="input-group-append">
                            <button
                                className="btn btn-outline-secondary"
                                type="button"
                                onClick={this.retrieveEntities}
                            >
                                Search
              </button>
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <h4>LegalSystem List:</h4>

                    <div className="mt-3">
                        {"Items per Page: "}
                        <select onChange={this.handlePageSizeChange} value={pageSize}>
                            {this.pageSizes.map((size) => (
                                <option key={size} value={size}>
                                    {size}
                                </option>
                            ))}
                        </select>

                        <Pagination
                            className="my-3"
                            count={count}
                            page={page}
                            siblingCount={1}
                            boundaryCount={1}
                            variant="outlined"
                            shape="rounded"
                            onChange={this.handlePageChange}
                        />
                    </div>

                    <ul className="list-group">
                        {entities &&
                            entities.map((entity, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActiveEntities(entity, index)}
                                    key={index}
                                >
                                    {entity.id}
                                </li>
                            ))}
                    </ul>

                    <Link
                        to={"/add/legalSystem"}
                        className="m-2 btn btn-light"
                    >
                        Add
              </Link>

                    <button
                        className="m-4 btn btn-sm btn-danger"
                        onClick={this.retrieveEntities}
                    >
                        Filter
          </button>
                    <div className="form-group">
                        <label htmlFor="groupName">groupName</label>
                        <input
                            type="text"
                            className="form-control"
                            id="groupName"
                            required
                            value={this.state.groupName}
                            onChange={this.onChangeGroupName}
                            name="groupName"
                        />
                    </div>
                    <div className="form-group">
                        {"TaxFixed: "}
                        <select onChange={this.onChangeIsTaxFixed} value={this.state.taxFixed}>
                            {this.state.taxFixedSelectable.map((entity) => (
                                <option key={entity} value={entity}>
                                    {entity}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="taxAboveMultiplier">taxAboveMultiplier</label>
                        <input
                            type="text"
                            className="form-control"
                            id="taxAboveMultiplier"
                            required
                            value={this.state.taxAboveMultiplier}
                            onChange={this.onChangeTaxAboveMultiplier}
                            name="taxAboveMultiplier"
                        />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="autonomyFactor">autonomyFactor</label>
                        <input
                            type="text"
                            className="form-control"
                            id="autonomyFactor"
                            required
                            value={this.state.autonomyFactor}
                            onChange={this.onChangeAutonomyFactor}
                            name="autonomyFactor"
                        />
                    </div>
                    

                    <div className="mt-3 mb-1">
                        {"IncomeModsId: "}
                        <select onChange={this.onChangeIncomeModsId} value={this.state.incomeModsId}>
                            {this.state.incomeModsIds.map((entity) => (
                                <option key={entity} value={entity}>
                                    {entity}
                                </option>
                            ))}
                        </select>
                        {" Page: "}
                        <select onChange={this.onChangePage} value={this.state.currentPage}>
                            {this.state.incomeModsIdsPages.map((entity) => (
                                <option key={entity} value={entity}>
                                    {entity}
                                </option>
                            ))}
                        </select>
                        </div>



                </div>
                <div className="col-md-6">
                    {currentEntities ? (
                        <div>
                            <h4>LegalSystem</h4>
                            <div>
                                <label>
                                    <strong>groupName:</strong>
                                </label>{" "}
                                {currentEntities.groupName}
                            </div>
                            <div>
                                <label>
                                    <strong>taxFixed:</strong>
                                </label>{" "}
                                {JSON.stringify(currentEntities.taxFixed)}
                            </div>
                            <div>
                                <label>
                                    <strong>taxAboveMultiplier:</strong>
                                </label>{" "}
                                {currentEntities.taxAboveMultiplier}
                            </div>
                            <div>
                                <label>
                                    <strong>autonomyFactor:</strong>
                                </label>{" "}
                                {currentEntities.autonomyFactor}
                            </div>
                            <div>
                                <label>
                                    <strong>incomeModsId:</strong>
                                </label>{" "}
                                {currentEntities.incomeModsId.id}
                            </div>
                            <Link
                                to={"/edit/legalSystem/" + currentEntities.id}
                                className="badge badge-warning"
                            >
                                Edit
              </Link>
                        </div>
                    ) : (
                            <div>
                                <br />
                                <p>Please click on a LegalSystem...</p>
                            </div>
                        )}

                </div>
            </div>
        );
    }
}

export { LegalSystemPage };