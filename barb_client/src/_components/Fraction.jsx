import React, { Component } from "react";
import { fractionService } from '@/_services';

class Fraction extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeRebellionTendency = this.onChangeRebellionTendency.bind(this);
        this.getEntity = this.getEntity.bind(this);
        this.updateEntity = this.updateEntity.bind(this);
        this.removeEntity = this.removeEntity.bind(this);
        this.backToList = this.backToList.bind(this);

        this.state = {
            currentEntity: {
                id: null,
                name: "",
                rebellionTendency: 0,
            },
            message: "",
        };
    }

    componentDidMount() {
        this.getEntity(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEntity: {
                    ...prevState.currentEntity,
                    name: name,
                },
            };
        });
    }


    onChangeRebellionTendency(e) {
        const rebellionTendency = e.target.value;

        this.setState((prevState) => ({
            currentEntity: {
                ...prevState.currentEntity,
                rebellionTendency: rebellionTendency,
            },
        }));
    }

    getEntity(id) {
        fractionService.getEntity(id)
            .then((response) => {
                this.setState({
                    currentEntity: response,
                });
                console.log(response);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateEntity() {
        fractionService
            .updateEntity(this.state.currentEntity.id, this.state.currentEntity)
            .then((reponse) => {
                console.log(reponse);

                this.setState({ message: "The fraction was updated successfully!" });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    removeEntity() {
        fractionService
            .deleteEntity(this.state.currentEntity.id)
            .then(() => {
                this.props.history.push("/fraction");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    backToList() {
        this.props.history.goBack();
    }

    render() {
        const { currentEntity } = this.state;

        return (
            <div>
                {currentEntity ? (
                    <div className="edit-form">
                        <h4>Fraction</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentEntity.name}
                                    onChange={this.onChangeName}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="rebellionTendency">RebellionTendency</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="rebellionTendency"
                                    value={currentEntity.rebellionTendency}
                                    onChange={this.onChangeRebellionTendency}
                                />
                            </div>
                        </form>

                        <button
                            className="badge badge-light mr-2"
                            onClick={this.backToList}
                        >
                            Back
            </button>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.removeEntity}
                        >
                            Delete
            </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEntity}
                        >
                            Update
            </button>


                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Please click on a Fraction...</p>
                        </div>
                    )}
            </div>
        );
    }
}

export { Fraction };
