import React, { Component } from "react";
import { provinceFractionsService, provinceService, fractionService } from '@/_services';

export default class AddProvinceFractions extends Component {
    constructor(props) {
        super(props);
        this.onChangeAuthority = this.onChangeAuthority.bind(this);
        this.onChangeIntegrity = this.onChangeIntegrity.bind(this);
        this.onChangeProvinceId = this.onChangeProvinceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getProvince = this.getProvince.bind(this);
        this.onChangeProvincePage = this.onChangeProvincePage.bind(this);
        this.OnClickAddProvince = this.OnClickAddProvince.bind(this);
        this.onChangeFractionId = this.onChangeFractionId.bind(this);
        this.getFraction = this.getFraction.bind(this);
        this.onChangeFractionPage = this.onChangeFractionPage.bind(this);
        this.OnClickAddFraction = this.OnClickAddFraction.bind(this);

        this.state = {
            authority: 0,
            integrity: 0,
            provinceId: 0,
            fractionId: 0,

            provinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentProvincePage: 0,

            fractionIds: [],
            fractionIdsTotalPages: 1,
            fractionIdsPages: [],
            currentFractionPage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getProvince(this.state.currentProvincePage);
        this.getFraction(this.state.currentFractionPage);
        if (localStorage.getItem("current_provinceFractions_state") != null) {
            console.log(localStorage.getItem("current_provinceFractions_state"));
            const new_state = JSON.parse(localStorage.getItem("current_provinceFractions_state"));
            this.setState({
                authority: new_state.authority,
                integrity: new_state.integrity,
                provinceId: new_state.provinceId,
                fractionId: new_state.fractionId,
                provinceIds: new_state.provinceIds,
                provinceIdsTotalPages: new_state.provinceIdsTotalPages,
                provinceIdsPages: new_state.provinceIdsPages,
                currentProvincePage: new_state.currentProvincePage,

                fractionIds: new_state.fractionIds,
                fractionIdsTotalPages: new_state.fractionIdsTotalPages,
                fractionIdsPages: new_state.fractionIdsPages,
                currentFractionPage: new_state.currentProvincePage,
                message: new_state.message
            });
            localStorage.removeItem('current_provinceFractions_state');
        }
        
    }

    onChangeAuthority(e) {
        this.setState({
            authority: e.target.value
        });
    }

    onChangeIntegrity(e) {
        this.setState({
            integrity: e.target.value
        });
    }

    onChangeProvinceId(e) {
        this.setState({
            provinceId: e.target.value
        });
    }
    onChangeProvincePage(e) {
        const page = e.target.value;
        this.getProvince(page);
        this.setState({
            currentProvincePage: page
        });
    }

    onChangeFractionId(e) {
        this.setState({
            fractionId: e.target.value
        });
    }
    onChangeFractionPage(e) {
        const page = e.target.value;
        this.getFraction(page);
        this.setState({
            currentFractionPage: page
        });
    }

    getProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    provinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    provinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getFraction(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        fractionService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    fractionIds: content,
                    fractionIdsTotalPages: totalPages,
                    fractionIdsPages: pages,
                    fractionId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {
            authority: this.state.authority,
            integrity: this.state.integrity,
            provinceId: this.state.provinceId,
            fractionId: this.state.fractionId
        };

        provinceFractionsService.createEntity(data)
            .then(response => {
                this.setState({
                    authority: response.authority,
                    integrity: response.integrity,
                    provinceId: response.provinceId,
                    fractionId: response.fractionId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            authority: 0,
            integrity: 0,



            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddProvince() {
        localStorage.setItem('current_provinceFractions_state', JSON.stringify(this.state));
        this.props.history.push('/add/province');
    }
    OnClickAddFraction() {
        localStorage.setItem('current_provinceFractions_state', JSON.stringify(this.state));
        this.props.history.push('/add/fraction');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"ProvinceId: "}
                                <select onChange={this.onChangeProvinceId} value={this.state.provinceId}>
                                    {this.state.provinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeProvincePage} value={this.state.currentProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"FractionId: "}
                                <select onChange={this.onChangeFractionId} value={this.state.fractionId}>
                                    {this.state.fractionIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeFractionPage} value={this.state.currentFractionPage}>
                                    {this.state.fractionIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddFraction}
                                >
                                    Add
            </button>
                            </div>
                            <div className="form-group">
                                <label htmlFor="authority">authority</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="authority"
                                    required
                                    value={this.state.authority}
                                    onChange={this.onChangeAuthority}
                                    name="authority"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="integrity">integrity</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="integrity"
                                    required
                                    value={this.state.integrity}
                                    onChange={this.onChangeIntegrity}
                                    name="integrity"
                                />
                            </div>
                           





                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddProvinceFractions };