import React, { Component } from "react";
import { provinceCulturesService, provinceService, cultureService } from '@/_services';

export default class AddProvinceCultures extends Component {
    constructor(props) {
        super(props);
        this.onChangePercentage = this.onChangePercentage.bind(this);
        this.onChangeProvinceId = this.onChangeProvinceId.bind(this);
        this.saveEntity = this.saveEntity.bind(this);
        this.newEntity = this.newEntity.bind(this);
        this.backToList = this.backToList.bind(this);
        this.getProvince = this.getProvince.bind(this);
        this.onChangeProvincePage = this.onChangeProvincePage.bind(this);
        this.OnClickAddProvince = this.OnClickAddProvince.bind(this);
        this.onChangeCultureId = this.onChangeCultureId.bind(this);
        this.getCulture = this.getCulture.bind(this);
        this.onChangeCulturePage = this.onChangeCulturePage.bind(this);
        this.OnClickAddCulture = this.OnClickAddCulture.bind(this);

        this.state = {
            percentage: 0,

            provinceIds: [],
            provinceIdsTotalPages: 1,
            provinceIdsPages: [],
            currentProvincePage: 0,

            cultureIds: [],
            cultureIdsTotalPages: 1,
            cultureIdsPages: [],
            currentCulturePage: 0,

            submitted: false
        };
    }

    componentDidMount() {
        this.getProvince(this.state.currentProvincePage);
        this.getCulture(this.state.currentCulturePage);
        if (localStorage.getItem("current_provinceCultures_state") != null) {
            console.log(localStorage.getItem("current_provinceCultures_state"));
            const new_state = JSON.parse(localStorage.getItem("current_provinceCultures_state"));
            this.setState({
                percentage: new_state.percentage,
                provinceId: new_state.provinceId,
                cultureId: new_state.cultureId,
                provinceIds: new_state.provinceIds,
                provinceIdsTotalPages: new_state.provinceIdsTotalPages,
                provinceIdsPages: new_state.provinceIdsPages,
                currentProvincePage: new_state.currentProvincePage,

                cultureIds: new_state.cultureIds,
                cultureIdsTotalPages: new_state.cultureIdsTotalPages,
                cultureIdsPages: new_state.cultureIdsPages,
                currentCulturePage: new_state.currentProvincePage,
                message: new_state.message
            });
            localStorage.removeItem('current_provinceCultures_state');
        }
        
    }

    onChangePercentage(e) {
        this.setState({
            percentage: e.target.value
        });
    }
    onChangeProvinceId(e) {
        this.setState({
            provinceId: e.target.value
        });
    }
    onChangeProvincePage(e) {
        const page = e.target.value;
        this.getProvince(page);
        this.setState({
            currentProvincePage: page
        });
    }

    onChangeCultureId(e) {
        this.setState({
            cultureId: e.target.value
        });
    }
    onChangeCulturePage(e) {
        const page = e.target.value;
        this.getCulture(page);
        this.setState({
            currentCulturePage: page
        });
    }

    getProvince(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        provinceService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    provinceIds: content,
                    provinceIdsTotalPages: totalPages,
                    provinceIdsPages: pages,
                    provinceId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    getCulture(page) {
        let params = {};
        params["page"] = page;
        params["size"] = 4;
        cultureService.getEntities(params)
            .then((response) => {
                const content = [...new Set(response.content.map(item => item.id))];
                const totalPages = response.totalPages;
                var pages = Array.from(Array(totalPages).keys())
                this.setState({
                    cultureIds: content,
                    cultureIdsTotalPages: totalPages,
                    cultureIdsPages: pages,
                    cultureId: content[0]
                });
                console.log(response.content);
            })
            .catch((e) => {
                console.log(e);
            });
    }






    saveEntity() {
        var data = {
            percentage: this.state.percentage,
            provinceId: this.state.provinceId,
            cultureId: this.state.cultureId
        };

        provinceCulturesService.createEntity(data)
            .then(response => {
                this.setState({
                    percentage: response.percentage,
                    provinceId: response.provinceId,
                    cultureId: response.cultureId,

                    submitted: true
                });
                console.log(response);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newEntity() {
        this.setState({
            percentage: 0,



            submitted: false
        });
    }

    backToList() {
        this.props.history.goBack();
    }

    OnClickAddProvince() {
        localStorage.setItem('current_provinceCultures_state', JSON.stringify(this.state));
        this.props.history.push('/add/province');
    }
    OnClickAddCulture() {
        localStorage.setItem('current_provinceCultures_state', JSON.stringify(this.state));
        this.props.history.push('/add/culture');
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button onClick={this.backToList} className="btn btn-light mr-2">
                            Back
            </button>
                        <button className="btn btn-success" onClick={this.newEntity}>
                            New
            </button>
                    </div>
                ) : (
                        <div>
                            <div className="mt-3 mb-1">
                                {"ProvinceId: "}
                                <select onChange={this.onChangeProvinceId} value={this.state.provinceId}>
                                    {this.state.provinceIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeProvincePage} value={this.state.currentProvincePage}>
                                    {this.state.provinceIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddProvince}
                                >
                                    Add
            </button>
                            </div>

                            <div className="mt-3 mb-1">
                                {"CultureId: "}
                                <select onChange={this.onChangeCultureId} value={this.state.cultureId}>
                                    {this.state.cultureIds.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>
                                {" Page: "}
                                <select onChange={this.onChangeCulturePage} value={this.state.currentCulturePage}>
                                    {this.state.cultureIdsPages.map((entity) => (
                                        <option key={entity} value={entity}>
                                            {entity}
                                        </option>
                                    ))}
                                </select>

                                <button
                                    className="badge badge-light ml-2 mt-3"
                                    onClick={this.OnClickAddCulture}
                                >
                                    Add
            </button>
                            </div>
                            <div className="form-group">
                                <label htmlFor="percentage">percentage</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="percentage"
                                    required
                                    value={this.state.percentage}
                                    onChange={this.onChangePercentage}
                                    name="percentage"
                                />
                            </div>






                            <button onClick={this.backToList} className="btn btn-light mr-2">
                                Back
            </button>

                            <button onClick={this.saveEntity} className="btn btn-success">
                                Submit
            </button>

                        </div>
                    )}
            </div>
        );
    }
}

export { AddProvinceCultures };