import React from 'react';
import { Link } from 'react-router-dom';

class TablePage extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <div>
                <h1>Tables:</h1>
                <ul className="list-group">
                    <li><Link
                        to={"/desease"}
                        className="m-3 list-group-item"
                    >
                        Desease
              </Link></li>
                    <li><Link
                        to={"/incomeAffectableMode"}
                        className="m-3 list-group-item"
                    >
                        IncomeAffectableMode
              </Link></li>
                    <li><Link
                        to={"/climat"}
                        className="m-3 list-group-item"
                    >
                        Climat
              </Link></li>
                    <li><Link
                        to={"/season"}
                        className="m-3 list-group-item"
                    >
                        Season
              </Link></li>
                    <li><Link
                        to={"/specialization"}
                        className="m-3 list-group-item"
                    >
                        Specialization
              </Link></li>
                    <li><Link
                        to={"/culture"}
                        className="m-3 list-group-item"
                    >
                        Culture
              </Link></li>
                    <li><Link
                        to={"/religion"}
                        className="m-3 list-group-item"
                    >
                        Religion
              </Link></li>
                    <li><Link
                        to={"/landscape"}
                        className="m-3 list-group-item"
                    >
                        Landscape
              </Link></li>
                    <li><Link
                        to={"/legalSystem"}
                        className="m-3 list-group-item"
                    >
                        LegalSystem
              </Link></li>
                    <li><Link
                        to={"/city"}
                        className="m-3 list-group-item"
                    >
                        City
              </Link></li>
                    <li><Link
                        to={"/fraction"}
                        className="m-3 list-group-item"
                    >
                        Fraction
              </Link></li>
                    <li><Link
                        to={"/relation"}
                        className="m-3 list-group-item"
                    >
                        Relation
              </Link></li>
                    <li><Link
                        to={"/resource"}
                        className="m-3 list-group-item"
                    >
                        Resource
              </Link></li>
                    <li><Link
                        to={"/state"}
                        className="m-3 list-group-item"
                    >
                        State
              </Link></li>
                    <li><Link
                        to={"/waterspace"}
                        className="m-3 list-group-item"
                    >
                        Waterspace
              </Link></li>
                    <li><Link
                        to={"/province"}
                        className="m-3 list-group-item"
                    >
                        Province
              </Link></li>
                    <li><Link
                        to={"/provinceResourcesLifecycle"}
                        className="m-3 list-group-item"
                    >
                        ProvinceResourcesLifecycle
              </Link></li>
                    <li><Link
                        to={"/provinceWaterspaceNeighborhood"}
                        className="m-3 list-group-item"
                    >
                        ProvinceWaterspaceNeighborhood
              </Link></li>
                    <li><Link
                        to={"/provinceReligions"}
                        className="m-3 list-group-item"
                    >
                        ProvinceReligions
              </Link></li>
                    <li><Link
                        to={"/provinceFractions"}
                        className="m-3 list-group-item"
                    >
                        ProvinceFractions
              </Link></li>
                    <li><Link
                        to={"/provinceDeseases"}
                        className="m-3 list-group-item"
                    >
                        ProvinceDeseases
              </Link></li>
                    <li><Link
                        to={"/provinceCultures"}
                        className="m-3 list-group-item"
                    >
                        ProvinceCultures
              </Link></li>
                    <li><Link
                        to={"/neighborhoodOfWaterspaces"}
                        className="m-3 list-group-item"
                    >
                        NeighborhoodOfWaterspaces
              </Link></li>
                    <li><Link
                        to={"/neighborhoodOfProvinces"}
                        className="m-3 list-group-item"
                    >
                        NeighborhoodOfProvinces
              </Link></li>
                </ul>
            </div>
        );
    }
}

export { TablePage };