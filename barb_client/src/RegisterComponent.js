import React from "react"
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Link, useHistory } from "react-router-dom"
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Register() {
    const history = useHistory();

    const [isSuccess, setSuccess] = React.useState(true)

    const [login, setLogin] = React.useState("")
    const [password, setPassword] = React.useState("")
    const classes = useStyles();

    const handleChangeLogin = (e) => {
        setLogin(e.target.value)
    }

    const handleChangePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleRegister = () => {
        getRegisterState("/public/users/register").then(r => {
            let localIsSuccess
            localIsSuccess = r !== false;
            setSuccess(localIsSuccess)
            if (localIsSuccess) {
                history.push("/login")
            }
        })
    }

    function getRegisterState(url) {
        return fetch(url, {
            "method": 'POST',
            "headers": { 'Content-Type': 'application/json' },
            "body": JSON.stringify({
                login, password
            })
        }).then(response => {
            return response.json()
        });
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    �����������
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            inputProps={{ "data-testid": "loginField" }}
                            variant="outlined"
                            required
                            fullWidth
                            id="login"
                            label="�����"
                            autoComplete="login"
                            onChange={handleChangeLogin}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            inputProps={{ "data-testid": "passwordField" }}
                            variant="outlined"
                            required
                            fullWidth
                            label="������"
                            type="password"
                            id="password"
                            onChange={handleChangePassword}
                            autoComplete="current-password"
                        />
                    </Grid>
                </Grid>
                <Button
                    data-testid={"registerButton"}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={handleRegister}
                >
                    ������������������
                </Button>
                <Grid container justify="flex-end">
                    <Grid item>
                        <Link to={"/public/users/login"}>
                            ��� ����������������? ���������������!
                        </Link>
                    </Grid>
                </Grid>
                <div style={{ padding: "10px" }} />
                {isSuccess
                    ?
                    <div />
                    : <Alert severity="error">
                        <AlertTitle>������!</AlertTitle>
                        <strong>����� ������������ ����������!</strong>
                    </Alert>
                }
            </div>
        </Container>
    );
}