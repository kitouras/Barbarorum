import React from 'react';
import { Router, Route, Link } from 'react-router-dom';

import { history } from '@/_helpers';
import { authenticationService } from '@/_services';
import { PrivateRoute } from '@/_components';
import { HomePage } from '@/HomePage';
import { TablePage } from '@/TablePage';
import { LoginPage } from '@/LoginPage';
import { DeseasePage } from '@/_components';
import { Desease } from '@/_components';
import { AddDesease } from '@/_components';
import { IncomeAffectableModePage } from '@/_components'; 
import { IncomeAffectableMode } from '@/_components'; 
import { AddIncomeAffectableMode } from '@/_components';
import { ClimatPage } from '@/_components';
import { Climat } from '@/_components';
import { AddClimat } from '@/_components';
import { SeasonPage } from '@/_components';
import { Season } from '@/_components';
import { AddSeason } from '@/_components';
import { LandscapePage } from '@/_components';
import { Landscape } from '@/_components';
import { AddLandscape } from '@/_components';
import { SpecializationPage } from '@/_components';
import { Specialization } from '@/_components';
import { AddSpecialization } from '@/_components';
import { ReligionPage } from '@/_components';
import { Religion } from '@/_components';
import { AddReligion } from '@/_components';
import { LegalSystemPage } from '@/_components';
import { LegalSystem } from '@/_components';
import { AddLegalSystem } from '@/_components';
import { CulturePage } from '@/_components';
import { Culture } from '@/_components';
import { AddCulture } from '@/_components';
import { CityPage } from '@/_components';
import { City } from '@/_components';
import { AddCity } from '@/_components';
import { FractionPage } from '@/_components';
import { Fraction } from '@/_components';
import { AddFraction } from '@/_components';
import { RelationPage } from '@/_components';
import { Relation } from '@/_components';
import { AddRelation } from '@/_components';
import { ResourcePage } from '@/_components';
import { Resource } from '@/_components';
import { AddResource } from '@/_components';
import { StatePage } from '@/_components';
import { State } from '@/_components';
import { AddState } from '@/_components';
import { WaterspacePage } from '@/_components';
import { Waterspace } from '@/_components';
import { AddWaterspace } from '@/_components';
import { ProvincePage } from '@/_components';
import { Province } from '@/_components';
import { AddProvince } from '@/_components';
import { Map } from '@/_components';
import { ProvinceResourcesLifecyclePage } from '@/_components';
import { ProvinceResourcesLifecycle } from '@/_components';
import { AddProvinceResourcesLifecycle } from '@/_components';
import { ProvinceWaterspaceNeighborhoodPage } from '@/_components';
import { AddProvinceWaterspaceNeighborhood } from '@/_components';
import { ProvinceReligionsPage } from '@/_components';
import { ProvinceReligions } from '@/_components';
import { AddProvinceReligions } from '@/_components';
import { ProvinceDeseasesPage } from '@/_components';
import { ProvinceDeseases } from '@/_components';
import { AddProvinceDeseases } from '@/_components';
import { ProvinceFractionsPage } from '@/_components';
import { ProvinceFractions } from '@/_components';
import { AddProvinceFractions } from '@/_components';
import { ProvinceCulturesPage } from '@/_components';
import { ProvinceCultures } from '@/_components';
import { AddProvinceCultures } from '@/_components';
import { NeighborhoodOfWaterspacesPage } from '@/_components';
import { AddNeighborhoodOfWaterspaces } from '@/_components';
import { NeighborhoodOfProvincesPage } from '@/_components';
import { AddNeighborhoodOfProvinces } from '@/_components';
import { NeighborhoodOfProvinces } from '@/_components';
import { QueriesPage, StateTreasury, ConditionalStateProvince, ProvincesFullPopulation } from '@/QueriesPage';



class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: null
        };
    }

    componentDidMount() {
        authenticationService.currentUser.subscribe(x => this.setState({ currentUser: x }));
    }

    logout() {
        authenticationService.logout();
        history.push('/login');
    }

    render() {
        const { currentUser } = this.state;
        return (
            <Router history={history}>
                <div>
                    {currentUser &&
                        <nav className="navbar navbar-expand navbar-dark bg-dark">
                            <div className="navbar-nav">
                            <Link to="/" className="nav-item nav-link">Home</Link>
                            <Link to="/tables" className="nav-item nav-link">Tables</Link>
                            <Link to="/queries" className="nav-item nav-link">Queries</Link>
                            <Link to="/map" className="nav-item nav-link">Map</Link>
                                <a onClick={this.logout} className="nav-item nav-link">Logout</a>
                            </div>
                        </nav>
                    }
                    <div className="jumbotron">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 offset-md-3">
                                    <PrivateRoute exact path="/" component={HomePage} />
                                    <PrivateRoute exact path="/desease" component={DeseasePage} />
                                    <Route path="/login" component={LoginPage} />
                                    <PrivateRoute path="/edit/desease/:id" component={Desease} />
                                    <PrivateRoute path="/add/desease" component={AddDesease} />
                                    <PrivateRoute path="/tables" component={TablePage} />
                                    <PrivateRoute path="/incomeAffectableMode" component={IncomeAffectableModePage} />
                                    <PrivateRoute path="/add/incomeAffectableMode" component={AddIncomeAffectableMode} />
                                    <PrivateRoute path="/edit/incomeAffectableMode/:id" component={IncomeAffectableMode} />
                                    <PrivateRoute exact path="/climat" component={ClimatPage} />
                                    <PrivateRoute path="/edit/climat/:id" component={Climat} />
                                    <PrivateRoute path="/add/climat" component={AddClimat} />
                                    <PrivateRoute exact path="/season" component={SeasonPage} />
                                    <PrivateRoute path="/edit/season/:id" component={Season} />
                                    <PrivateRoute path="/add/season" component={AddSeason} />
                                    <PrivateRoute exact path="/specialization" component={SpecializationPage} />
                                    <PrivateRoute path="/edit/specialization/:id" component={Specialization} />
                                    <PrivateRoute path="/add/specialization" component={AddSpecialization} />
                                    <PrivateRoute exact path="/culture" component={CulturePage} />
                                    <PrivateRoute path="/edit/culture/:id" component={Culture} />
                                    <PrivateRoute path="/add/culture" component={AddCulture} />
                                    <PrivateRoute exact path="/religion" component={ReligionPage} />
                                    <PrivateRoute path="/edit/religion/:id" component={Religion} />
                                    <PrivateRoute path="/add/religion" component={AddReligion} />
                                    <PrivateRoute exact path="/landscape" component={LandscapePage} />
                                    <PrivateRoute path="/edit/landscape/:id" component={Landscape} />
                                    <PrivateRoute path="/add/landscape" component={AddLandscape} />
                                    <PrivateRoute exact path="/legalSystem" component={LegalSystemPage} />
                                    <PrivateRoute path="/edit/legalSystem/:id" component={LegalSystem} />
                                    <PrivateRoute path="/add/legalSystem" component={AddLegalSystem} />
                                    <PrivateRoute exact path="/city" component={CityPage} />
                                    <PrivateRoute path="/edit/city/:id" component={City} />
                                    <PrivateRoute path="/add/city" component={AddCity} />
                                    <PrivateRoute exact path="/fraction" component={FractionPage} />
                                    <PrivateRoute path="/edit/fraction/:id" component={Fraction} />
                                    <PrivateRoute path="/add/fraction" component={AddFraction} />
                                    <PrivateRoute exact path="/relation" component={RelationPage} />
                                    <PrivateRoute path="/edit/relation/:id" component={Relation} />
                                    <PrivateRoute path="/add/relation" component={AddRelation} />
                                    <PrivateRoute exact path="/resource" component={ResourcePage} />
                                    <PrivateRoute path="/edit/resource/:id" component={Resource} />
                                    <PrivateRoute path="/add/resource" component={AddResource} />
                                    <PrivateRoute exact path="/state" component={StatePage} />
                                    <PrivateRoute path="/edit/state/:id" component={State} />
                                    <PrivateRoute path="/add/state" component={AddState} />
                                    <PrivateRoute exact path="/waterspace" component={WaterspacePage} />
                                    <PrivateRoute path="/edit/waterspace/:id" component={Waterspace} />
                                    <PrivateRoute path="/add/waterspace" component={AddWaterspace} />
                                    <PrivateRoute exact path="/province" component={ProvincePage} />
                                    <PrivateRoute path="/edit/province/:id" component={Province} />
                                    <PrivateRoute path="/add/province" component={AddProvince} />
                                    <PrivateRoute path="/map" component={Map} />
                                    <PrivateRoute exact path="/provinceResourcesLifecycle" component={ProvinceResourcesLifecyclePage} />
                                    <PrivateRoute path="/edit/provinceResourcesLifecycle/:provinceId/:resourceId" component={ProvinceResourcesLifecycle} />
                                    <PrivateRoute path="/add/provinceResourcesLifecycle" component={AddProvinceResourcesLifecycle} />
                                    <PrivateRoute exact path="/provinceWaterspaceNeighborhood" component={ProvinceWaterspaceNeighborhoodPage} />
                                    <PrivateRoute path="/add/provinceWaterspaceNeighborhood" component={AddProvinceWaterspaceNeighborhood} />
                                    <PrivateRoute exact path="/provinceReligions" component={ProvinceReligionsPage} />
                                    <PrivateRoute path="/edit/provinceReligions/:provinceId/:religionId" component={ProvinceReligions} />
                                    <PrivateRoute path="/add/provinceReligions" component={AddProvinceReligions} />
                                    <PrivateRoute exact path="/provinceFractions" component={ProvinceFractionsPage} />
                                    <PrivateRoute path="/edit/provinceFractions/:provinceId/:fractionId" component={ProvinceFractions} />
                                    <PrivateRoute path="/add/provinceFractions" component={AddProvinceFractions} />
                                    <PrivateRoute exact path="/provinceDeseases" component={ProvinceDeseasesPage} />
                                    <PrivateRoute path="/edit/provinceDeseases/:provinceId/:deseaseId" component={ProvinceDeseases} />
                                    <PrivateRoute path="/add/provinceDeseases" component={AddProvinceDeseases} />
                                    <PrivateRoute exact path="/provinceCultures" component={ProvinceCulturesPage} />
                                    <PrivateRoute path="/edit/provinceCultures/:provinceId/:cultureId" component={ProvinceCultures} />
                                    <PrivateRoute path="/add/provinceCultures" component={AddProvinceCultures} />
                                    <PrivateRoute exact path="/neighborhoodOfWaterspaces" component={NeighborhoodOfWaterspacesPage} />
                                    <PrivateRoute path="/add/neighborhoodOfWaterspaces" component={AddNeighborhoodOfWaterspaces} />
                                    <PrivateRoute exact path="/neighborhoodOfProvinces" component={NeighborhoodOfProvincesPage} />
                                    <PrivateRoute path="/add/neighborhoodOfProvinces" component={AddNeighborhoodOfProvinces} />
                                    <PrivateRoute path="/edit/neighborhoodOfProvinces/:firstProvinceId/:secondProvinceId" component={NeighborhoodOfProvinces} />
                                    <PrivateRoute path="/queries" component={QueriesPage} />
                                    <PrivateRoute path="/province/statesTreasury" component={StateTreasury} />
                                    <PrivateRoute path="/province/states" component={ConditionalStateProvince} />
                                    <PrivateRoute path="/province/fullPopulation" component={ProvincesFullPopulation} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}

export { App }; 