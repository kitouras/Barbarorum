import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const provinceDeseasesService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceDeseases?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(provinceId, deseaseId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceDeseases/'.concat(provinceId).concat('_').concat(deseaseId), requestOptions)
        .then(handleResponse);
}

function updateEntity(provinceId, deseaseId, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            prevalence: data.prevalence,
            collectiveImmunity: data.collectiveImmunity,
        })
    };

    return fetch('/api/provinceDeseases/'.concat(provinceId).concat('_').concat(deseaseId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(provinceId, deseaseId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/provinceDeseases/'.concat(provinceId).concat('_').concat(deseaseId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            prevalence: data.prevalence,
            collectiveImmunity: data.collectiveImmunity,
            provinceId: data.provinceId,
            deseaseId: data.deseaseId
        })
    };

    return fetch('/api/provinceDeseases/', requestOptions)
        .then(handleResponse);
}