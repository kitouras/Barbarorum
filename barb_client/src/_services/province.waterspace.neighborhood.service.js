import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const provinceWaterspaceNeighborhoodService = {
    getEntities,
    getEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceWaterspaceNeighborhood?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(provinceId, waterspaceId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceWaterspaceNeighborhood/'.concat(provinceId).concat('_').concat(waterspaceId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(provinceId, waterspaceId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/provinceWaterspaceNeighborhood/'.concat(provinceId).concat('_').concat(waterspaceId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            provinceId: data.provinceId,
            waterspaceId: data.waterspaceId
        })
    };

    return fetch('/api/provinceWaterspaceNeighborhood/', requestOptions)
        .then(handleResponse);
}