import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const seasonService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };
    const page = params["page"] != null ? params["page"] : '';
    const size = params["size"] != null ? params["size"] : '';
    const sortBy = params["sortBy"] != null ? params["sortBy"] : '';
    const sortDir = params["sortDir"] != null ? params["sortDir"] : '';
    const id = params["id"] != null ? params["id"] : '';
    const name = params["name"] != null ? params["name"] : '';
    const temperatureMod = params["temperatureMod"] != null ? params["temperatureMod"] : '';
    const windMod = params["windMod"] != null ? params["windMod"] : '';
    const humidityMod = params["humidityMod"] != null ? params["humidityMod"] : '';
    const atmosphericPressureMod = params["atmosphericPressureMod"] != null ? params["atmosphericPressureMod"] : '';
    const incomeModsId = params["incomeModsId"] != null ? params["incomeModsId"] : '';

    return fetch('/api/season?page='.concat(page).concat('&size=').concat(size)
        .concat('&sortBy=').concat(sortBy).concat('&sortDir=').concat(sortDir)
        .concat('&id=').concat(id).concat('&name=').concat(name)
        .concat('&temperatureMod=').concat(temperatureMod).concat('&windMod=').concat(windMod)
        .concat('&humidityMod=').concat(humidityMod).concat('&atmosphericPressureMod=').concat(atmosphericPressureMod)
        .concat('&incomeModsId=').concat(incomeModsId), requestOptions)
        .then(handleResponse);
}

function getEntity(id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/season/'.concat(id), requestOptions)
        .then(handleResponse);
}

function updateEntity(id, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            temperatureMod: data.temperatureMod,
            windMod: data.windMod,
            humidityMod: data.humidityMod,
            atmosphericPressureMod: data.atmosphericPressureMod,
            incomeModsId: data.incomeModsId
        })
    };

    return fetch('/api/season/'.concat(id), requestOptions)
        .then(handleResponse);
}

function deleteEntity(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/season/'.concat(id), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            temperatureMod: data.temperatureMod,
            windMod: data.windMod,
            humidityMod: data.humidityMod,
            atmosphericPressureMod: data.atmosphericPressureMod,
            incomeModsId: data.incomeModsId
        })
    };

    return fetch('/api/season/', requestOptions)
        .then(handleResponse);
}