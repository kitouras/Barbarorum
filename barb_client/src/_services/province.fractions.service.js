import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const provinceFractionsService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceFractions?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(provinceId, fractionId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceFractions/'.concat(provinceId).concat('_').concat(fractionId), requestOptions)
        .then(handleResponse);
}

function updateEntity(provinceId, fractionId, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            authority: data.authority,
            integrity: data.integrity,
        })
    };

    return fetch('/api/provinceFractions/'.concat(provinceId).concat('_').concat(fractionId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(provinceId, fractionId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/provinceFractions/'.concat(provinceId).concat('_').concat(fractionId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            authority: data.authority,
            integrity: data.integrity,
            provinceId: data.provinceId,
            fractionId: data.fractionId
        })
    };

    return fetch('/api/provinceFractions/', requestOptions)
        .then(handleResponse);
}