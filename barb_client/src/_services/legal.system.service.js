import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const legalSystemService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };
    const page = params["page"] != null ? params["page"] : '';
    const size = params["size"] != null ? params["size"] : '';
    const sortBy = params["sortBy"] != null ? params["sortBy"] : '';
    const sortDir = params["sortDir"] != null ? params["sortDir"] : '';
    const id = params["id"] != null ? params["id"] : '';
    const groupName = params["groupName"] != null ? params["groupName"] : '';
    const taxFixed = params["taxFixed"] != null ? params["taxFixed"] : '';
    const taxAboveMultiplier = params["taxAboveMultiplier"] != null ? params["taxAboveMultiplier"] : '';
    const autonomyFactor = params["autonomyFactor"] != null ? params["autonomyFactor"] : '';
    const incomeModsId = params["incomeModsId"] != null ? params["incomeModsId"] : '';


    return fetch('/api/legalSystem?page='.concat(page).concat('&size=').concat(size)
        .concat('&sortBy=').concat(sortBy).concat('&sortDir=').concat(sortDir)
        .concat('&id=').concat(id).concat('&groupName=').concat(groupName)
        .concat('&isTaxFixed=').concat(taxFixed).concat('&taxAboveMultiplier=').concat(taxAboveMultiplier)
        .concat('&autonomyFactor=').concat(autonomyFactor).concat('&incomeModsId=').concat(incomeModsId), requestOptions)
        .then(handleResponse);
}

function getEntity(id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/legalSystem/'.concat(id), requestOptions)
        .then(handleResponse);
}

function updateEntity(id, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            groupName: data.groupName,
            taxFixed: data.taxFixed,
            taxAboveMultiplier: data.taxAboveMultiplier,
            autonomyFactor: data.autonomyFactor,
            incomeModsId: data.incomeModsId
        })
    };

    return fetch('/api/legalSystem/'.concat(id), requestOptions)
        .then(handleResponse);
}

function deleteEntity(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/legalSystem/'.concat(id), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            groupName: data.groupName,
            taxFixed: data.taxFixed,
            taxAboveMultiplier: data.taxAboveMultiplier,
            autonomyFactor: data.autonomyFactor,
            incomeModsId: data.incomeModsId
        })
    };

    return fetch('/api/legalSystem/', requestOptions)
        .then(handleResponse);
}