import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const climatService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };
    const page = params["page"] != null ? params["page"] : '';
    const size = params["size"] != null ? params["size"] : '';
    const sortBy = params["sortBy"] != null ? params["sortBy"] : '';
    const sortDir = params["sortDir"] != null ? params["sortDir"] : '';
    const id = params["id"] != null ? params["id"] : '';
    const name = params["name"] != null ? params["name"] : '';
    const temperatureDegree = params["temperatureDegree"] != null ? params["temperatureDegree"] : '';
    const windDegree = params["windDegree"] != null ? params["windDegree"] : '';
    const humidityDegree = params["humidityDegree"] != null ? params["humidityDegree"] : '';
    const atmosphericPressureDegree = params["atmosphericPressureDegree"] != null ? params["atmosphericPressureDegree"] : '';
    const incomeModsId = params["incomeModsId"] != null ? params["incomeModsId"] : '';

    return fetch('/api/climat?page='.concat(page).concat('&size=').concat(size)
        .concat('&sortBy=').concat(sortBy).concat('&sortDir=').concat(sortDir)
        .concat('&id=').concat(id).concat('&name=').concat(name)
        .concat('&temperatureDegree=').concat(temperatureDegree).concat('&windDegree=').concat(windDegree)
        .concat('&humidityDegree=').concat(humidityDegree).concat('&atmosphericPressureDegree=').concat(atmosphericPressureDegree)
        .concat('&incomeModsId=').concat(incomeModsId), requestOptions)
        .then(handleResponse);
}

function getEntity(id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/climat/'.concat(id), requestOptions)
        .then(handleResponse);
}

function updateEntity(id, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            temperatureDegree: data.temperatureDegree,
            windDegree: data.windDegree,
            humidityDegree: data.humidityDegree,
            atmosphericPressureDegree: data.atmosphericPressureDegree,
            incomeModsId: data.incomeModsId
        })
    };

    return fetch('/api/climat/'.concat(id), requestOptions)
        .then(handleResponse);
}

function deleteEntity(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/climat/'.concat(id), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            temperatureDegree: data.temperatureDegree,
            windDegree: data.windDegree,
            humidityDegree: data.humidityDegree,
            atmosphericPressureDegree: data.atmosphericPressureDegree,
            incomeModsId: data.incomeModsId
        })
    };

    return fetch('/api/climat/', requestOptions)
        .then(handleResponse);
}