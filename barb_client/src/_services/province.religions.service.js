import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const provinceReligionsService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceReligions?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(provinceId, religionId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceReligions/'.concat(provinceId).concat('_').concat(religionId), requestOptions)
        .then(handleResponse);
}

function updateEntity(provinceId, religionId, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            percentage: data.percentage,
        })
    };

    return fetch('/api/provinceReligions/'.concat(provinceId).concat('_').concat(religionId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(provinceId, religionId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/provinceReligions/'.concat(provinceId).concat('_').concat(religionId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            percentage: data.percentage,
            provinceId: data.provinceId,
            religionId: data.religionId
        })
    };

    return fetch('/api/provinceReligions/', requestOptions)
        .then(handleResponse);
}