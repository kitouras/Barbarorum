import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const neighborhoodOfProvincesService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/neighborhoodOfProvinces?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(firstProvinceId, secondProvinceId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/neighborhoodOfProvinces/'.concat(firstProvinceId).concat('_').concat(secondProvinceId), requestOptions)
        .then(handleResponse);
}

function updateEntity(firstProvinceId, secondProvinceId, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            relationId: data.relationId,
        })
    };

    return fetch('/api/neighborhoodOfProvinces/'.concat(firstProvinceId).concat('_').concat(secondProvinceId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(firstProvinceId, secondProvinceId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/neighborhoodOfProvinces/'.concat(firstProvinceId).concat('_').concat(secondProvinceId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            firstProvinceId: data.firstProvinceId,
            secondProvinceId: data.secondProvinceId,
            relationId: data.relationId,
        })
    };

    return fetch('/api/neighborhoodOfProvinces/', requestOptions)
        .then(handleResponse);
}