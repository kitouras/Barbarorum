import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const incomeAffectableModeService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/incomeAffectableMode?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/incomeAffectableMode/'.concat(id), requestOptions)
        .then(handleResponse);
}

function updateEntity(id, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            villageTaxMultiplier: data.villageTaxMultiplier,
            cityTaxMultiplier: data.cityTaxMultiplier,
            foodMultiplier: data.foodMultiplier,
            tradeMultiplier: data.tradeMultiplier,
            handicraftMultiplier: data.handicraftMultiplier,
            foodMarketingMultiplier: data.foodMarketingMultiplier,
            tradeMarketingMultiplier: data.tradeMarketingMultiplier,
            handicraftMarketingMultiplier: data.handicraftMarketingMultiplier
        })
    };

    return fetch('/api/incomeAffectableMode/'.concat(id), requestOptions)
        .then(handleResponse);
}

function deleteEntity(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/incomeAffectableMode/'.concat(id), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            villageTaxMultiplier: data.villageTaxMultiplier,
            cityTaxMultiplier: data.cityTaxMultiplier,
            foodMultiplier: data.foodMultiplier,
            tradeMultiplier: data.tradeMultiplier,
            handicraftMultiplier: data.handicraftMultiplier,
            foodMarketingMultiplier: data.foodMarketingMultiplier,
            tradeMarketingMultiplier: data.tradeMarketingMultiplier,
            handicraftMarketingMultiplier: data.handicraftMarketingMultiplier
        })
    };

    return fetch('/api/incomeAffectableMode/', requestOptions)
        .then(handleResponse);
}