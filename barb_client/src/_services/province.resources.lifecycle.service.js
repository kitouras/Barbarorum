import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const provinceResourcesLifecycleService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };
    const page = params["page"] != null ? params["page"] : '';
    const size = params["size"] != null ? params["size"] : '';
    const sortBy = params["sortBy"] != null ? params["sortBy"] : '';
    const sortDir = params["sortDir"] != null ? params["sortDir"] : '';
    const provinceId = params["provinceId"] != null ? params["provinceId"] : '';
    const resourceId = params["resourceId"] != null ? params["resourceId"] : '';
    const resourceStock = params["resourceStock"] != null ? params["resourceStock"] : '';
    const resourceProduction = params["resourceProduction"] != null ? params["resourceProduction"] : '';
    const resourceLimit = params["resourceLimit"] != null ? params["resourceLimit"] : '';
    const resourceMarketing = params["resourceMarketing"] != null ? params["resourceMarketing"] : '';


    return fetch('/api/provinceResourcesLifecycle?page='.concat(page).concat('&size=').concat(size)
        .concat('&sortBy=').concat(sortBy).concat('&sortDir=').concat(sortDir)
            .concat('&provinceId=').concat(provinceId).concat('&resourceId=').concat(resourceId)
                .concat('&resourceStock=').concat(resourceStock).concat('&resourceProduction=').concat(resourceProduction)
                .concat('&resourceLimit=').concat(resourceLimit).concat('&resourceMarketing=').concat(resourceMarketing),
                requestOptions)
        .then(handleResponse);
}

function getEntity(provinceId, resourceId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceResourcesLifecycle/'.concat(provinceId).concat('_').concat(resourceId), requestOptions)
        .then(handleResponse);
}

function updateEntity(provinceId, resourceId, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            resourceStock: data.resourceStock,
            resourceProduction: data.resourceProduction,
            resourceLimit: data.resourceLimit,
            resourceMarketing: data.resourceMarketing,
        })
    };

    return fetch('/api/provinceResourcesLifecycle/'.concat(provinceId).concat('_').concat(resourceId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(provinceId, resourceId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/provinceResourcesLifecycle/'.concat(provinceId).concat('_').concat(resourceId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            resourceStock: data.resourceStock,
            resourceProduction: data.resourceProduction,
            resourceLimit: data.resourceLimit,
            resourceMarketing: data.resourceMarketing,
            provinceId: data.provinceId,
            resourceId: data.resourceId
        })
    };

    return fetch('/api/provinceResourcesLifecycle/', requestOptions)
        .then(handleResponse);
}