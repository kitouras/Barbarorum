import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const provinceCulturesService = {
    getEntities,
    getEntity,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceCultures?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(provinceId, cultureId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/provinceCultures/'.concat(provinceId).concat('_').concat(cultureId), requestOptions)
        .then(handleResponse);
}

function updateEntity(provinceId, cultureId, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            percentage: data.percentage,
        })
    };

    return fetch('/api/provinceCultures/'.concat(provinceId).concat('_').concat(cultureId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(provinceId, cultureId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/provinceCultures/'.concat(provinceId).concat('_').concat(cultureId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            percentage: data.percentage,
            provinceId: data.provinceId,
            cultureId: data.cultureId
        })
    };

    return fetch('/api/provinceCultures/', requestOptions)
        .then(handleResponse);
}