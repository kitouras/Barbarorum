import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const neighborhoodOfWaterspacesService = {
    getEntities,
    getEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/neighborhoodOfWaterspaces?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);
}

function getEntity(firstWaterspaceId, secondWaterspaceId) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/neighborhoodOfWaterspaces/'.concat(firstWaterspaceId).concat('_').concat(secondWaterspaceId), requestOptions)
        .then(handleResponse);
}

function deleteEntity(firstWaterspaceId, secondWaterspaceId) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/neighborhoodOfWaterspaces/'.concat(firstWaterspaceId).concat('_').concat(secondWaterspaceId), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            firstWaterspaceId: data.firstWaterspaceId,
            secondWaterspaceId: data.secondWaterspaceId
        })
    };

    return fetch('/api/neighborhoodOfWaterspaces/', requestOptions)
        .then(handleResponse);
}