import { handleResponse } from '@/_helpers';

import { authenticationService } from '@/_services';

export const provinceService = {
    getEntities,
    getEntity,
    getProvincesByStateName,
    countAllStatesProvincesWithConditions,
    countFullPopulationOfProvinces,
    countStatesTreasuryByProvince,
    updateEntity,
    deleteEntity,
    createEntity
};

function getEntities(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/province?page='.concat(params["page"]).concat('&size=').concat(params["size"]), requestOptions)
        .then(handleResponse);

}

function getProvincesByStateName(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/province/state/'.concat(params["name"]).concat('?page=').concat(params["page"]).concat(' & size=').concat(params["size"]), requestOptions)
        .then(handleResponse);

}

function countAllStatesProvincesWithConditions() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/province/states', requestOptions)
        .then(handleResponse);
}

function countFullPopulationOfProvinces() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/province/fullPopulation', requestOptions)
        .then(handleResponse);
}

function countStatesTreasuryByProvince(params) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/province/statesTreasury', requestOptions)
        .then(handleResponse);

}


function getEntity(id) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue)
        }
    };


    return fetch('/api/province/'.concat(id), requestOptions)
        .then(handleResponse);
}

function updateEntity(id, data) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            autonomy: data.autonomy,
            rebellionPotential: data.rebellionPotential,
            villagePopularity: data.villagePopularity,
            popularityGrowth: data.popularityGrowth,
            treasury: data.treasury,
            taxInto: data.taxInto,
            taxAbove: data.taxAbove,
            cityId: data.cityId,
            stateId: data.stateId,
            legalSystemId: data.legalSystemId,
            climatId: data.climatId,
            seasonId: data.seasonId,
            landscapeId: data.landscapeId
        })
    };

    return fetch('/api/province/'.concat(id), requestOptions)
        .then(handleResponse);
}

function deleteEntity(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({})
    };

    return fetch('/api/province/'.concat(id), requestOptions)
        .then(handleResponse);
}

function createEntity(data) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer '.concat(authenticationService.currentUserValue),
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: data.name,
            autonomy: data.autonomy,
            rebellionPotential: data.rebellionPotential,
            villagePopularity: data.villagePopularity,
            popularityGrowth: data.popularityGrowth,
            treasury: data.treasury,
            taxInto: data.taxInto,
            taxAbove: data.taxAbove,
            cityId: data.cityId,
            stateId: data.stateId,
            legalSystemId: data.legalSystemId,
            climatId: data.climatId,
            seasonId: data.seasonId,
            landscapeId: data.landscapeId
        })
    };

    return fetch('/api/province/', requestOptions)
        .then(handleResponse);
}