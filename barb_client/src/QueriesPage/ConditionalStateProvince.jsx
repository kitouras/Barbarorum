import React from 'react';
import { provinceService } from '@/_services';
class ConditionalStateProvince extends React.Component {
    constructor(props) {

        super(props);
        this.retrieveEntities = this.retrieveEntities.bind(this);
        this.state = {
            entities: []
        }
    }

    componentDidMount() {
        this.retrieveEntities();
    }

    retrieveEntities() {
        provinceService.countAllStatesProvincesWithConditions().then((response) => {
            console.log(response);
            this.setState({
                entities: response
            })
        })
    }

    render() {
        const {
            entities
        } = this.state;
        return (<div>
            <ul className="list-group">
                <ul className="list-group">
                    {entities &&
                        entities.map((entity, index) => (
                            <li
                                className={
                                    "list-group-item "
                                }
                                key={index}
                            >
                                {entity[0].concat(': ').concat(entity[1])}
                            </li>
                        ))}
                </ul>
            </ul>
        </div>
        );
    }
}


export { ConditionalStateProvince };