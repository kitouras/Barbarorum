import React from 'react';
import { Link } from 'react-router-dom';

class QueriesPage extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <div>
                <h1>Queries:</h1>
                <ul className="list-group">
                    <li><Link
                        to={"/province/statesTreasury"}
                        className="m-3 list-group-item"
                    >
                        State treasury by province.
              </Link></li>
                    <li><Link
                        to={"/province/states"}
                        className="m-3 list-group-item"
                    >
                        Count province by state with conditions.
              </Link></li>

                    <li><Link
                        to={"/province/fullPopulation"}
                        className="m-3 list-group-item"
                    >
                        Provinces full population.
              </Link></li>
                   
                </ul>
            </div>
            
            );
    }


}

export { QueriesPage };